# RfC Manager

RfC stands for Request for Change. According to ITIL a Request for Change is a formal request for changes on a Configuration Item or a Service. It can be submited by an stakeholder or service user.

The RfC Manager supports organisations on the implementation of an ITIL conform Change Management Process. It offers interfaces to the CMDB i-doit and the Service Desk Software Zammad.

## Installation
### From Source

Clone the Repository from [GitLab](https://gitlab.com/request-for-change/rfc/) beneath your Webroot:

```
git clone git@gitlab.com:request-for-change/rfc.git rfc
```

Change to the source directory and execute 

```
composer update
```
Create a file name `.env.local` with Config to your Database and a SMTP Host o send E-Mails:

```
DATABASE_URL="mysql://user:password@host:port/databasename?serverVersion=db-version"
MAILER_DSN="smtp://user:password@host:port"
```

E.g. if you are using a Mariadb Server 10.3.26, you should use `mariadb-10.3.26` as db-version in your config.

Create a database with the configured name and grant the configured user the appropriate rights to create, write and query the database.

Create the tables and fill them with init values:

```
bin/console doctrine:migrations:migrate
```

You are done! Navigate with your browser to the login page, e.g. [http://127.0.0.1/rfc/public](http://127.0.0.1/rfc/public). Login with the username `admin` and the password `changeme`.