#! /bin/bash

tables=(authorizations business_processes categories config cost_centers date_types decisions departments doctrine_migration_versions field_types implementation_status notification_templates notification_types priorities projects reasons reports ressource_types rfc rfc_authorizations rfc_bpo_feedback rfc_classifications rfc_notices rfc_standardchanges rfc_workflows roles roles_users standardchange_steps status units user_tokens users)

for i in ${tables[*]} ; do
    echo $i
    mysqldump cwrm_data $i > $i.sql
done
