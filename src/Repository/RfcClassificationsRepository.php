<?php

namespace App\Repository;

use App\Entity\RfcClassifications;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RfcClassifications|null find($id, $lockMode = null, $lockVersion = null)
 * @method RfcClassifications|null findOneBy(array $criteria, array $orderBy = null)
 * @method RfcClassifications[]    findAll()
 * @method RfcClassifications[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RfcClassificationsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RfcClassifications::class);
    }

    // /**
    //  * @return RfcClassifications[] Returns an array of RfcClassifications objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RfcClassifications
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
