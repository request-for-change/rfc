<?php

namespace App\Repository;

use App\Entity\Ressourcetypes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Ressourcetypes|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ressourcetypes|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ressourcetypes[]    findAll()
 * @method Ressourcetypes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RessourcetypesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Ressourcetypes::class);
    }

    // /**
    //  * @return Ressourcetypes[] Returns an array of Ressourcetypes objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Ressourcetypes
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
