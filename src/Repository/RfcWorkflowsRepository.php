<?php

namespace App\Repository;

use App\Entity\RfcWorkflows;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RfcWorkflows|null find($id, $lockMode = null, $lockVersion = null)
 * @method RfcWorkflows|null findOneBy(array $criteria, array $orderBy = null)
 * @method RfcWorkflows[]    findAll()
 * @method RfcWorkflows[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RfcWorkflowsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RfcWorkflows::class);
    }

    /**
     * @return RfcWorkflows[] Returns an array of RfcWorkflows objects
    */
    public function findByRfc($value)
    {
        $wfs = $this->createQueryBuilder('r')
                   ->andWhere('r.rfc = :val')
                   ->setParameter('val', $value)
                   ->orderBy('r.start_date', 'ASC')
                   ->addOrderBy('r.end_date', 'ASC')
                   ->getQuery()
                   ->getResult()
        ;

        uasort($wfs, [$this, 'cmp']);

        return ($wfs);
    }

    public function getByCmdbObject($obj_id): ?array
    {
        $qb = $this->createQueryBuilder('r');

        $obj = array('"'.$obj_id.'"');

        $qb->where("JSON_CONTAINS(r.objects, :obj_id) = 1")
            ->setParameter('obj_id', $obj)
        ;

        $query = $qb->getQuery();
        return $query->getResult();
    }

    private function cmp($a, $b) {
        // Beide haben keine paren id
        if (!$a->parent && !$b->parent) {
            return 0;
        }
        // nur einer hat eine Parent id -> denjenigen mit nach oben sortieren
        if (!$a->parent) {
            return -1;
        }
        if (!$b->parent) {
            return 1;
        }
        // beide haben die selbe paren id -> alles bleibt gleich
        if ($a->parent->id == $b->parent->id) {
            return 0;
        }

        // einer von beiden hat den anderen als parent
        if ($a->parent->id == $b->id) {
            return 1;
        }
        if ($b->parent->id == $a->id) {
            return -1;
        }
        return 0;

    }


    /*
    public function findOneBySomeField($value): ?RfcWorkflows
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
