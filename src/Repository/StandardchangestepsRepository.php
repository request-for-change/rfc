<?php

namespace App\Repository;

use App\Entity\Standardchangesteps;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Standardchangesteps|null find($id, $lockMode = null, $lockVersion = null)
 * @method Standardchangesteps|null findOneBy(array $criteria, array $orderBy = null)
 * @method Standardchangesteps[]    findAll()
 * @method Standardchangesteps[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StandardchangestepsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Standardchangesteps::class);
    }

    // /**
    //  * @return Standardchangesteps[] Returns an array of Standardchangesteps objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Standardchangesteps
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
