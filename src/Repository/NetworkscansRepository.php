<?php

namespace App\Repository;

use App\Entity\Networkscans;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Networkscans|null find($id, $lockMode = null, $lockVersion = null)
 * @method Networkscans|null findOneBy(array $criteria, array $orderBy = null)
 * @method Networkscans[]    findAll()
 * @method Networkscans[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NetworkscansRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Networkscans::class);
    }

    // /**
    //  * @return Networkscans[] Returns an array of Networkscans objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Networkscans
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
