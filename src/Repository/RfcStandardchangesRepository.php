<?php

namespace App\Repository;

use App\Entity\RfcStandardchanges;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RfcStandardchanges|null find($id, $lockMode = null, $lockVersion = null)
 * @method RfcStandardchanges|null findOneBy(array $criteria, array $orderBy = null)
 * @method RfcStandardchanges[]    findAll()
 * @method RfcStandardchanges[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RfcStandardchangesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RfcStandardchanges::class);
    }

    // /**
    //  * @return RfcStandardchanges[] Returns an array of RfcStandardchanges objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RfcStandardchanges
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
