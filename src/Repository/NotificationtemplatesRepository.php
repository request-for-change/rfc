<?php

namespace App\Repository;

use App\Entity\Notificationtemplates;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Notificationtemplates|null find($id, $lockMode = null, $lockVersion = null)
 * @method Notificationtemplates|null findOneBy(array $criteria, array $orderBy = null)
 * @method Notificationtemplates[]    findAll()
 * @method Notificationtemplates[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NotificationtemplatesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Notificationtemplates::class);
    }

    // /**
    //  * @return Notificationtemplates[] Returns an array of Notificationtemplates objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Notificationtemplates
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
