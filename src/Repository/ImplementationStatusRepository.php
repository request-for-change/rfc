<?php

namespace App\Repository;

use App\Entity\ImplementationStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ImplementationStatus|null find($id, $lockMode = null, $lockVersion = null)
 * @method ImplementationStatus|null findOneBy(array $criteria, array $orderBy = null)
 * @method ImplementationStatus[]    findAll()
 * @method ImplementationStatus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ImplementationStatusRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ImplementationStatus::class);
    }

    // /**
    //  * @return ImplementationStatus[] Returns an array of ImplementationStatus objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ImplementationStatus
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
