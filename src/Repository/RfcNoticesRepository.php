<?php

namespace App\Repository;

use App\Entity\RfcNotices;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RfcNotices|null find($id, $lockMode = null, $lockVersion = null)
 * @method RfcNotices|null findOneBy(array $criteria, array $orderBy = null)
 * @method RfcNotices[]    findAll()
 * @method RfcNotices[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RfcNoticesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RfcNotices::class);
    }

    // /**
    //  * @return RfcNotices[] Returns an array of RfcNotices objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RfcNotices
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
