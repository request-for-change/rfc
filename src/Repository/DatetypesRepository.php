<?php

namespace App\Repository;

use App\Entity\Datetypes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Datetypes|null find($id, $lockMode = null, $lockVersion = null)
 * @method Datetypes|null findOneBy(array $criteria, array $orderBy = null)
 * @method Datetypes[]    findAll()
 * @method Datetypes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DatetypesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Datetypes::class);
    }

    // /**
    //  * @return Datetypes[] Returns an array of Datetypes objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Datetypes
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
