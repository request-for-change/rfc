<?php

namespace App\Repository;

use App\Entity\Costcenters;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Costcenters|null find($id, $lockMode = null, $lockVersion = null)
 * @method Costcenters|null findOneBy(array $criteria, array $orderBy = null)
 * @method Costcenters[]    findAll()
 * @method Costcenters[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CostcentersRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Costcenters::class);
    }

    // /**
    //  * @return Costcenters[] Returns an array of Costcenters objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Costcenters
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
