<?php

namespace App\Repository;

use App\Entity\Hostrecords;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Hostrecords|null find($id, $lockMode = null, $lockVersion = null)
 * @method Hostrecords|null findOneBy(array $criteria, array $orderBy = null)
 * @method Hostrecords[]    findAll()
 * @method Hostrecords[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HostrecordsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Hostrecords::class);
    }

    // /**
    //  * @return Hostrecords[] Returns an array of Hostrecords objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Hostrecords
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
