<?php

namespace App\Repository;

use App\Entity\EvidenceCollections;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EvidenceCollections|null find($id, $lockMode = null, $lockVersion = null)
 * @method EvidenceCollections|null findOneBy(array $criteria, array $orderBy = null)
 * @method EvidenceCollections[]    findAll()
 * @method EvidenceCollections[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EvidenceCollectionsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EvidenceCollections::class);
    }

    // /**
    //  * @return EvidenceCollections[] Returns an array of EvidenceCollections objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EvidenceCollections
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function getWithSearchQueryBuilder(array $filter): QueryBuilder
    {
        $qb = $this->createQueryBuilder('evidence_collection');

        foreach ($filter as $field => $value) {
            if ($value) {
                $qb->andWhere('evidence_collection.'.$field.' = :'.$field.'')
                    ->setParameter($field, $value);
            }
        }


        return $qb
            ->orderBy('evidence_collection.created', 'DESC')
            ;
    }
}
