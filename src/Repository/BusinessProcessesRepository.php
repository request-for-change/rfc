<?php

namespace App\Repository;

use App\Entity\BusinessProcesses;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BusinessProcesses|null find($id, $lockMode = null, $lockVersion = null)
 * @method BusinessProcesses|null findOneBy(array $criteria, array $orderBy = null)
 * @method BusinessProcesses[]    findAll()
 * @method BusinessProcesses[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BusinessProcessesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BusinessProcesses::class);
    }

    // /**
    //  * @return BusinessProcesses[] Returns an array of BusinessProcesses objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BusinessProcesses
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
