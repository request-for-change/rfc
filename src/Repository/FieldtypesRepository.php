<?php

namespace App\Repository;

use App\Entity\Fieldtypes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Fieldtypes|null find($id, $lockMode = null, $lockVersion = null)
 * @method Fieldtypes|null findOneBy(array $criteria, array $orderBy = null)
 * @method Fieldtypes[]    findAll()
 * @method Fieldtypes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FieldtypesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Fieldtypes::class);
    }

    // /**
    //  * @return Fieldtypes[] Returns an array of Fieldtypes objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Fieldtypes
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
