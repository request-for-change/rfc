<?php

namespace App\Repository;

use App\Entity\RfcAuthorizations;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RfcAuthorizations|null find($id, $lockMode = null, $lockVersion = null)
 * @method RfcAuthorizations|null findOneBy(array $criteria, array $orderBy = null)
 * @method RfcAuthorizations[]    findAll()
 * @method RfcAuthorizations[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RfcAuthorizationsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RfcAuthorizations::class);
    }

    // /**
    //  * @return RfcAuthorizations[] Returns an array of RfcAuthorizations objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RfcAuthorizations
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
