<?php

namespace App\Repository;

use App\Entity\RfcBpoFeedback;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RfcBpoFeedback|null find($id, $lockMode = null, $lockVersion = null)
 * @method RfcBpoFeedback|null findOneBy(array $criteria, array $orderBy = null)
 * @method RfcBpoFeedback[]    findAll()
 * @method RfcBpoFeedback[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RfcBpoFeedbackRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RfcBpoFeedback::class);
    }

    // /**
    //  * @return RfcBpoFeedback[] Returns an array of RfcBpoFeedback objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RfcBpoFeedback
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
