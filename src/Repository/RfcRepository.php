<?php

namespace App\Repository;

use App\Entity\Rfc;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Rfc|null find($id, $lockMode = null, $lockVersion = null)
 * @method Rfc|null findOneBy(array $criteria, array $orderBy = null)
 * @method Rfc[]    findAll()
 * @method Rfc[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RfcRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Rfc::class);
    }

    // /**
    //  * @return Rfc[] Returns an array of Rfc objects
    //  */

    public function getWithSearchQueryBuilder(array $filter, array $default_status_filter, array $sort): QueryBuilder
    {
        $qb = $this->createQueryBuilder('r');

        $qb
            ->leftJoin('r.rfc_classification', 'c', 'WITH', 'c.rfc = r.id')
            ->leftJoin('r.rfcWorkflows', 'w', 'WITH', 'w.rfc = r.id')
        ;

        foreach ($filter as $field => $value) {
            if ($value) {
                if ($field == 'classificator' or $field == 'authorizationtype') {
                    $qb->andWhere('c.' . $field . ' = :' . $field)
                        ->setParameter($field, $value);
                } elseif ($field == 'title') {
                    $qb->andWhere('r.' . $field . ' like :' . $field)
                        ->orWhere('r.description like :description')
                        ->setParameter($field, $value)
                        ->setParameter("description", "%$value%")
                    ;
                } elseif ($field == 'change_after') {
                    // Query for changes after
                    $qb->andWhere('w.start_date >= :' . $field)
                        ->setParameter($field, $value);
                }elseif ($field == 'change_before') {
                    // Query for changes before
                    $qb->andWhere('w.end_date <= :' . $field)
                        ->setParameter($field, $value);
                } else {
                    $qb->andWhere('r.' . $field . ' = :' . $field)
                        ->setParameter($field, $value);
                }
            }
        }

        // Only show finished states if queried for specific status or id
        if (
            !$filter['status'] &&
            !$filter['id'] &&
            !$filter['title'] &&
            !$filter['referenced_ticketid'] &&
            !$filter['change_after'] &&
            !$filter['change_before']
        ) {
            $qb->andWhere('r.status IN (:default_status)')
                ->setParameter('default_status', $default_status_filter);
        }

        //dd($sort);
        if ($sort['by'] == "classificator") {
            $order_by = "c.classificator";
        } else {
            $order_by = 'r.' . $sort['by'];
        }
        //dd($order_by);

        $qb->orderBy($order_by, $sort['direction']);

        return $qb;
    }

    public function getByCmdbObject($obj_id): ?array
    {
        $qb = $this->createQueryBuilder('r');

        $obj = array('"'.$obj_id.'"');

        $qb->where("JSON_CONTAINS(r.affected_componentsstore, :obj_id) = 1")
            ->setParameter('obj_id', $obj)
        ;

        $query = $qb->getQuery();
        return $query->getResult();
    }



    /*
    public function findOneBySomeField($value): ?Rfc
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
