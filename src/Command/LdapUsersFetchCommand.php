<?php

namespace App\Command;

use App\Entity\Departments;
use App\Entity\User;
use App\Repository\ConfigRepository;
use App\Repository\UserRepository;
use App\Repository\DepartmentsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class LdapUsersFetchCommand extends Command
{
    protected static $defaultName = 'ldap-users:fetch';
    protected static $defaultDescription = 'Fetch users from configured LDAP Server';
    /**
     * @var LdapController
     */
    private $configRepository;

    public function __construct(ConfigRepository $configRepository, UserRepository $userRepository, DepartmentsRepository $departmentsRepository, EntityManagerInterface $entityManager, LoggerInterface $logger)
    {
        parent::__construct(null);
        $this->configRepository = $configRepository;
        $this->userRepository = $userRepository;
        $this->departmentsRepository = $departmentsRepository;
        $this->entityManager = $entityManager;
        $this->logger = $logger;
    }


    protected function configure(): void
    {
        $this
            ->setDescription('')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $counter = 0;
        $io = new SymfonyStyle($input, $output);

        $paged_ldap_entries = $this->fetch_data();


        if ($paged_ldap_entries) {
            $io->progressStart(count($paged_ldap_entries));
            foreach ($paged_ldap_entries as $ldap_entries) {
                foreach ($ldap_entries as $ldap_entry) {
                    $io->progressAdvance();
                    if (is_array($ldap_entry)) {
                        $clean_entry = $this->cleanup_entry($ldap_entry);
                        $user = $this->userRepository->findOneBy([
                            'username' => $clean_entry['samaccountname']
                        ]);
                        if (!$user) {
                            $user = new User();
                            $department = $this->departmentsRepository->findBy([], ['title' => 'ASC'], 1);
                            $user->setDepartment($department[0]);
                        }
                        $user->setEmail($clean_entry["mail"])
                            ->setFirstname($clean_entry["givenname"])
                            ->setLastname($clean_entry["sn"])
                            ->setUsername($clean_entry["samaccountname"])
                            ->setPassword('')
                            ->setLogins(0)
                            ->setLastLogin(0);


                        $this->entityManager->persist($user);
                        $counter++;
                        $this->logger->info('synced LDAP entry ' . $clean_entry["samaccountname"] . ' with local user base.');
                    }
                }
            }
            $this->entityManager->flush();
            $io->success('Synced ' . $counter . ' User Accounts with LDAP.');
            return Command::SUCCESS;
        } else {
            $io->error('Syncing with LDAP failed, please review yor LDAP settings.');
        }
    }

    private function fetch_data() {
        $ldap_entries = array();
        $config = $this->fetch_config();

        if ($config['ssl'] == "1") {
            $ldap_host = 'ldaps://'. $config['hostname'];
        } else {
            $ldap_host = 'ldap://'. $config['hostname'];
        }
        if ($ldap_connection = ldap_connect($ldap_host)) {
            try {
                if (ldap_set_option($ldap_connection, LDAP_OPT_PROTOCOL_VERSION, 3)) {
                    if ($ldap_bind = ldap_bind($ldap_connection, $config['rdn'], $config['password'])){
                        $cookie = '';
                        do {
                            $ldap_search = ldap_search(
                                $ldap_connection,
                                $config['base_dn'],
                                $config['filter'],
                                ["givenName", "sn", "ou", "mail", "sAMAccountName"],
                                0,
                                -1,
                                -1,
                                LDAP_DEREF_NEVER,
                                [
                                    [
                                        'oid' => LDAP_CONTROL_PAGEDRESULTS,
                                        'value' => [
                                            'size' => 500,
                                            'cookie' => $cookie
                                        ]
                                    ]
                                ]
                            );
                            ldap_parse_result($ldap_connection, $ldap_search, $error_code , $matched_dn , $error_message , $referrals, $controls);
                            // ToDo: Sensible error handling
                            $ldap_entries[] = ldap_get_entries( $ldap_connection , $ldap_search);


                            if (isset($controls[LDAP_CONTROL_PAGEDRESULTS]['value']['cookie'])) {
                                // You need to pass the cookie from the last call to the next one
                                $cookie = $controls[LDAP_CONTROL_PAGEDRESULTS]['value']['cookie'];
                            } else {
                                $cookie = '';
                            }
                            // Empty cookie means last page
                        } while (!empty($cookie));

                        // End new code

                        return $ldap_entries;
                    } else {
                        throw new \Exception('can’t use LDAP Version 3');
                    }

                }
            } catch (\Exception $e) {
                // ToDo do something sensitive
                $this->logger->error($e);
                return false;
            }
        } else {
            // ToDo catch error;
            $this->logger->error('Connection to LDAP Server failed');
            return false;
        }
    }

    private function fetch_config() {
        $items = $this->configRepository->findBy(["group_name" => "ldap"]);

        foreach ($items as $item) {
            $config[$item->getConfigKey()] = unserialize($item->getConfigValue());
        }
        return $config;
    }

    private function cleanup_entry( $entry ) {
        $return_entry = array(
            "sn"        => "",
            "givenname" => "",
            "mail"      => "",
            "username"  => ""
        );
        for ( $i = 0; $i < $entry['count']; $i++ ) {
            $attribute = $entry[$i];
            if ( $entry[$attribute]['count'] == 1 ) {
                $return_entry[$attribute] = $entry[$attribute][0];
            } else {
                for ( $j = 0; $j < $entry[$attribute]['count']; $j++ ) {
                    $return_entry[$attribute][] = $entry[$attribute][$j];
                }
            }
        }
        return $return_entry;
    }
}
