<?php

namespace App\Command;

use App\Repository\UserRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class StatusChangeSendCommand extends Command
{
    protected static $defaultName = 'app:status-change:send';

    private $userRepository;

    public function __construct(UserRepository $userRepository) {
        parent::__construct(NULL);
        $this->userRepository = $userRepository;
    }


    protected function configure()
    {
        $this
            ->setDescription('Send Notification on RfC Status Change')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $io->success('Notified Users about Status Change');


        return Command::SUCCESS;
    }
}
