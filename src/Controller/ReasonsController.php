<?php

namespace App\Controller;

use App\Entity\Reasons;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Uuid;

class ReasonsController extends AbstractController
{
    /**
     * @Route("/reasons", name="reasons")
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $repository = $entityManager->getRepository(Reasons::class);
        
        //dd($repository->findOneBy(["id" => 2])->serialized());
        return $this->render('reasons/index.html.twig', [
            'selection_list' => $repository->findAll(),
        ]);
    }

    /**
     * @Route ("/reasons/{id<(\d)+>}", name="reasons_edit")
     */
    public function edit($id, EntityManagerInterface $entityManager) :Response
    {
        $repository = $entityManager->getRepository(Reasons::class);
        $reasons = $repository->find($id);

        /** @var Reasons|null $reasons */
        if (!$reasons) {
            throw $this->createNotFoundException('This Reason does not exist');
        }

        return $this->render('reasons/edit.html.twig', array(
            'reason' => $reasons
        ));

    }

    /**
     * @Route ("/reasons/{id<(\d)+>}/save"), name="reason_fields_save")
     */
    public function fields_save(Request $request, EntityManagerInterface $entityManager, $id) :Response
    {
        $repository = $entityManager->getRepository(Reasons::class);
        $reasons = $repository->find($id);

        /** @var Reasons|null $reasons */
        if (!$reasons) {
            throw $this->createNotFoundException('This Reason does not exist');
        }


        if ($request->request->has("save")) {
            // if a field with this id already exists, we delete it first
            $fields = $reasons->getFields();
            $fields = $this->delete_field($fields, $request->request->get("id"));

            // Now we write the field definition to the array of fields
            $field_names = ["id", "label", "type", "placeholder", "default"];
            $index = 0;
            foreach ($field_names as $field_name) {
                $field[$index]["name"] = $field_name;
                $field[$index]["value"] = $request->request->get($field_name);
                $index++;
            }

            $fields[] = $field;
        }
        elseif ($request->request->has("delete")) {
            $fields = $reasons->getFields();
            $fields = $this->delete_field($fields, $request->request->get("id"));
        }


        $reasons->setFields($fields);

        $entityManager->persist($reasons);
        $entityManager->flush();

        return $this->redirectToRoute('reasons_edit', array('id' => $reasons->getId()));

    }

    private function delete_field(array $fields, string $id ) :array
    {
        foreach ($fields as $index => $field_definitions) {
            foreach ($field_definitions as $field_definition) {
                if ($field_definition["name"] == "id" && $field_definition["value"] == $id) {
                    unset($fields[$index]);
                }
            }
        }
        return $fields;
    }

    /**
     * @Route ("/reasons/save", name="reasons_save")
     */
    public function save(EntityManagerInterface $entityManager, Request $request) {
        $message = '';
        $repository = $entityManager->getRepository(Reasons::class);

        $reason = $repository->findOneBy(["id" => $request->request->get("id")]);
        if (!$reason) {
            $reason = new Reasons();
            $reason->setLocked(1);
            $reason->setFields([]);
        }

        $reason->setTitle($request->request->get("title"));
        $entityManager->persist($reason);
        $entityManager->flush();

        $items = $repository->findAll();

        $trs = array();
        foreach ($items as $item) {
            $trs[] = [
                "id" => $item->getId(),
                "title" => $item->getTitle()
            ];

        }

        $message .= '<span class="">Reason saved successfully</span>';

        return $this->redirectToRoute('reasons');
    }

    /**
     * @Route ("/reason/fields/{reason<(\d)+>}"), name="reason_fields")
     */
    public function reason_fields($reason, EntityManagerInterface $entityManager): JsonResponse
    {
        $repository = $entityManager->getRepository(Reasons::class);
        $reason = $repository->find($reason);

        $fields =[];
        if($reason) {
            $fieldDefinitions = $reason->getFields();
            $fieldsIndex = 0;
            foreach ($fieldDefinitions as $fieldDefinition) {
                foreach ($fieldDefinition as $definition) {
                    $fields[$fieldsIndex][$definition["name"]] = $definition["value"];
                }
                $fieldsIndex++;
            }
        }
        return new JsonResponse($fields);
    }

    /**
     * @Route ("/reason/default/{reason<(\d)+>}"), name="reason_default")
     */
    public function make_default($reason, EntityManagerInterface $entityManager): JsonResponse
    {
        $repository = $entityManager->getRepository(Reasons::class);
        $reason = $repository->find($reason);

        $repository = $entityManager->getRepository(Reasons::class);
        // Set the isDefault property of all other reasons to false
        $otherReasons = $repository->findAll();
        foreach ($otherReasons as $otherReason) {
            if ($otherReason !== $reason) {
                $otherReason->setIsDefault(false);
            }
        }
        // Set the isDefault property of the given reason to true
        $reason->setIsDefault(true);

        $entityManager->flush();

        return new JsonResponse(['success' => true]);
    }

    /**
     * @Route ("/reason/active/{reason<(\d)+>}/{state}"), name="reason_active")
     */
    public function toggle_active($reason, EntityManagerInterface $entityManager, string $state): JsonResponse
    {
        $repository = $entityManager->getRepository(Reasons::class);
        $reason = $repository->find($reason);

        $state = filter_var($state, FILTER_VALIDATE_BOOLEAN);

        // Set the isActive property of the given reason to true
        $reason->setIsActive($state);

        $entityManager->flush();

        return new JsonResponse(['success' => true]);
    }
}
