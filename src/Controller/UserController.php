<?php

namespace App\Controller;

use App\Entity\Departments;
use App\Entity\Roles;
use App\Entity\User;
use App\Repository\DepartmentsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * @Route("/user", name="rfc_user", methods="GET")
     */
    public function index(EntityManagerInterface $entityManager, PaginatorInterface $paginator, Request $request): Response
    {
        $repository = $entityManager->getRepository(Departments::class);
        $departments = $repository->findBy([],['title' => 'ASC']);

        $repository = $entityManager->getRepository(Roles::class);
        $roles = $repository->findAll();

        $filter = [
            "name"       => "",
            "department" => "",
            "email"      => "",
            "role"       => "",
        ];

        foreach ($filter as $parameter => $value) {
            $filter[$parameter] = $request->query->get($parameter);
        }

        $repository = $entityManager->getRepository(User::class);
        $queryBuilder = $repository->getWithSearchQueryBuilder($filter);
        $pagination = $paginator->paginate(
            $queryBuilder, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );
        $pagination->setTemplate('@KnpPaginator/Pagination/twitter_bootstrap_v4_pagination.html.twig');

        $supported_locales = $this->getParameter('app.supported_locales');


        return $this->render('user/index.html.twig', [
            'users'             => $pagination,
            'departments'       => $departments,
            'roles'             => $roles,
            'filter'            => $filter,
            'supported_locales' => $supported_locales,
        ]);
    }

    /**
     * @Route ("/user/add", name="rfc_user_update", methods="POST")
     */
    public function update(EntityManagerInterface $entityManager, Request $request, UserPasswordHasherInterface $userPasswordHasher)
    {
        //dd($request->request);

        $repository = $entityManager->getRepository(Departments::class);
        $department = $repository->findOneBy(["id" => $request->request->get("department")]);

        $repository = $entityManager->getRepository(User::class);
        $user       = $repository->findOneBy(["id" => $request->request->get("id")]);

        if (!$user) {
            $user = new User();
            $user->setFirstname($request->request->get("firstname"))
                ->setLastname($request->request->get("lastname"))
                ->setEmail($request->request->get("email"))
                ->setUsername($request->request->get("username"))
                ->setLogins(0)
                ->setLastLogin(0)
            ;
        }

        $user->setDepartment($department);
        $user->setFirstname($request->request->get("firstname"))
            ->setLastname($request->request->get("lastname"))
            ->setEmail($request->request->get("email"))
            ->setLanguage($request->request->get("language"))
            ->setLocked($request->request->get("locked"))
        ;

        if ($request->request->get("password") && $request->request->get("password") != '') {
            $user->setPassword($userPasswordHasher->hashPassword($user, $request->request->get("password")));
        }

        $repository = $entityManager->getRepository(Roles::class);
        if (is_array($request->request->get("roles"))) {
            foreach ($user->getroles() as $role_const ) {
                //dd($role);
                $role = $repository->findOneBy(['const' => $role_const]);
                if ($role instanceof Roles) {
                    $user->removeroles($role);
                }
             }
            foreach ($request->request->get("roles") as $role_const) {
                $role = $repository->findOneBy(['const' => $role_const]);
                $user->addroles($role);
            }

        }

        $entityManager->persist($user);
        $entityManager->flush();

        $tr = [
            'id'          => $user->getId(),
            'firstname'   => $user->getFirstname(),
            'lastname'    => $user->getLastname(),
            'email'       => $user->getEmail(),
            'username'    => $user->getUsername(),
            'department'  => $user->getDepartment()->getTitle(),
            'roles'       => $user->getroles(),
            'serialized'  => $user->getSerialized(),
        ];

        return new JsonResponse([
            'message' => '<span class="alert-success">Saving User data succeeded</span>',
            'user'    => $tr,
        ]);

    }
}
