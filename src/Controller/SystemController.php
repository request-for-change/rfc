<?php

namespace App\Controller;

use App\Entity\Config;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SystemController extends AbstractController
{
    private $config = array(
        "system_url"              => "",
        "four_eyes"               => 0,
        "language"                => "en",
        "notify_at_once"          => 0,
        "section_required"        => 0,
        "process_required"        => 0,
        "currency"                => "€",
        "allow_classification"    => 0,
        "ciso"                    => NULL,
        "sacm"                    => NULL,
        "optional_target_date"    => NULL,
    );
    /**
     * @Route("/system", name="system")
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $repository = $entityManager->getRepository(User::class);
        $users = $repository->findBy([], ['lastname' => 'ASC']);
        $this->config = $this->get_config($entityManager);
        return $this->render('system/show.html.twig', [
            'config' => $this->config,
            'users'  => $users,
            'server_info' => $_SERVER,
        ]);
    }

    /**
     * @param EntityManagerInterface $entityManager
     * @param Request $request
     * @Route ("system/save", name="system_save")
     */
    public function save(EntityManagerInterface $entityManager, Request $request)
    {
        $repository = $entityManager->getRepository(Config::class);
        $items = $repository->findBy(["group_name" => "system"]);

        foreach ($items as $item) {
            $item->setConfigValue(serialize($request->request->get($item->getConfigKey())));
            $entityManager->persist($item);
            $entityManager->flush();
        }

        return new JsonResponse([
            'message' => '<span class="">System settings saved successfully</span>'
        ]);
    }

    public function get_config(EntityManagerInterface $entityManager)
    {
        $repository = $entityManager->getRepository(Config::class);
        $items = $repository->findBy(["group_name" => "system"]);

        foreach ($items as $item) {
            $config[$item->getConfigKey()] = unserialize($item->getConfigValue());
        }

        return $config;
    }
}

