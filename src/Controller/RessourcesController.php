<?php

namespace App\Controller;

use App\Entity\Ressourcetypes;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RessourcesController extends AbstractController
{
    /**
     * @Route("/ressources", name="ressources")
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $repository = $entityManager->getRepository(Ressourcetypes::class);
        return $this->render('ressources/index.html.twig', [
            'selection_list' => $repository->findAll(),
        ]);
    }

    /**
     * @Route ("/ressources/save", name="ressources_save")
     */
    public function save(EntityManagerInterface $entityManager, Request $request) {
        $message = '';
        $repository = $entityManager->getRepository(Ressourcetypes::class);

        $ressource = $repository->findOneBy(["id" => $request->request->get("id")]);
        if (!$ressource) {
            $ressource = new Ressourcetypes();
            $ressource->setLocked(1);
        }
        
        $ressource->setTitle($request->request->get("title"));
        $entityManager->persist($ressource);
        $entityManager->flush();

        $items = $repository->findAll();

        $trs = array();
        foreach ($items as $item) {
            $trs[] = [
                "id" => $item->getId(),
                "title" => $item->getTitle()
            ];

        }

        return $this->redirectToRoute('ressources');
    }
}
