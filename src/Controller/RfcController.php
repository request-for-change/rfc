<?php
/**
 * don’t panic it-services
 * User: chris
 * Date: 27.11.20
 * Time: 15:20
 */

namespace App\Controller;


use App\Entity\Authorizations;
use App\Entity\BusinessProcesses;
use App\Entity\Categories;
use App\Entity\Config;
use App\Entity\Costcenters;
use App\Entity\Datetypes;
use App\Entity\Departments;
use App\Entity\ImplementationStatus;
use App\Entity\Priorities;
use App\Entity\Projects;
use App\Entity\Reasons;
use App\Entity\Ressourcetypes;
use App\Entity\Rfc;
use App\Entity\RfcAuthorizations;
use App\Entity\RfcBpoFeedback;
use App\Entity\RfcClassifications;
use App\Entity\RfcNotices;
use App\Entity\RfcStandardchanges;
use App\Entity\RfcWorkflows;
use App\Entity\Roles;
use App\Entity\Status;
use App\Entity\Units;
use App\Entity\User;
use bheisig\idoitapi\API;
use bheisig\idoitapi\CMDBObjects;
use bheisig\idoitapi\CMDBObjectTypes;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Knp\Component\Pager\PaginatorInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Twig\Extra\Markdown\DefaultMarkdown;

class RfcController extends AbstractController
{
    private DefaultMarkdown $markdownConverter;

    public function __construct()
    {
        $this->markdownConverter = new DefaultMarkdown();
    }


    /**
     * @Route("/index", name="rfc_index", methods="GET")
     */
    public function index(EntityManagerInterface $entityManager, PaginatorInterface $paginator, Request $request) :Response
    {
        $repository = $entityManager->getRepository(Status::class);
        $states = $repository->findSorted();

        // Build default Status filter array
        $default_states = array(
            'C__STATUS__INCOME',
            'C__STATUS__CAB',
            'C__STATUS__CMOK',
            'C__STATUS__CABOK',
            'C__STATUS__CMDB',
            'C__STATUS__TRANSFERED',
            'C__STATUS__DRAFT',
            'C__STATUS__VALIDATION',
            'C__STATUS__BPO'

        );

        foreach ($states as $state) {
            if (in_array($state['const'], $default_states)) {
                $default_status_filter[] = $state['id'];
            }
        }
        // End build default Status filter array
        
        $repository = $entityManager->getRepository(Roles::class);
        $role_cm = $repository->findOneBy(['const' => 'C__ROLES__CM']);
        if ($role_cm) {
            $changemangagers = $role_cm->getUsers();
        } else {
            $changemangagers = NULL;
        }

        $repository = $entityManager->getRepository(User::class);
        $users = $repository->findBy([], ['lastname' => 'ASC']);

        $repository = $entityManager->getRepository(Costcenters::class);
        $costcenters = $repository->findBy([], ['title' => 'ASC']);

        $repository = $entityManager->getRepository(BusinessProcesses::class);
        $business_processes = $repository->findBy([], ['title' => 'ASC']);

        $repository = $entityManager->getRepository(Projects::class);
        $projects = $repository->findBy([], ['title' => 'ASC']);

        $repository = $entityManager->getRepository(Authorizations::class);
        $authorizations = $repository->findBy([], ['title' => 'ASC']);

        $filter = array(
            "status" => "",
            "creator" => "",
            "classificator" => "",
            "referenced_ticketid" => "",
            "costcenter" => "",
            "business_process" => "",
            "project" => "",
            "id" => "",
            "authorizationtype" => "",
            "title" => "",
            "change_after" => "",
            "change_before" => "",
        );

        $sort = array(
            "by" => "created",
            "direction" => "DESC"
        );

        if ($request->query->get('sort_by')) {
            $sort["by"] = $request->query->get('sort_by');
        }
        if ($request->query->get('sort_direction')) {
            $sort["direction"] = $request->query->get('sort_direction');
        }

        foreach ($filter as $parameter => $value) {
            if ($request->query->get($parameter)) {
                $filter[$parameter] = $request->query->get($parameter);
            } else {
                $filter[$parameter] = $value;
            }
        }

        if (!in_array('C__ROLES__CM', $this->getUser()->getRoles())) {
            $filter['creator'] = $this->getUser()->getId();
        }

        //dd($filter);


        $repository = $entityManager->getRepository(Rfc::class);
        //$rfcs = $repository->findByFilter($filter);
        $queryBuilder = $repository->getWithSearchQueryBuilder($filter, $default_status_filter, $sort);

        $pagination = $paginator->paginate(
            $queryBuilder, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );
        $pagination->setTemplate('@KnpPaginator/Pagination/twitter_bootstrap_v4_pagination.html.twig');

        return $this->render('rfc/index.html.twig', array(
            'rfcs' => $pagination,
            'states' => $states,
            'changemanagers' => $changemangagers,
            'users' => $users,
            'costcenters' => $costcenters,
            'business_processes' => $business_processes,
            'projects' => $projects,
            'authorizations' => $authorizations,
            'filter' => $filter,
            'sort' => $sort,
        ));
    }

    /**
     * @Route("/rfc/new", name="rfc_new")
     */
    public function new(EntityManagerInterface $entityManager) {
        //$this->denyAccessUnlessGranted('ROLE_ADMIN');
        $repository = $entityManager->getRepository(Status::class);
        $status = $repository->findOneBy(['const' => 'C__STATUS__DRAFT']);
        $repository = $entityManager->getRepository(User::class);
        $user = $repository->findOneBy(['username' => $this->getUser()->getUsername()]);
        $repository = $entityManager->getRepository(Datetypes::class);
        $datetype = $repository->findOneBy(['const' => 'C__DATETYPE__WISH']);
        $repository = $entityManager->getRepository(Priorities::class);
        $priority = $repository->findOneBy(['const' => 'C__PRIORITY__MIDDLE']);


        $rfc = new Rfc();
        $rfc->setTitle('')
            ->setAffectedComponents('')
            ->setAffectedComponentsstore('')
            ->setCommentary(NULL)
            ->setConsequences('')
            ->setCreator($user)
            ->setDescription('')
            ->setDepartment($user->getDepartment())
            ->setStatus($status)
            ->setDatetype($datetype)
            ->setDate(new \DateTime())
            ->setStatus($status)
            ->setCreated(new \DateTime())
            ->setAttachment('')
            ->setPriority($priority)
        ;

        $entityManager->persist($rfc);

        $entityManager->flush();



        return $this->redirectToRoute('rfc_details', array('id' => $rfc->getId()));
    }

    /**
     * @Route("/rfc/{id<(\d)+>}", name="rfc_details", methods="GET")
     */
    public function show($id, EntityManagerInterface $entityManager, LoggerInterface $logger) :Response
    {
        $repository = $entityManager->getRepository(Rfc::class);
        $rfc = $repository->find($id);
        /** @var Rfc|null $rfc */
        if (!$rfc) {
            throw $this->createNotFoundException('Gibts nicht :-(');
        }
        $repository = $entityManager->getRepository(User::class);
        $users = $repository->findBy(["locked" => null], ["lastname" => "ASC"]);

        $repository = $entityManager->getRepository(Costcenters::class);
        $costcenters = $repository->findBy([], ["title" => "ASC"]);

        $repository = $entityManager->getRepository(BusinessProcesses::class);
        $business_processes = $repository->findBy([], ["title" => "ASC"]);
        
        $repository = $entityManager->getRepository(Datetypes::class);
        $date_types = $repository->findBy([], ["title" => "ASC"]);

        // fetch tts config
        $repository = $entityManager->getRepository(Config::class);
        $items = $repository->findBy(["group_name" => "tts"]);
        foreach ($items as $item) {
            $tts_config[$item->getConfigKey()] = unserialize($item->getConfigValue());
        }

        // Fetch i-doit Object Groups and affected_components
        $affected_components = array();

        $repository = $entityManager->getRepository(Config::class);
        $items      = $repository->findBy(["group_name" => "i-doit"]);

        $repository = $entityManager->getRepository(Roles::class);
        $role_cm = $repository->findOneBy(['const' => 'C__ROLES__CM']);

        foreach ($items as $item) {
            $config[$item->getConfigKey()] = unserialize($item->getConfigValue());
        }
        $url_parts = parse_url($config['idoit_url']);

        if (isset($url_parts["port"])) {
            $config['port'] = (int)$url_parts["port"];
        } elseif (isset($url_parts["scheme"]) && $url_parts["scheme"] == "https") {
            $config['port'] = 443;
        } else {
            $config['port'] = 80;
        }


        if ($this->getUser()->getLanguage() == '') {
            $config["api_lang"] = 'en';
        } else {
            $config["api_lang"] = $this->getUser()->getLanguage();
        };

        if ($config['username'] != '' && $config['password'] != '' && $config['idoit_url'] != '') {
            $idoit_config = array(
                API::URL                      => $config['idoit_url'] . '/src/jsonrpc.php',
                API::PORT                     => $config['port'],
                API::KEY                      => $config['api_key'],
                API::USERNAME                 => $config['username'],
                API::PASSWORD                 => $config['password'],
                API::LANGUAGE                 => $config["api_lang"],
                API::BYPASS_SECURE_CONNECTION => true
            );
            $api = new API($idoit_config);
            try {
                $api->login();
            } catch (\Exception $exception) {
                $logger->error($exception->getMessage());
            }
            if ($api->isLoggedIn()) {
                $cmdbObjectTypes = new CMDBObjectTypes($api);
                $object_types = $cmdbObjectTypes->read();

                if (is_array(json_decode($rfc->getAffectedComponentsstore())) && count(json_decode($rfc->getAffectedComponentsstore())) > 0) {
                    $cmdbObjects = new CMDBObjects($api);
                    $affected_components = $cmdbObjects->readByIDs(json_decode($rfc->getAffectedComponentsstore()));
                }

            } else {
                $object_types = array();
            }
        } else {
            $object_types = array();
        }
        // End Fetch i-doit Object Groups

        // Get Attachments
        $attachments = json_decode($rfc->getAttachment(), true);
        if (json_last_error_msg() and json_last_error_msg() != "No error") {
            if ($rfc->getAttachment() != '') {
                $attachments = [$rfc->getAttachment()];
            } else {
                $attachments = array();
            }
        }

        $repository = $entityManager->getRepository(Reasons::class);
        $reasons = $repository->findBy(["isActive" => true], ["title" => "ASC"]);

        $repository = $entityManager->getRepository(Priorities::class);
        $priorities = $repository->findBy([], ["id" => "ASC"]);

        $repository = $entityManager->getRepository(Projects::class);
        $projects = $repository->findBy([], ["title" => "ASC"]);

        $repository = $entityManager->getRepository(RfcClassifications::class);
        $rfc_classification = $repository->findOneBy(['rfc' => $rfc]);

        $repository = $entityManager->getRepository(Ressourcetypes::class);
        $ressources = $repository->findAll();

        $repository = $entityManager->getRepository(Units::class);
        $units = $repository->findAll();

        $repository = $entityManager->getRepository(Categories::class);
        $categories = $repository->findBy([], ['id' => 'ASC']);

        $repository = $entityManager->getRepository(Authorizations::class);
        $authorizationtypes = $repository->findAll();

        $repository = $entityManager->getRepository(RfcAuthorizations::class);
        $rfc_authorization = $repository->findOneBy(['classification' => $rfc_classification]);
        
        $repository = $entityManager->getRepository(Status::class);
        $dispatch_status = $repository->findOneBy(['const' => 'C__STATUS__TRANSFERED']); // Status change has happened when notice is stored
        $bpo_status      = $repository->findOneBy(['const' => 'C__STATUS__BPO']);
        
        $repository = $entityManager->getRepository(RfcNotices::class);
        $rfc_notices = $repository->findBy(['rfc' => $rfc], ['created' => 'DESC']);
        $dispatch_notice = $repository->findOneBy(['rfc' => $rfc, 'status' => $dispatch_status]);
        $bpo_notice      = $repository->findOneBy(['rfc' => $rfc, 'status' => $bpo_status]);

        
        $repository = $entityManager->getRepository(RfcWorkflows::class);
        $rfc_workflows = $repository->findByRfc($rfc->getId());

        // Ticket
        // Ticket association Logic
        $ticket = array(
            "id"       => "",
            "group_id" =>  "",
            "state_id" => "",
            "number"   => "",
            "title"    => "",
            "owner_id" => "",
            "url"      => "",
        );
        if ($rfc->getReferencedTicketid()) {
            $zammad = new ZammadController();
            $zammad_config = $zammad->get_config($entityManager);

            if ($zammad_config["active"] == 1 && $zammad_config["driver"] == "Zammad") {
                // first try to find ticket with id
                $ticket = $zammad->get_ticket_by_id($rfc->getReferencedTicketid(), $entityManager)->getValues();

                if (isset($ticket['id'])) {
                    $ticket['url'] = str_replace('%id%', $ticket['id'], $zammad_config['pattern']);
                } else {
                    $cleanTicketId = preg_replace("/[^0-9]/", '', $rfc->getReferencedTicketid());
                    $ticket['id'] = $rfc->getReferencedTicketid();
                    $ticket['url'] = str_replace('%id%', $cleanTicketId, $zammad_config['pattern']);
                    $ticket['number'] = '';
                }
            } else {
                $ticket['id'] = $rfc->getReferencedTicketid();
            }

        }


        // If no workflows are created, but a Standard change is assigned, grep the Workflows from Standard change
        if (count($rfc_workflows) == 0 && $rfc_classification && $rfc_classification->getStandardchange()) {
            //dd($rfc_classification->getStandardchange()->getWorkflowdata());
            $standardchange_workflowdata = json_decode($rfc_classification->getStandardchange()->getWorkflowdata());

            if (json_last_error_msg() == "No error") {
                //dd($standardchange_workflowdata);
                //$standardchange_workflowdata = uasort($standardchange_workflowdata, [$this, 'cmp']);
                $repository = $entityManager->getRepository(ImplementationStatus::class);
                $implementation_status = $repository->findOneBy(['const' => 'C_STATUS_NEW']);
                foreach ($standardchange_workflowdata as $task) {
                    //dd($task);
                    if (is_array($task) && $task[0] && $task[0]->value) {
                        $workflow_id = $task[0]->value;
                        $rfc_workflow[$workflow_id] = new RfcWorkflows();
                        $rfc_workflow[$workflow_id]->setCreator($this->getUser())
                            ->setTitle($task[1]->value)
                            ->setDescription($task[2]->value)
                            ->setStartDate(new \DateTime())
                            ->setEndDate((new \DateTime())->add(new \DateInterval('P1D')))
                            ->setObjects('')
                            ->setPersons('')
                            ->setRfc($rfc)
                            ->setImplementationStatus($implementation_status)
                        ;
                        // Wenn ein Parent task eingetragen ist, dann muss vorher schon der Parent task angelegt worden sein
                        // Außerdem sollte er in Array mit dem key verspeichert sein der der Standardchange workflow_id entspricht
                        if ($task[3] && $task[3]->value) {
                            $parent_workflow = $rfc_workflow[$task[3]->value];
                            $rfc_workflow[$workflow_id]->setParent($parent_workflow);
                        }
                        $entityManager->persist($rfc_workflow[$workflow_id]);
                        $entityManager->flush();
                    }
                }
            }
        }

        // Re-fetch Workflows
        $repository = $entityManager->getRepository(RfcWorkflows::class);
        $rfc_workflows = $repository->findByRfc($rfc->getId());

        $repository = $entityManager->getRepository(RfcStandardchanges::class);
        $standardchanges = $repository->findBy(['active' => 1], ['title' => 'ASC']);


        // Get Authorization Attachments
        $authorization_attachments = array();
        if ($rfc_authorization) {
            $authorization_attachments = json_decode($rfc_authorization->getAttachment(), true);
            if (json_last_error_msg() and json_last_error_msg() != "No error") {
                if ($rfc_authorization->getAttachment() != '') {
                    $authorization_attachments = [$rfc_authorization->getAttachment()];
                }
            }
        }
        $repository = $entityManager->getRepository(RfcBpoFeedback::class);
        $rfcBpoFeedback = $repository->findOneBy(['rfc' => $rfc]);

        $system = new SystemController();
        $system_config = $system->get_config($entityManager);

        $user_is_unpriviledged = true;
        if (in_array('C__ROLES__CM', $this->getUser()->getRoles()) or in_array('C__ROLES__CAB', $this->getUser()->getRoles())) {
            $user_is_unpriviledged = false;
        }

        // Check authorization
        if ($user_is_unpriviledged && ($this->getUser()->getID() != $rfc->getCreator()->getId() && $this->getUser() != $rfcBpoFeedback->getDelegatee())) {
            throw new \Exception('Error: You are not allowed to view this RfC.');
        }

        $additional_fields = [];
        $customfields = json_decode($rfc->getCustomfields(), true);

        // get configured fields from selected reason
        if ($rfc->getReason() && $customfields) {
            $fieldDefinitions = $rfc->getReason()->getFields();
            $fieldsIndex = 0;
            foreach ($fieldDefinitions as $fieldDefinition) {
                foreach ($fieldDefinition as $definition) {
                    $additional_fields[$fieldsIndex][$definition["name"]] = $definition["value"];
                    if ($definition["name"] == "id") {
                        if (is_array($customfields) && array_key_exists($definition["value"], $customfields)) {
                            $additional_fields[$fieldsIndex]["value"] = $customfields[$definition["value"]];
                        }
                    }
                }
                $fieldsIndex++;
            }
        }


        return $this->render('rfc/show.html.twig', array(
            'rfc' => $rfc,
            'users' => $users,
            'costcenters' => $costcenters,
            'business_processes' => $business_processes,
            'date_types' => $date_types,
            'object_types' => $object_types,
            'reasons' => $reasons,
            'priorities' => $priorities,
            'projects' => $projects,
            'affacted_components' => $affected_components,
            'attachments' => $attachments,
            'changemanagers' => $role_cm->getUsers(),
            'rfc_classification' => $rfc_classification,
            'ressources' => $ressources,
            'units' => $units,
            'categories' => $categories,
            'authorizationtypes' => $authorizationtypes,
            'rfc_notices' => $rfc_notices,
            'rfc_authorization' => $rfc_authorization,
            'authorization_attachments' => $authorization_attachments,
            'rfc_workflows' => $rfc_workflows,
            'config' => $config, // i-doit config
            'dispatch_notice' => $dispatch_notice,
            'bpo_notice' => $bpo_notice,
            'rfcBpoFeedback' => $rfcBpoFeedback,
            'standardchanges' => $standardchanges,
            'ticket' => $ticket,
            'tts_config' => $tts_config,
            'system_config' => $system_config,
            'customfields' => $additional_fields,
        ));
    }

    /**
     * @Route ("/rfc/finish", name="rfc_finish", methods="POST")
     */
    public function finish(EntityManagerInterface $entityManager, Request $request, MailerInterface $mailer) {
        $repository = $entityManager->getRepository(Rfc::class);
        $rfc = $repository->findOneBy(['id' => $request->request->get('rfc_id')]);

        if (!$rfc) {
            throw new \Exception('RfC does not exist');
        }

        $repository = $entityManager->getRepository(Status::class);
        $status = $repository->findOneBy(['const' => $request->request->get('status')]);

        $old_status_title = $rfc->getStatus()->getTitle();
        $new_status_title = $status->getTitle();
        $posted_message   = $request->request->get('commentary');
        $rfc_notice_content = <<<EOD
$posted_message

```
Previous state: $old_status_title
New state:      $new_status_title
```
EOD;


        $rfc_notice = new RfcNotices();
        $rfc_notice->setRfc($rfc)
            ->setTitle('Logbook entry')
            ->setContent($rfc_notice_content)
            ->setStatus($status)
            ->setCreated(new \DateTime())
            ->setAuthor($this->getUser())
        ;

        $entityManager->persist($rfc_notice);

        $rfc->setStatus($status);
        $rfc->setCommentary($request->request->get('commentary'));
        $entityManager->persist($rfc);
        $entityManager->flush();

        // Zammad Logic
        $zammad = new ZammadController();
        $zammad_config = $zammad->get_config($entityManager);
        if ($zammad_config["active"] == 1 && $rfc->getReferencedTicketid()) {
            //Update referenced Ticket
            if ($rfc->getStatus()->getConst() == 'C__STATUS__COMPLETE'){
                $zammad->remove_tag($rfc->getReferencedTicketid(), 'RfC_Status_Validation', $entityManager);
                $zammad->add_tag($rfc->getReferencedTicketid(), 'RfC_Status_Finished', $entityManager);
            } elseif ($rfc->getStatus()->getConst() == 'C__STATUS__FAILED'){
                $zammad->remove_tag($rfc->getReferencedTicketid(), 'RfC_Status_Validation', $entityManager);
                $zammad->add_tag($rfc->getReferencedTicketid(), 'RfC_Status_Failed', $entityManager);
            } elseif ($rfc->getStatus()->getConst() == 'C__STATUS__CABOK') {
                $zammad->remove_tag($rfc->getReferencedTicketid(), 'RfC_Status_Validation', $entityManager);
                $zammad->add_tag($rfc->getReferencedTicketid(), 'RfC_Status_Implementation', $entityManager);
            }
            $zammad->create_article($rfc->getReferencedTicketid(), 'Validation Comment', $request->request->get('commentary'), $entityManager);
        }
        if ($rfc->getStatus()->getConst() == 'C__STATUS__CABOK') {
            /**
             * send E-Mail to CM -> Begin Implementation
             */
            $repository = $entityManager->getRepository(RfcClassifications::class);
            $rfc_classification = $repository->findOneBy(['rfc' => $rfc]);
            $cm = $rfc_classification->getClassificator();
            $firstname = $cm->getFirstname();
            $deeplink = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . '/rfc/' . $rfc->getId();

            $message = <<<EOD
Hello $firstname,

a Request for Change has been sent back due to imcomplete implementation.

Please open the [RfC Manager]($deeplink) and begin with the implementation.
EOD;
            $message = $this->markdownConverter->convert($message);
            $email = new EmailController();
            $email->send_email($entityManager, $cm, $message, '[RfC# ' . $rfc->getId() . '] A Request for Change has been validated as incomplete', $mailer);

        }

        return $this->redirectToRoute('rfc_details', array('id' => $rfc->getId()));
    }


    /**
     * @Route ("/rfc/save", name="rfc_save", methods="POST")
     * @throws \Exception
     */
    public function save(EntityManagerInterface $entityManager, Request $request, MailerInterface $mailer, UserInterface $loggedin_user, LoggerInterface $logger) {
        /** @var UploadedFile $uploadedFile */
        $attachment = array();
        $uploadedFiles = ($request->files->get('attachment'));
        $destination = $this->getParameter('kernel.project_dir').'/uploads/';
        if (is_array($uploadedFiles)) {
            foreach ($uploadedFiles as $uploadedFile) {
                $newFilename = uniqid() . '-' . $uploadedFile->getClientOriginalName();
                ($uploadedFile->move($destination, $newFilename));
                $attachment[] = array(
                    'original_name' => $uploadedFile->getClientOriginalName(),
                    'saved_name'    => $newFilename
                );
            }
        }

        $repository = $entityManager->getRepository(User::class);
        $creator = $repository->findOneBy(['id' => $request->request->get('creator')]);
        $department = $creator->getDepartment();
        $repository = $entityManager->getRepository(Costcenters::class);
        $costcenter = $repository->findOneBy(['id' => $request->request->get('costcenters')]);
        $repository = $entityManager->getRepository(Reasons::class);
        $reason = $repository->findOneBy(['id' => $request->request->get('reason')]);
        $repository = $entityManager->getRepository(Priorities::class);
        $priority   = $repository->findOneBy(['id' => $request->request->get('priority')]);
        $repository = $entityManager->getRepository(Datetypes::class);
        $datetype   = $repository->findOneBy(['id' => $request->request->get('date_type')]);
        $repository = $entityManager->getRepository(BusinessProcesses::class);
        $business_process = $repository->findOneBy(['id' => $request->request->get('business_process')]);
        $repository = $entityManager->getRepository(Projects::class);
        $project    = $repository->findOneBy(['id' => $request->request->get('project')]);
        $repository = $entityManager->getRepository(Status::class);
        $status     = $repository->findOneBy(['const' => $request->request->get('status')]);


        $repository = $entityManager->getRepository(Rfc::class);
        $rfc = $repository->findOneBy(['id' => $request->request->get('id')]);

        if (!$rfc) {
            $rfc = new Rfc();
            $rfc->setCreated(new \DateTime());
            $rfc->setStatus($status);
        }

        // if we enter a new status, lets check if we have to send notifications
        $deeplink = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . '/rfc/' . $rfc->getId();
        $old_status_const = $rfc->getStatus()->getConst();
        $old_status_title = $rfc->getStatus()->getTitle();
        $new_status_const = $status->getConst();
        $new_status_title = $status->getTitle();


        // Get allready stored attachments
        $stored_attachments = json_decode($rfc->getAttachment(), true);
        if (json_last_error_msg() and json_last_error_msg() != "No error") {
            if ($rfc->getAttachment() != '') {
                $stored_attachments = [['original_name' => $rfc->getAttachment()]];
            } else {
                $stored_attachments = array();
            }
        }
        $attachment = array_merge($attachment, $stored_attachments);

        // Ticket association Logic
        //if status is C__STATUS__DRAFT or C__STATUS__INCOME
        // lets create a new ticket
        $zammad = new ZammadController();
        $zammad_config = $zammad->get_config($entityManager);

        if ($zammad_config["active"] == 1 && $zammad_config["driver"] == "Zammad") {
            if (in_array($new_status_const, ['C__STATUS__DRAFT', 'C__STATUS__INCOME'])) {
                if ($request->request->get('referenced_ticket')) {
                    // if ticket # is submitted we always update regardless if Ticket exists or not
                    $cleanTicketNumber = preg_replace("/[^0-9]/", '', $request->request->get('referenced_ticket'));
                    $ticket = $zammad->get_ticket_by_number($cleanTicketNumber, $entityManager);

                    if ($ticket) {
                        $rfc->setReferencedTicketid($cleanTicketNumber);
                    }
                    else {
                        $rfc->setReferencedTicketid($request->request->get('referenced_ticket'));
                    }
                } else {
                    // No ticket # was submitted, so let’s create a new one

                    if ($costcenter) {
                        $responsible = $costcenter->getResponsible();
                    } else {
                        $responsible = false;
                    }
                    try {
                        $ticket = $zammad->create_ticket($request->request->get('title'), $request->request->get('description'), $creator, $responsible, $entityManager, $logger);
                        $rfc->setReferencedTicketid($ticket->getId());
                    }
                    catch (\Exception $e) {
                        throw new \Exception('Could not create Zammad Ticket');
                    }

                }
            }

            // Here we have a ticket id so lets tag the ticket
            if ($rfc->getReferencedTicketid()) {
                $zammad->add_tag($rfc->getReferencedTicketid(), 'RfC_' . $rfc->getId(), $entityManager);
            }
        } elseif ($request->request->get('referenced_ticket')){
            $rfc->setReferencedTicketid($request->request->get('referenced_ticket'));
        }



        $rfc->setTitle($request->request->get('title'))
            ->setAffectedComponents('')
            ->setAffectedComponentsstore($request->request->get('affectedComponentsstore'))
            ->setCommentary(NULL)
            ->setConsequences($request->request->get('consequences'))
            ->setCreator($creator)
            ->setCostcenter($costcenter)
            ->setDescription($request->request->get('description'))
            ->setAttachment(json_encode($attachment))
            ->setDepartment($department)
            ->setStatus($status)
            ->setPriority($priority)
            ->setDatetype($datetype)
            ->setDate(new \DateTime($request->request->get('date')))
            ->setReason($reason)
            ->setCustomfields(json_encode($request->request->get('additional_fields')))
            ->setCommentary($request->request->get('commentary'))
            ->setBusinessProcess($business_process)
            ->setProject($project)
        ;

        $entityManager->persist($rfc);

        $rfc_notice_content = <<<EOD
```
Previous state: $old_status_title
New state:      $new_status_title
```
EOD;


        $rfc_notice = new RfcNotices();
        $rfc_notice->setRfc($rfc)
            ->setTitle('Logbook entry')
            ->setContent($rfc_notice_content)
            ->setStatus($status)
            ->setCreated(new \DateTime())
            ->setAuthor($loggedin_user)
        ;
        $entityManager->persist($rfc_notice);

        $entityManager->flush();

        if ($old_status_const != $new_status_const) {
            /*
             * C__STATUS__INCOME	RFC wurde eingestellt, aber noch nicht weiter bearbeitet (dispatching)	1	NULL
             * C__STATUS__CAB	RFC liegt dem CM oder CAB zur Authorisierung vor.	1	NULL
             * C__STATUS__CMOK	RFC wurde vom CM freigegeben.	1	NULL
             * C__STATUS__CABOK	RFC wurde von CAB freigegeben.	1	NULL
             * C__STATUS__CMKO	RFC wurde vom CM abgelehnt.	1	NULL
             * C__STATUS__CABKO	RFC wurde von CAB abgelehnt.	1	NULL
             * C__STATUS__CMDB	RFC wurde als Workflow an die CMDB übertragen.	1	NULL
             * C__STATUS__COMPLETE	RFC wurde erfolgreich abgeschlossen.	1	NULL
             * C__STATUS__CANCEL	RFC wurde abgebrochen.	1	NULL
             * C__STATUS__TRANSFERED	RFC wurde von einem CM zur Klassifizierung übernommen.	1	NULL
             * C__STATUS__DRAFT	RfC wird gerade erstellt 	0	NULL
             * C__STATUS__VALIDATION	RfC Implementierung ist abgschlossen und wartet auf Validierung	0	NULL
             * C__STATUS__BPO	NULL	0	NULL
             *
             */
            if ($old_status_const == 'C__STATUS__DRAFT' && $new_status_const == 'C__STATUS__INCOME') {


                if ($rfc->getCostcenter() && !$rfc->getRfcClassification()) { // A costcenter has been choosen AND NO classification created yet
                /*
                 * We have a new RfC waiting for Section responsible to submit it to a Change Manager
                 * Send E-Mail to Section Responsible
                 * Create Ticket if not exists
                 * Tag Ticket with rfc_id
                 * Tag Ticket with RFC_STATUS_DISPATCHING
                 */
                    if ($zammad_config["active"] == 1) {
                        $zammad->add_tag($rfc->getReferencedTicketid(), 'RfC_Status_Dispatching', $entityManager);
                    }
                    $costcenter = $rfc->getCostcenter();
                    $costcenter_title = $costcenter->getTitle();
                    $responsible = $costcenter->getResponsible();
                    $firstname = $responsible->getFirstname();
                    $message = <<<EOD
Hello $firstname,

a Request for Change concerning the section $costcenter_title, where you are 
assigned as responsible person, has been submitted.

Please open the [RfC Manager]($deeplink) and assign the Request to a Change Manager.
EOD;
                    $message = $this->markdownConverter->convert($message);
                    $email = new EmailController();
                    $email->send_email($entityManager, $responsible, $message, '[RfC# ' . $rfc->getId() . '] A Request for Change in your section has been submitted', $mailer);
                } else {
                    /*
                     * we have a new RfC without a section OR we allready have a classification
                     * Send E-Mail to all Change Managers
                     * Skip Status Income an set direct to transfered
                     */
                    $repository = $entityManager->getRepository(Status::class);
                    $status     = $repository->findOneBy(['const' => 'C__STATUS__TRANSFERED']);
                    $rfc->setStatus($status);
                    $entityManager->persist($rfc);
                    $entityManager->flush();

                    if ($zammad_config["active"] == 1) {
                        $zammad->add_tag($rfc->getReferencedTicketid(), 'RfC_Status_Dispatching', $entityManager);
                    }

                    if (!$rfc->getRfcClassification()) {
                        // We have no classification yet - send E-Mail to all CM
                        $repository = $entityManager->getRepository(Roles::class);
                        $role_cm = $repository->findOneBy(['const' => 'C__ROLES__CM']);
                        $changemanagers = $role_cm->getUsers();
                    } else {
                        // We DO have a classification - send E-Mail only to classificator
                        $changemanagers = [$rfc->getRfcClassification()->getClassificator()];
                    }
                    foreach ($changemanagers as $changemanager) {
                        $firstname = $changemanager->getFirstname();
                        $message = <<<EOD
Hello $firstname,

a Request for Change has been submitted. Please open the [RfC Manager]($deeplink) and classify the Request.
EOD;
                        $message = $this->markdownConverter->convert($message);
                        $email = new EmailController();
                        $email->send_email($entityManager, $changemanager, $message, '[RfC# ' . $rfc->getId() . '] A Request for Change has been submitted', $mailer);
                    }

                }

            } else {
                throw new \Exception('Status to Status change in this combination is not allowed');
            }
        }

        return $this->redirectToRoute('rfc_details', array('id' => $rfc->getId()));
    }

    /**
     * @Route("/rfc/delete/{id<(\d)+>}", name="rfc_delete", methods="GET")
     */
    public function delete($id, EntityManagerInterface $entityManager) :Response
    {
        $repository = $entityManager->getRepository(Rfc::class);
        $rfc = $repository->find($id);
        /** @var Rfc|null $rfc */
        if (!$rfc) {
            throw $this->createNotFoundException('Invalid RfC id');
        }

        if ($rfc->getStatus()->getConst() == 'C__STATUS__DRAFT') {
            if ($rfc->getCreator() == $this->getUser()) {
                $entityManager->remove($rfc);
                $entityManager->flush();
            } else {
                throw new \Exception('Only the creator of an RfC is allowed to delete it');
            }
        } else {
            throw new \Exception('Only in state draft a RfC may be deleted');
        }


        return $this->redirectToRoute('rfc_index');
    }

    /**
     * @Route ("/rfc/attachment/download/{saved_name}/{original_name}", name="rfc_attachment_download", methods="GET")
     */
    public function download($saved_name, $original_name) {
        //dd($saved_name);
        $file = $this->getParameter('kernel.project_dir').'/uploads/'.$saved_name;

        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'.basename($original_name).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            readfile($file);
            exit;
        }
    }

    private function cmp($a, $b) {
        // Beide haben keine paren id
        if (!$a->parent && !$b->parent) {
            return 0;
        }
        // nur einer hat eine Parent id -> denjenigen mit nach oben sortieren
        if (!$a->parent) {
            return -1;
        }
        if (!$b->parent) {
            return 1;
        }
        // beide haben die selbe paren id -> alles bleibt gleich
        if ($a->parent->id == $b->parent->id) {
            return 0;
        }

        // einer von beiden hat den anderen als parent
        if ($a->parent->id == $b->id) {
            return 1;
        }
        if ($b->parent->id == $a->id) {
            return -1;
        }
        return 0;

    }


}
