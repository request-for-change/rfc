<?php

namespace App\Controller;

use App\Entity\Rfc;
use App\Entity\RfcAuthorizations;
use App\Entity\RfcClassifications;
use App\Entity\RfcNotices;
use App\Entity\Status;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Extra\Markdown\DefaultMarkdown;

class RfcAuthorizationsController extends AbstractController
{
    private DefaultMarkdown $markdownConverter;

    public function __construct()
    {
        $this->markdownConverter = new DefaultMarkdown();
    }
    
    /**
     * @Route("/rfc/authorizations", name="rfc_authorizations")
     */
    public function index(): Response
    {
        return $this->render('rfc_authorizations/index.html.twig', [
            'controller_name' => 'RfcAuthorizationsController',
        ]);
    }

    /**
     * @Route ("/rfc/authorizations/save", name="rfc_authorizations_save")
     */
    public function save(EntityManagerInterface $entityManager, Request $request, MailerInterface $mailer) {
        /** @var UploadedFile $uploadedFile */
        $attachment = array();
        $uploadedFiles = ($request->files->get('attachment'));
        $destination = $this->getParameter('kernel.project_dir').'/uploads/';
        if (is_array($uploadedFiles)) {
            foreach ($uploadedFiles as $uploadedFile) {
                $newFilename = uniqid() . '-' . $uploadedFile->getClientOriginalName();
                ($uploadedFile->move($destination, $newFilename));
                $attachment[] = array(
                    'original_name' => $uploadedFile->getClientOriginalName(),
                    'saved_name' => $newFilename
                );
            }
        }
        $zammad = new ZammadController();
        $zammad_config = $zammad->get_config($entityManager);

        $repository = $entityManager->getRepository(Rfc::class);
        $rfc = $repository->findOneBy(['id' => $request->request->get('rfc_id')]);
        $repository = $entityManager->getRepository(RfcClassifications::class);
        $rfc_classification = $repository->findOneBy(['rfc' => $rfc]);
        $repository = $entityManager->getRepository(Status::class);

        $status_const = $request->request->get('status');
        if ($status_const == 'C__STATUS__CABOK' && $request->request->get('decision') == '0') {
            // we have only one value for submit decision so we change the status to C__STATUS__CABOK if decision is 0
            $status_const = 'C__STATUS__CABKO';
        }
        $status = $repository->findOneBy(['const' => $status_const]);

        $repository = $entityManager->getRepository(RfcAuthorizations::class);
        $rfc_authorization = $repository->findOneBy(['classification' => $rfc_classification]);
        
        if (!$rfc_authorization) {
            $rfc_authorization = new RfcAuthorizations();
            $rfc_authorization->setCreated(new \DateTime())
                ->setAuthor($this->getUser())
                ->setClassification($rfc_classification);
        }

        // Get allready stored attachments
        $stored_attachments = json_decode($rfc_authorization->getAttachment(), true);
        if (json_last_error_msg() and json_last_error_msg() != "No error") {
            if ($rfc_authorization->getAttachment() != '') {
                $stored_attachments = [['original_name' => $rfc_authorization->getAttachment()]];
            } else {
                $stored_attachments = array();
            }
        }
        $attachment = array_merge($attachment, $stored_attachments);

        //dd($request->request->get('target_date'));
        if($request->request->get('target_date')) {
            $target_date = new \DateTime($request->request->get('target_date'));
        } else {
            $target_date = null;
        }

        if (ctype_digit($request->request->get('decision'))) {
            $rfc_authorization->setEstablishment($request->request->get('establishment'))
                ->setDecision($request->request->get('decision'))
                ->setTargetDate($target_date)
                ->setAttachment(json_encode($attachment));
            $entityManager->persist($rfc_authorization);
        }



        // rfc exists -> assigning new status
        $old_status_const = $rfc->getStatus()->getConst();
        $old_status_title = $rfc->getStatus()->getTitle();
        $new_status_const = $status->getConst();
        $new_status_title = $status->getTitle();


        //set RfC Status
        $rfc->setStatus($status);
        $entityManager->persist($rfc);

        $posted_message = $request->request->get('message');

        $rfc_notice_content = <<<EOD
$posted_message

```
Previous state: $old_status_title
New state:      $new_status_title
```

EOD;


        $rfc_notice = new RfcNotices();
        $rfc_notice->setRfc($rfc)
            ->setTitle('Logbook entry')
            ->setContent($rfc_notice_content)
            ->setStatus($status)
            ->setCreated(new \DateTime())
            ->setAuthor($this->getUser())
        ;
        $entityManager->persist($rfc_notice);
        $entityManager->flush();

        // Send Notifications if necessary
        if ($old_status_const != $new_status_const) {
            $deeplink = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . '/rfc/' . $rfc->getId();
            if ($new_status_const == 'C__STATUS__CABOK') {
                /**
                 * send E-Mail to CM -> Begin Implementation
                 */
                $cm = $rfc_classification->getClassificator();
                $firstname = $cm->getFirstname();

                $message = <<<EOD
Hello $firstname,

a Request for Change has been approved.

Please open the [RfC Manager]($deeplink) and begin with the implementation.
EOD;
                $message = $this->markdownConverter->convert($message);
                $email = new EmailController();
                $email->send_email($entityManager, $cm, $message, '[RfC# ' . $rfc->getId() . '] A Request for Change has been approved', $mailer);

                if ($zammad_config["active"] == 1 ) {
                    $zammad->remove_tag($rfc->getReferencedTicketid(), 'RfC_Status_Authorization', $entityManager);
                    $zammad->remove_tag($rfc->getReferencedTicketid(), 'RfC_Status_Classification', $entityManager);
                    $zammad->add_tag($rfc->getReferencedTicketid(), 'RfC_Status_Implementation', $entityManager);
                }

            } elseif ($new_status_const == 'C__STATUS__CABKO') {
                /**
                 * send E-Mail to Initiator -> Request was rejected
                 */
                $creator = $rfc->getCreator();
                $firstname = $creator->getFirstname();
                $rfc_title = $rfc->getTitle();

                $message = <<<EOD
Hello $firstname,

your Request for Change "**$rfc_title**" has been rejected.

Please open the [RfC Manager]($deeplink) to view the history of this RfC.
EOD;

                $message = $this->markdownConverter->convert($message);
                $email = new EmailController();
                $email->send_email($entityManager, $creator, $message, '[RfC# ' . $rfc->getId() . '] A Request for Change has been rejected', $mailer);

                if ($zammad_config["active"] == 1 ) {
                    $zammad->remove_tag($rfc->getReferencedTicketid(), 'RfC_Status_Authorization', $entityManager);
                    $zammad->add_tag($rfc->getReferencedTicketid(), 'RfC_Status_Rejected', $entityManager);
                }

            } elseif ($new_status_const == 'C__STATUS__TRANSFERED') {
                /**
                 * send E-Mail to CM -> Request needs new Classification
                 */
                $cm = $rfc_classification->getClassificator();
                $firstname = $cm->getFirstname();
                $posted_message = $request->request->get('message');

                $message = <<<EOD
$posted_message

Please open the [RfC Manager]($deeplink) and re-classify the request.
EOD;
                $message = $this->markdownConverter->convert($message);
                $email = new EmailController();
                $email->send_email($entityManager, $cm, $message, '[RfC# ' . $rfc->getId() . '] A Request for Change needs your attention', $mailer);

                if ($zammad_config["active"] == 1 ) {
                    $zammad->remove_tag($rfc->getReferencedTicketid(), 'RfC_Status_Authorization', $entityManager);
                    $zammad->add_tag($rfc->getReferencedTicketid(), 'RfC_Status_Classification', $entityManager);
                }
            }
        }

        return $this->redirectToRoute('rfc_details', array('id' => $rfc->getId()));
    }

    /**
     * @Route ("/rfc/authorization/download/{saved_name}/{original_name}", name="rfc_authorization_download", methods="GET")
     */
    public function download($saved_name, $original_name) {
        //dd($saved_name);
        $file = $this->getParameter('kernel.project_dir').'/uploads/'.$saved_name;

        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'.basename($original_name).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            readfile($file);
            exit;
        }
    }
}
