<?php
/**
 * don’t panic it-services
 * User: chris
 * Date: 26.12.20
 * Time: 16:58
 */

namespace App\Controller;


use App\Entity\ImplementationStatus;
use App\Entity\Rfc;
use App\Entity\RfcWorkflows;
use App\Entity\Status;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Extra\Markdown\DefaultMarkdown;

class ApiController extends AbstractController
{
    private DefaultMarkdown $markdownConverter;

    public function __construct()
    {
        $this->markdownConverter = new DefaultMarkdown();
    }
    /**
     * @Route ("/api/getRFC/{id<(\d)+>}", name="get_rfc", methods="GET")
     */

    public function get_rfc($id, EntityManagerInterface $entityManager) {
        $repository = $entityManager->getRepository(Rfc::class);
        $results = $repository->getByCmdbObject($id);

        $rfcs = array(
            "affected_components"  => array(),
            "workflow_assignments" => array()
        );
        foreach ($results as $result ) {
            $rfc["id"] = $result->getId();
            $rfc["title"] = $result->getTitle();
            $rfc["status"] = $result->getStatus()->getTitle();

            $rfcs["affected_components"][] = $rfc;
        }

        $repository = $entityManager->getRepository(RfcWorkflows::class);
        $results = $repository->getByCmdbObject($id);

        foreach ($results as $result ) {
            $rfc["id"] = $result->getId();
            $rfc["title"] = $result->getTitle();
            $rfc["status"] = $result->getImplementationStatus()->getTitle();

            $rfcs["workflow_assignments"][] = $rfc;
        }

        return new JsonResponse($rfcs);
    }

    /**
     * @Route ("/api/zammad", name="zammad_api", methods="POST")
     */
    public function update_related(Request $request, LoggerInterface $logger, EntityManagerInterface $entityManager, MailerInterface $mailer) {

        $data = json_decode($request->getContent(), true);
        $logger->info('***********************************');
        $logger->info('Ticket ID: ' . $data['ticket']['id']);
        $logger->info('Ticket #: ' . $data['ticket']['number']);
        $logger->info('Ticket title: ' . $data['ticket']['title']);
        $logger->info('Owner: ' . $data['ticket']['owner']["email"]);
        $logger->info('State: ' . $data['ticket']['state']);

        // Search Rfc Entities for Ticket ID and update State

        // Search RfcWorkflows for Ticket ID and update State
        $repository = $entityManager->getRepository(RfcWorkflows::class);
        $rfcWorkflow = $repository->findOneBy(['referenced_ticketid' => $data['ticket']['id']]);

        if ($rfcWorkflow) { //$logger->debug($rfcWorkflow->getTitle());
            $repository = $entityManager->getRepository(ImplementationStatus::class);
            if ($data['ticket']['state'] == 'open') {
                $implementationStatus = $repository->findOneBy(['const' => 'C_STATUS_OPEN']);
            } elseif ($data['ticket']['state'] == 'closed') {
                $implementationStatus = $repository->findOneBy(['const' => 'C_STATUS_CLOSED']);
            }
            $rfcWorkflow->setImplementationStatus($implementationStatus);
            $entityManager->persist($rfcWorkflow);
            $entityManager->flush();
            $repository = $entityManager->getRepository(RfcWorkflows::class);

            if ($rfcWorkflow->getParent()) {
                $sibling_tasks = $repository->findBy(['parent' => $rfcWorkflow->getParent()->getId()]);
            } else {
                $sibling_tasks = $repository->findBy(['parent' => NULL, 'rfc' => $rfcWorkflow->getRfc()]);
            }

            $count_open_siblings = 0;
            if (is_array($sibling_tasks)) {
                foreach ($sibling_tasks as $sibling_task) {
                    if ($sibling_task->getImplementationStatus()->getConst() == 'C_STATUS_NEW' || $sibling_task->getImplementationStatus()->getConst() == 'C_STATUS_OPEN') {
                        $count_open_siblings++;
                    }

                }
            }

            $childWorkflows = $repository->findBy(['parent' => $rfcWorkflow->getId()]);
            $rfcWorkflowsController = new RfcWorkflowsController();
            if (count($childWorkflows) > 0 || $count_open_siblings > 0) {
                foreach ($childWorkflows as $childWorkflow) {
                    if ($childWorkflow->getImplementationStatus()->getConst() == 'C_STATUS_NEW') $rfcWorkflowsController->notify($childWorkflow, $entityManager, $request, $mailer, $logger);
                }
                foreach ($sibling_tasks as $sibling_task) {
                    if ($sibling_task->getImplementationStatus()->getConst() == 'C_STATUS_NEW') $rfcWorkflowsController->notify($sibling_tasks, $entityManager, $request, $mailer, $logger);
                }
            } else {
                // the last task is closed
                $repository = $entityManager->getRepository(Status::class);
                $status = $repository->findOneBy(['const' => 'C__STATUS__VALIDATION']);
                $rfc = $rfcWorkflow->getRfc();
                $rfc->setStatus($status);
                $entityManager->persist($rfc);
                $entityManager->flush();
                $rfcWorkflowsController->notify_rfc_creator($rfc, $entityManager, $request, $mailer);
                return $this->redirectToRoute('rfc_details', array('id' => $rfc->getId()));
            }
        }

        return new JsonResponse(array());
    }


}
