<?php

namespace App\Controller;

use App\Entity\Costcenters;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CostCentersController extends AbstractController
{
    /**
     * @Route("/costcenters", name="costcenters")
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $repository = $entityManager->getRepository(User::class);
        $users = $repository->findBy([], ['lastname' => 'ASC']);

        $repository = $entityManager->getRepository(Costcenters::class);
        $cost_centers = $repository->findBy([], ['title' => 'ASC']);

        return $this->render('cost_centers/index.html.twig', [
            'selection_list' => $cost_centers,
            'users'          => $users
        ]);
    }

    /**
     * @Route ("/costcenters/save", name="costcenters_save")
     */
    public function save(EntityManagerInterface $entityManager, Request $request) {
        $message = '';

        $repository = $entityManager->getRepository(User::class);
        $responsible = $repository->findOneBy(['id' => $request->request->get('responsible')]);

        $repository = $entityManager->getRepository(Costcenters::class);
        $cost_centers = $repository->findOneBy(["id" => $request->request->get("id")]);

        if (!$cost_centers) {
            $cost_centers = new Costcenters();
            $cost_centers->setLocked(0);
        }

        $cost_centers->setTitle($request->request->get("title"));
        $cost_centers->setResponsible($responsible);
        $entityManager->persist($cost_centers);
        $entityManager->flush();


        $tr = [
            "id" => $cost_centers->getId(),
            "title" => $cost_centers->getTitle(),
            "responsible" => $cost_centers->getResponsible()->getFirstname() .' '. $cost_centers->getResponsible()->getLastname() .' ('. $cost_centers->getResponsible()->getUsername() .')',
            "serialized" => $cost_centers->serialized()
        ];



        $message .= '<span class="">Section saved successfully</span>';
        return new JsonResponse([
            "message"    => $message,
            "costcenter" => $tr
            ]);
    }
}
