<?php

namespace App\Controller;

use App\Entity\Config;
use App\Entity\Departments;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LdapController extends AbstractController
{
    private $config = array(
        "driver"    => "Activedirectory",
        "hostname"  => "",
        "base_dn"   => "",
        "filter"    => "",
        "port"      => "389",
        "ssl"       => "",
        "rdn"       => "",
        "password"  => "",
        "active"    => 0
    );

    /**
     * @Route("/ldap", name="ldap")
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        return $this->render('ldap/index.html.twig', [
            'config' => $this->fetch_config($entityManager),
        ]);
    }

    /**
     * @Route("/ldap/save", name="ldap_save", methods="POST")
     */
    public function save(EntityManagerInterface $entityManager,Request $request, LoggerInterface $logger) {
        $message = '';

        $repository = $entityManager->getRepository(Config::class);

        // Iterate over $this->config, fetch values and create if one does not exits
        foreach ($this->config as $config_key => $value) {
            $config = $repository->findOneBy([
                "group_name" => "ldap",
                "config_key" => $config_key
            ]);

            if (!$config) {
                $config = new Config();
                $config->setGroupName('ldap')
                       ->setConfigKey($config_key)
                       ->setConfigValue(serialize($request->request->get($config_key)))
                       ->setLabel('')
                       ->setFieldType('text')
                       ->setDate(new \DateTime())
                ;
            } else {
                $config->setConfigValue(serialize($request->request->get($config_key)));
            }
            // Copy form data to $this->config
            $this->config[$config_key] = $request->request->get($config_key);

            $entityManager->persist($config);
        }
        $entityManager->flush();

        $count_ldap_entries = $this->fetch_data($entityManager, $logger);

        $message .= '<span class="">LDAP Config saved successfully, ' . $count_ldap_entries . ' entries synced</span>';
        return new JsonResponse([
            'message' => $message
        ]);

    }

    private function fetch_config(EntityManagerInterface $entityManager) {
        $repository = $entityManager->getRepository(Config::class);
        $items = $repository->findBy(["group_name" => "ldap"]);

        $config = $this->config;
        foreach ($items as $item) {
            $config[$item->getConfigKey()] = unserialize($item->getConfigValue());
        }
        return $config;
    }

    public function fetch_data(EntityManagerInterface $entityManager, LoggerInterface $logger) {
        $config = $this->fetch_config($entityManager);
        
        if ($config['ssl'] == "1") {
            $ldap_host = 'ldaps://'. $config['hostname'] . ':' . $config['port'];
            $controls = [[
                "oid" => LDAP_CONTROL_PAGEDRESULTS,
                "size" => 1000,
                "cookie" => "cookie"
            ]];
        } else {
            $ldap_host = 'ldap://'. $config['hostname'] . ':' . $config['port'];
            $controls = [];
        }
        $ldap_attributes = ["givenName", "sn", "ou", "mail", "sAMAccountName"];
        if ($config['driver'] == "OpenLDAP") {
            $ldap_attributes = ["givenName", "sn", "ou", "mail", "uid"];
        }

        if ($ldap_connection = ldap_connect($ldap_host)) {
            try {
                if (ldap_set_option($ldap_connection, LDAP_OPT_PROTOCOL_VERSION, 3)) {
                    if ($ldap_bind = ldap_bind($ldap_connection, $config['rdn'], $config['password'])) {
                        $ldap_search = ldap_search(
                            $ldap_connection,
                            $config['base_dn'],
                            $config['filter'],
                            $ldap_attributes,
                            0,
                            -1,
                            -1,
                            LDAP_DEREF_NEVER,
                            $controls
                        );
                        $ldap_entries = ldap_get_entries($ldap_connection, $ldap_search);
                        foreach ($ldap_entries as $ldap_entry) {
                            if (is_array($ldap_entry)) {
                                try {
                                    $clean_entry = $this->cleanup_entry($ldap_entry);
                                    $repository = $entityManager->getRepository(User::class);
                                    $user = $repository->findOneBy([
                                        'username' => $clean_entry['samaccountname']
                                    ]);
                                    if (!$user) {
                                        $user = new User();
                                        $repository = $entityManager->getRepository(Departments::class);
                                        $department = $repository->findBy([], ['title' => 'ASC'], 1);
                                        $user->setDepartment($department[0]);
                                    }
                                    $user->setEmail($clean_entry["mail"])
                                        ->setFirstname($clean_entry["givenname"])
                                        ->setLastname($clean_entry["sn"])
                                        ->setUsername($clean_entry["samaccountname"])
                                        ->setPassword('')
                                        ->setLogins(0)
                                        ->setLastLogin(0);
                                    $entityManager->persist($user);
                                } catch (\Exception $e) {
                                    // ToDo: somethind snsible if User can’t be added orupdated
                                }
                            }
                        }
                        $entityManager->flush();

                        return count($ldap_entries);
                    }
                }
            } catch (\Exception $e) {
                $logger->error($e);
            }
        } else {
            // ToDo catch error;
        }
    }

    public function login (EntityManagerInterface $entityManager, $credentials) {
        $config = $this->fetch_config($entityManager);

        if ($config['hostname'] && $config['hostname'] != '') {
            if ($config['ssl'] == "1") {
                $ldap_host = 'ldaps://' . $config['hostname'];
            } else {
                $ldap_host = 'ldap://' . $config['hostname'];
            }
            if ($ldap_connection = ldap_connect($ldap_host)) {
                try {
                    if ($ldap_bind = ldap_bind($ldap_connection, $credentials['username'], $credentials['password'])) {
                        return true;
                    }
                } catch (\Exception $e) {
                    return false;
                }
            }
        }

    }

    private function cleanup_entry( $entry ) {
        $return_entry = array(
            "sn"        => "",
            "givenname" => "",
            "mail"      => "",
            "username"  => ""
        );
        for ( $i = 0; $i < $entry['count']; $i++ ) {
            $attribute = $entry[$i];
            if ( $entry[$attribute]['count'] == 1 ) {
                $return_entry[$attribute] = $entry[$attribute][0];
            } else {
                for ( $j = 0; $j < $entry[$attribute]['count']; $j++ ) {
                    $return_entry[$attribute][] = $entry[$attribute][$j];
                }
            }
        }
        return $return_entry;
    }
}
