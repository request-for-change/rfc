<?php

namespace App\Controller;

use App\Entity\Projects;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProjectsController extends AbstractController
{
    /**
     * @Route("/projects", name="projects")
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $repository = $entityManager->getRepository(Projects::class);
        $items = $repository->findAll();
        return $this->render('projects/index.html.twig', [
            'selection_list' => $items,
        ]);
    }

    /**
     * @Route ("/projects/save", name="projects_save")
     */
    public function save(EntityManagerInterface $entityManager, Request $request) {
        $repository = $entityManager->getRepository(Projects::class);
        $project = $repository->findOneBy(["id" => $request->request->get("id")]);

        if (!$project) {
            $project = new Projects();
            $project->setLocked(1);
        }

        $project->setTitle($request->request->get("title"));
        $entityManager->persist($project);
        $entityManager->flush();
        $message = '';
        $items = $repository->findAll();
        $trs = array();
        foreach ($items as $item) {
            $trs[] = [
                "id" => $item->getId(),
                "title" => $item->getTitle()
            ];

        }
        $message .= '<span class="">Section saved successfully</span>';
        return new JsonResponse([
            "message" => $message,
            "trs"     => $trs
        ]);
    }
}
