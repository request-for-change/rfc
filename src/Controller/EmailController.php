<?php

namespace App\Controller;

use App\Entity\Config;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;

class EmailController extends AbstractController
{
    private $config = array(
        "driver"            => "",
        "from"              => "",
        "send_notification" => "",
        "sendmail"          => "/usr/sbin/sendmail -bs",
        "hostname"          => "",
        "port"              => "",
        "username"          => "",
        "password"          => "",
        "options"           => array(
            "hostname" => "",
            "port"     => "",
            "username" => "",
            "password" => ""

        ),
        "signature"          => "",
        "verify_peer"        => 1
    );

    /**
     * @Route("/email", name="email")
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        //$config = $this->fetch_config($entityManager);
        //dd($config);
        return $this->render('email/index.html.twig', [
            'config' => $this->fetch_config($entityManager),
        ]);
    }

    /**
     * @Route ("email/save", name="email_save")
     */
    public function save(EntityManagerInterface $entityManager, Request $request) {
        $message = '';
        $repository = $entityManager->getRepository(Config::class);

        foreach ($this->config as $config_key => $value) {
            $config = $repository->findOneBy([
                "group_name" => "email",
                "config_key" => $config_key
            ]);

            if (!$config) {
                $config = new Config();
                $config->setGroupName('email')
                       ->setConfigKey($config_key)
                       ->setConfigValue(serialize($request->request->get($config_key)))
                       ->setLabel('')
                       ->setFieldType('text')
                       ->setDate(new \DateTime());
            } else {
                $config->setConfigValue(serialize($request->request->get($config_key)));
            }

            $this->config[$config_key] = $request->request->get($config_key);
            $entityManager->persist($config);
        }


        $entityManager->flush();
        $config = $this->fetch_config($entityManager);

        $env_file = '../.env.local';
        $current = file($env_file); // reads an array of lines
        if ($config['verify_peer'] != 1 ) {
            $config['verify_peer'] = 0;
        }
        $future = array();
        foreach ($current as $line) {
            if (strpos($line, 'MAILER_DSN') !== false) {
                if ($config['username'] && $config['password']) {
                    $line = 'MAILER_DSN="smtp://' . $config['username'] . ':' . $config['password'] . '@' . $config['hostname'] . ':' . $config['port'] . '"?verify_peer=' . $config['verify_peer'];
                } else {
                    $line = 'MAILER_DSN="smtp://' . $config['hostname'] . ':' . $config['port'] . '"?verify_peer=' . $config['verify_peer'];
                }
            }
            $future[] = $line;
        }
        //dd($future);

        file_put_contents($env_file, $future);

        $message .= '<span class="">E-Mail Config saved successfully</span>';
        return new JsonResponse([
            'message' => $message
        ]);

    }

    public function send_email(EntityManagerInterface $entityManager, User $user, $message, $subject, MailerInterface $mailer){
        $config = $this->fetch_config($entityManager);
        // Make shure, environment variable is set
        $env_file = '../.env.local';
        $current = file($env_file); // reads an array of lines
        if ($config['verify_peer'] != 1 ) {
            $config['verify_peer'] = 0;
        }
        $future = array();
        foreach ($current as $line) {
            if (strpos($line, 'MAILER_DSN') !== false) {
                $line = 'MAILER_DSN="smtp://' . $config['username'] . ':' . $config['password'] . '@' . $config['hostname'] . ':' . $config['port'] . '"?verify_peer=' . $config['verify_peer'];
            }
            $future[] = $line;
        }
        file_put_contents($env_file, $future);

        $emailaddress = $user->getEmail();
        $fullname = $user->getFirstname() .' '. $user->getLastname();

        if ($config['send_notification'] == 1 && filter_var($emailaddress, FILTER_VALIDATE_EMAIL)) {
            $email = (new TemplatedEmail())
                ->from($config['from'])
                ->to(new Address($emailaddress, $fullname))
                ->subject($subject)
                ->htmlTemplate('email/rfc_template.html.twig')
                ->context(['message' => $message, 'signature' => $config['signature']])
            ;


            try {
                $mailer->send($email);

            } catch (TransportExceptionInterface $e) {
                // TdoDo: o something sensible
            }
        }
    }

    /**
     * @param EntityManagerInterface $entityManager
     * @return array
     */
    private function fetch_config(EntityManagerInterface $entityManager) {
        $repository = $entityManager->getRepository(Config::class);
        $items = $repository->findBy(["group_name" => "email"]);

        $config = $this->config;
        foreach ($items as $item) {
            $config[$item->getConfigKey()] = unserialize($item->getConfigValue());
        }

        // Legacy code for old RfC Add-on: Copy some values to config['options'] Array
        $config['options'] = array(
                          $config["sendmail"],
            "hostname" => $config["hostname"],
            "port"     => $config["port"],
            "username" => $config["username"],
            "password" => $config["password"]
        );

        return $config;
    }
}
