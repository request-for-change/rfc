<?php

namespace App\Controller;

use App\Entity\Config;
use App\Entity\EvidenceCollections;
use App\Entity\Hostrecords;
use App\Entity\Networkscans;
use App\Entity\Roles;
use App\Repository\NetworkscansRepository;
use bheisig\idoitapi\API;
use bheisig\idoitapi\CMDBCategory;
use bheisig\idoitapi\CMDBObject;
use bheisig\idoitapi\CMDBObjects;
use bheisig\idoitapi\CMDBObjectTypes;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NetworkscansController extends AbstractController
{
    /**
     * @Route ("/networkscan/upload", name="networkscan_upload")
     */
    public function temporaryUploadAction(EntityManagerInterface $entityManager, Request $request)
    {
        $repository = $entityManager->getRepository(EvidenceCollections::class);
        $evidenceCollection = $repository->findOneBy(['id' => $request->request->get('evidenceCollection')]);
        /** @var UploadedFile $networkscan_file */
        $networkscan_file = $request->files->get('networkscan');
        if ($networkscan_file->getMimeType() == "text/xml"){

            if ($evidenceCollection->getObjId()) {
                $l3network = $this->fetch_l3network($entityManager, $evidenceCollection->getObjId());
            }
            //dd($l3network);
            $networkscan_content = simplexml_load_file($networkscan_file->getRealPath());

            if (isset($networkscan_content->host) && is_iterable($networkscan_content->host)) {
                $hostrecords = array();
                $index = 0;
                foreach ($networkscan_content->host as $host) {
                    if (isset($host->address) && is_iterable($host->address)) {
                        foreach ($host->address as $addresses) {
                            if ($addresses["addrtype"] == "ipv4") {
                                if (isset($addresses["addr"])) $hostrecords[$index]["ipv4_addr"] = ip2long(strval($addresses["addr"]));
                            }

                            if ($addresses["addrtype"] == "mac") {
                                if (isset($addresses["addr"]))   $hostrecords[$index]["mac_addr"] = strval($addresses["addr"]);
                                if (isset($addresses["vendor"])) $hostrecords[$index]["vendor"] = strval($addresses["vendor"]);
                            }

                            if (isset($l3network[ip2long(strval($addresses["addr"]))])) {
                                $hostrecords[$index]["obj_id"] = $l3network[ip2long(strval($addresses["addr"]))];
                            }
                        }
                    }
                    if (isset($host->hostnames) && is_iterable($host->hostnames)) {
                        foreach ($host->hostnames as $hostname) {
                            if (isset($hostname->hostname["name"])) $hostrecords[$index]["hostname"] = strval($hostname->hostname["name"]);
                        }
                    }
                    $index++;
                }
                if(count($hostrecords)) {
                    $networkscans = new Networkscans();
                    $networkscans->setCreated(new \DateTime())
                        ->setCreator($this->getUser())
                        ->setEvidenceCollection($evidenceCollection);

                    $entityManager->persist($networkscans);
                    $entityManager->flush();
                    foreach ($hostrecords as $hostrecord) {
                        $hostrecordEntity = new Hostrecords();
                        $hostrecordEntity->setNetworkscan($networkscans);
                        if (isset($hostrecord["ipv4_addr"])) $hostrecordEntity->setIpv4Addr($hostrecord["ipv4_addr"]);
                        if (isset($hostrecord["mac_addr"]))  $hostrecordEntity->setMacAddr($hostrecord["mac_addr"]);
                        if (isset($hostrecord["hostname"]))  $hostrecordEntity->setHostname($hostrecord["hostname"]);
                        if (isset($hostrecord["vendor"]))    $hostrecordEntity->setVendor($hostrecord["vendor"]);
                        if (isset($hostrecord["obj_id"]))    $hostrecordEntity->setObjid($hostrecord["obj_id"]);
                        $entityManager->persist($hostrecordEntity);
                    }
                    $evidenceCollection->setUpdated(new \DateTime());
                    $entityManager->persist($evidenceCollection);
                    $entityManager->flush();
                    return $this->redirectToRoute('evidence_collection_show', array('id' => $evidenceCollection->getId()));
                }
            }
        }
    }

    /**
     * @param Networkscans $networkscans
     * @param EntityManagerInterface $entityManager
     * @return Response
     * @Route ("/networkscan/show/{id<(\d)+>}", name="networkscan_show", methods="GET")
     */
    public function show(Networkscans $networkscans, EntityManagerInterface $entityManager): Response
    {
        /** @var Networkscans|null $networkscans */
        if (!$networkscans) {
            throw $this->createNotFoundException('This networkscan does not exist');
        }
        //dd($networkscans->getHostrecords()->getValues()[0]);
        $hostrecords = array();
        foreach ($networkscans->getHostrecords() as $values) {
            $hostrecords[$values->getIpv4Addr()]["ipv4_addr"] = $values->getIpv4Addr();
            //$hostrecords[$values->getIpv4Addr()]["obj_id"] = $values->getObjId();
            if ($values->getObjId()) {
                $hostrecords[$values->getIpv4Addr()]["object"] = $this->fetch_obj($entityManager, $values->getObjId());
            }
            $hostrecords[$values->getIpv4Addr()]["hostname"] = $values->getHostname();
            $hostrecords[$values->getIpv4Addr()]["mac_addr"] = $values->getMacAddr();
            $hostrecords[$values->getIpv4Addr()]["vendor"] = $values->getVendor();
        }
        ksort($hostrecords);
        //dd($hostrecords);

        return $this->render('networkscans/show.html.twig',[
            'networkscans' => $networkscans,
            'hostrecords'  => $hostrecords,
            'config'       => $this->get_idoit_config($entityManager),
        ]);
    }

    /**
     * @param EntityManagerInterface $entityManager
     * @Route ("/evidence/collection/delete/{id<(\d)+>}", name="networkscan_delete", methods="GET")
     */
    public function delete(Networkscans $networkscans, EntityManagerInterface $entityManager) {
        /** @var Networkscans|null $networkscans */
        if (!$networkscans) {
            throw $this->createNotFoundException('This networkscan does not exist');
        }
        //dd($networkscans->getHostrecords()->getValues()[0]->getIpv4Addr());
        $evidenceCollection = $networkscans->getEvidenceCollection();
        $entityManager->remove($networkscans);
        $entityManager->flush();
        return $this->redirectToRoute('evidence_collection_show', array('id' => $evidenceCollection->getId()));
    }

    /**
     * @param EntityManagerInterface $entityManager
     * @Route ("/networkscans/compare", name="notworkscans_compare", methods="POST")
     */
    public function compare(EntityManagerInterface $entityManager, Request $request): Response
    {

        $repository = $entityManager->getRepository(Networkscans::class);
        foreach ($request->request->get('networkscans') as $networkscan){
            $networkscans[] = $repository->findOneBy(["id" => $networkscan]);
        }

        if (count($networkscans) == 2) {//dd($networkscans);
            $merged_scans = array();
            $networkscan_index = 0;
            foreach ($networkscans as $networkscan) {
                foreach ($networkscan->getHostrecords() as $hostrecord) {
                    $merged_scans[$hostrecord->getIpv4Addr()][$networkscan_index]["ipv4_addr"] = long2ip($hostrecord->getIpv4Addr());
                    $merged_scans[$hostrecord->getIpv4Addr()][$networkscan_index]["objid"] = $hostrecord->getObjid();
                    if ($hostrecord->getObjId()) {
                        $merged_scans[$hostrecord->getIpv4Addr()][$networkscan_index]["object"] = $this->fetch_obj($entityManager, $hostrecord->getObjId());
                    } else {
                        $merged_scans[$hostrecord->getIpv4Addr()][$networkscan_index]["object"] = false;
                    }
                    $merged_scans[$hostrecord->getIpv4Addr()][$networkscan_index]["hostname"] = $hostrecord->getHostname();
                    $merged_scans[$hostrecord->getIpv4Addr()][$networkscan_index]["mac_addr"] = $hostrecord->getMacAddr();
                    $merged_scans[$hostrecord->getIpv4Addr()][$networkscan_index]["vendor"] = $hostrecord->getVendor();
                    $evidence_collection = $hostrecord->getNetworkscan()->getEvidenceCollection();
                }
                $networkscan_index++;
            }
            ksort($merged_scans);//dd($merged_scans);
            return $this->render('networkscans/compare.html.twig', [
                'merged_scans' => $merged_scans,
                'evidence_collection' => $evidence_collection,
                'networkscans' => $networkscans,
                'config' => $this->get_idoit_config($entityManager),
            ]);
        } else {
            throw new \Exception('We only can compare exact 2 networkscans, ' . count($networkscans) . ' given.');
        }
    }

    private function fetch_l3network(EntityManagerInterface $entityManager, $objId) {
        $config = $this->get_idoit_config($entityManager);

        if ($config['username'] != '' && $config['password'] != '' && $config['idoit_url'] != '') {
            $idoit_config = array(
                API::URL                      => $config['idoit_url'] . '/src/jsonrpc.php',
                API::PORT                     => $config['port'],
                API::KEY                      => $config['api_key'],
                API::USERNAME                 => $config['username'],
                API::PASSWORD                 => $config['password'],
                API::LANGUAGE                 => $config["api_lang"],
                API::BYPASS_SECURE_CONNECTION => true
            );
            $api = new API($idoit_config);
            try {
                $api->login();
            } catch (\Exception $exception) {
                //
            }
            if ($api->isLoggedIn()) {
                // C__CATS__NET_IP_ADDRESSES
                $cmdbCategory = new CMDBCategory($api);
                $category = $cmdbCategory->read($objId, 'C__CATS__NET_IP_ADDRESSES');
                $l3network = array();
                foreach ($category as $host) {
                    $l3network[ip2long($host["title"])] = $host["object"]["id"];
                }

            } else {
                $l3network = array();
            }
        } else {
            $l3network = array();
        }


        // End Fetch i-doit Object Groups
        return $l3network;
    }

    private function fetch_obj(EntityManagerInterface $entityManager, $objID) {
        $config = $this->get_idoit_config($entityManager);

        if ($config['username'] != '' && $config['password'] != '' && $config['idoit_url'] != '') {
            $idoit_config = array(
                API::URL                      => $config['idoit_url'] . '/src/jsonrpc.php',
                API::PORT                     => $config['port'],
                API::KEY                      => $config['api_key'],
                API::USERNAME                 => $config['username'],
                API::PASSWORD                 => $config['password'],
                API::LANGUAGE                 => $config["api_lang"],
                API::BYPASS_SECURE_CONNECTION => true
            );
            $api = new API($idoit_config);
            try {
                $api->login();
            } catch (\Exception $exception) {
                //
            }
            if ($api->isLoggedIn()) {
                $cmdbObject = new CMDBObject($api);
                $object = $cmdbObject->read($objID);

            } else {
                $object = false;
            }
        } else {
            $object = false;
        }


        // End Fetch i-doit Object Groups
        return $object;
    }

    private function get_idoit_config(EntityManagerInterface $entityManager) {
        $repository = $entityManager->getRepository(Config::class);
        $items      = $repository->findBy(["group_name" => "i-doit"]);

        foreach ($items as $item) {
            $config[$item->getConfigKey()] = unserialize($item->getConfigValue());
        }
        $url_parts = parse_url($config['idoit_url']);

        if (isset($url_parts["port"])) {
            $config['port'] = (int)$url_parts["port"];
        } elseif (isset($url_parts["scheme"]) && $url_parts["scheme"] == "https") {
            $config['port'] = 443;
        } else {
            $config['port'] = 80;
        }


        if ($this->getUser()->getLanguage() == '') {
            $config["api_lang"] = 'en';
        } else {
            $config["api_lang"] = $this->getUser()->getLanguage();
        };

        return $config;
    }
}
