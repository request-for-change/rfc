<?php

namespace App\Controller;

use App\Entity\Config;
use App\Entity\Inventory;
use App\Entity\Roles;
use App\Entity\User;
use bheisig\idoitapi\API;
use bheisig\idoitapi\CMDBCategory;
use bheisig\idoitapi\CMDBObject;
use bheisig\idoitapi\CMDBObjects;
use bheisig\idoitapi\CMDBObjectTypes;
use bheisig\idoitapi\Idoit;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Extra\Markdown\DefaultMarkdown;

class InventoryController extends AbstractController
{
    private DefaultMarkdown $markdownConverter;

    public function __construct()
    {
        $this->markdownConverter = new DefaultMarkdown();
    }


    /**
     * @Route ("/inventory/save", name="inventory_save")
     */
    public function save(EntityManagerInterface $entityManager, Request $request, MailerInterface $mailer, TranslatorInterface $translator): Response
    {
        $session = $request->getSession();
        $userId = null;
        if ($session->has('userId')) {
            $userId = $session->get('userId');
        }

        $api = $this->get_idoit_api($entityManager);
        $datetime = new \DateTime();
        $datetime_string = $datetime->format('Y-m-d H:i:s');
        //dd($datetime_string);
        // 2022-04-19 08:00:00
        // 2022-04-20 07:50:27

        $obj_ids = $request->request->get('obj_ids');
        if (is_array($obj_ids)) {
            foreach ($obj_ids as $obj_id) {
                $cmdbCategory = new CMDBCategory($api);
                $cmdbCategory->save(
                    $obj_id,
                    'C__CATG__CUSTOM_FIELDS__RFC_INVENTORY',
                    [
                        'f_popup_c_1650390297823' => $datetime_string,
                        'f_popup_c_1650390297828' => (int)$request->request->get('idoit_user'),

                    ]);
            }
        }
        // and now mark workplace as seen even if no device was marked as seen
        $cmdbCategory = new CMDBCategory($api);
        $cmdbCategory->save(
            (int)$request->request->get('idoit_workplace'),
            'C__CATG__CUSTOM_FIELDS__RFC_INVENTORY',
            [
                'f_popup_c_1650390297823' => $datetime_string,
                'f_popup_c_1650390297828' => (int)$request->request->get('idoit_user'),

            ]);

        if ($request->request->get('message')){
            $repository = $entityManager->getRepository(Config::class);
            $items      = $repository->findBy(["group_name" => "system"]);

            foreach ($items as $item) {
                $config[$item->getConfigKey()] = unserialize($item->getConfigValue());
            }
            $posted_message = $request->request->get('message');
            if ($config['sacm']) {
                $repository = $entityManager->getRepository(User::class);
                $sacmUser = $repository->findOneBy(['id' => $config['sacm']]);

                if ($sacmUser) {

                    $repository = $entityManager->getRepository(Config::class);
                    $items      = $repository->findBy(["group_name" => "i-doit"]);

                    foreach ($items as $item) {
                        $config[$item->getConfigKey()] = unserialize($item->getConfigValue());
                    }
                    $workplace_url = $config['idoit_url'] . '/?objID=' . $request->request->get('idoit_workplace');
                    $user_string = $this->getUser()->getFirstname() . ' ' . $this->getUser()->getLastname() . ' (' . $this->getUser()->getUsername() . ')';


                    $message = <<<EOD
Hello,

$user_string sent an inventory check for a [workplace]($workplace_url)  with the following message to you has been submitted:

$posted_message

EOD;
                    $message = $this->markdownConverter->convert($message);
                    $email = new EmailController();
                    $email->send_email($entityManager, $sacmUser, $message, 'An inventory check with a message to you has been submitted', $mailer);
                }

            }

        }
        $entityManager->flush();
        $this->addFlash('success', $translator->trans('Your workplace and the selected items have been marked as recently seen.'));
        if ($userId) {
            return $this->redirectToRoute('inventory', ['userId' => $userId]);
        } else {
            return $this->redirectToRoute('inventory');
        }
    }


    /**
     * @Route("/inventory/{userId}", name="inventory", defaults={"userId"=null})
     */
    public function index(EntityManagerInterface $entityManager, Request $request, $userId = null): Response
    {
        $session = $request->getSession();
        $session->set('userId', $userId);
        // Fetch i-doit Object Groups and affected_components
        $idoit_user = false;
        $idoit_workplaces = false;
        $assigned_objects = array();
        $idoit_inventory = false;

        $api = $this->get_idoit_api($entityManager);
        if ($api && $api->isLoggedIn()) {
            $cmdbObjects = new CMDBObjects($api);
            if ($userId and in_array('C__ROLES__ADMIN', $this->getUser()->getRoles())) {
                // Get i-doit User from requested userId
                $repository = $entityManager->getRepository(User::class);
                $user = $repository->find($userId);
                $user_filter = array(
                    "first_name" => trim($user->getFirstname()),
                    "last_name" => trim($user->getLastname()),
                    "email" => trim($user->getEmail()),
                );
            } else {
                // Get i-doit User from loggedin User
                $user_filter = array(
                    "first_name" => $this->getUser()->getFirstname(),
                    "last_name" => $this->getUser()->getLastname(),
                    "email" => $this->getUser()->getEmail(),
                );
            }
            $idoit_result = $cmdbObjects->read($user_filter);
            if (is_array($idoit_result) && count($idoit_result) > 0) {
                $idoit_user = (array_values($idoit_result)[0]); // First result
            }

            if ($idoit_user) { // Find 1st assigned workplace if i-doit user exists
                $cmdbCategory = new CMDBCategory($api);
                $idoit_result = $cmdbCategory->read($idoit_user["id"], 'C__CATG__PERSON_ASSIGNED_WORKSTATION');
                if (is_array($idoit_result) && count($idoit_result) > 0) {
                    $idoit_workplaces = (array_values($idoit_result)); // All results
                }

                // Fetch assigned workplace if user has workplace assigned

                foreach ($idoit_workplaces as $idoit_workplace) {
                    if ($idoit_workplace) {
                        $cmdbCategory = new CMDBCategory($api);
                        $idoit_result = $cmdbCategory->read($idoit_workplace["objID"], 'C__CATG__ASSIGNED_LOGICAL_UNIT');
                        if (is_array($idoit_result)) {
                            foreach ($idoit_result as $object) {

                                //Read Catg General and get Tags
                                $object["assigned_object"]["tags"] = '';
                                $idoit_inventory_general = $cmdbCategory->read($object["assigned_object"]["id"], 'C__CATG__GLOBAL');
                                if (is_array($idoit_inventory_general)) {
                                    $idoit_inventory_general = array_shift($idoit_inventory_general);
                                }

                                if (array_key_exists('tag', $idoit_inventory_general) && is_array($idoit_inventory_general['tag'])) {
                                    foreach ($idoit_inventory_general['tag'] as $tag) {
                                        $object["assigned_object"]["tags"] .= $tag['title'] . ', ';
                                    }
                                }
                                $object["assigned_object"]["tags"] = trim($object["assigned_object"]["tags"], ', ');


                                $idoit_inventory_result = $cmdbCategory->read($object["assigned_object"]["id"], 'C__CATG__CUSTOM_FIELDS__RFC_INVENTORY');
                                if (is_array($idoit_inventory_result) && count($idoit_inventory_result) > 0) {
                                    $idoit_inventory = (array_values($idoit_inventory_result)[0]); // First result
                                    if (is_array($idoit_inventory) && array_key_exists('f_popup_c_1650390297823', $idoit_inventory) && array_key_exists('f_popup_c_1650390297828', $idoit_inventory)) {
                                        if (array_key_exists('title', $idoit_inventory['f_popup_c_1650390297823'])) {
                                            $object["assigned_object"]["last_seen"] = $idoit_inventory['f_popup_c_1650390297823']['title'];
                                        } else {
                                            $object["assigned_object"]["last_seen"] = false;
                                        }
                                        if (array_key_exists('title', $idoit_inventory['f_popup_c_1650390297828'])) {
                                            $object["assigned_object"]["by_user"] = $idoit_inventory['f_popup_c_1650390297828']['title'];
                                        } else {
                                            $object["assigned_object"]["by_user"] = false;
                                        }
                                    }
                                } else {
                                    $object["assigned_object"]["last_seen"] = false;
                                    $object["assigned_object"]["by_user"] = false;
                                }
                                $assigned_objects[$idoit_workplace["objID"]][] = $object["assigned_object"];
                            }
                        }
                    }
                }
            }
        }
        // End Fetch i-doit Object Groups

        return $this->render('inventory/index.html.twig', [
            'assigned_objects' => $assigned_objects,
            'idoit_user' => $idoit_user,
            'idoit_workplaces' => $idoit_workplaces,
        ]);
    }
    private function get_idoit_api($entityManager) {
        $repository = $entityManager->getRepository(Config::class);
        $items      = $repository->findBy(["group_name" => "i-doit"]);

        foreach ($items as $item) {
            $config[$item->getConfigKey()] = unserialize($item->getConfigValue());
        }
        $url_parts = parse_url($config['idoit_url']);

        if (isset($url_parts["port"])) {
            $config['port'] = (int)$url_parts["port"];
        } elseif (isset($url_parts["scheme"]) && $url_parts["scheme"] == "https") {
            $config['port'] = 443;
        } else {
            $config['port'] = 80;
        }


        if ($this->getUser()->getLanguage() == '') {
            $config["api_lang"] = 'en';
        } else {
            $config["api_lang"] = $this->getUser()->getLanguage();
        };

        if ($config['username'] != '' && $config['password'] != '' && $config['idoit_url'] != '') {
            $idoit_config = array(
                API::URL => $config['idoit_url'] . '/src/jsonrpc.php',
                API::PORT => $config['port'],
                API::KEY => $config['api_key'],
                API::USERNAME => $config['username'],
                API::PASSWORD => $config['password'],
                API::LANGUAGE => $config["api_lang"],
                API::BYPASS_SECURE_CONNECTION => true
            );
            $api = new API($idoit_config);
            try {
                $api->login();
            } catch (\Exception $exception) {
                //
            }
            if ($api->isLoggedIn()) {
                return $api;
            }
        }
        return false;
    }
}
