<?php

namespace App\Controller;


use App\Entity\Config;
use App\Entity\ImplementationStatus;
use App\Entity\Rfc;
use App\Entity\RfcNotices;
use App\Entity\RfcWorkflows;
use App\Entity\Roles;
use App\Entity\Status;
use App\Entity\User;
use bheisig\idoitapi\API;
use bheisig\idoitapi\CMDBObjects;
use bheisig\idoitapi\CMDBObjectTypes;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Extra\Markdown\DefaultMarkdown;

class RfcWorkflowsController extends AbstractController
{
    private DefaultMarkdown $markdownConverter;

    public function __construct()
    {
        $this->markdownConverter = new DefaultMarkdown();
    }
    /**
     * @Route("/rfc/workflows", name="rfc_workflows")
     */
    public function index(Request $request, EntityManagerInterface $entityManager): Response
    {
        $repository  = $entityManager->getRepository(ImplementationStatus::class);
        $status_new  = $repository->findOneBy(['const' => 'C_STATUS_NEW']);
        $status_open = $repository->findOneBy(['const' => 'C_STATUS_OPEN']);

        $repository = $entityManager->getRepository(RfcWorkflows::class);
        $worklows = $repository->findBy(['assignee' => $this->getUser(), 'implementation_status' => [$status_new, $status_open]], ['end_date' => 'ASC', 'start_date' => 'ASC']);

        return $this->render('rfc_workflows/index.html.twig', [
            'workflows' => $worklows,
            'controller_name' => 'RfcWorkflowsController',
        ]);
    }

    /**
     * @Route ("/rfc/workflows/save", name="rfc_workflows_save")
     */
    public function save (Request $request, EntityManagerInterface $entityManager)
    {
        /** @var UploadedFile $uploadedFile */
        $attachment = array();
        $uploadedFiles = ($request->files->get('attachment'));
        $destination = $this->getParameter('kernel.project_dir').'/uploads/';
        foreach ($uploadedFiles as $uploadedFile) {
            $newFilename = uniqid().'-'.$uploadedFile->getClientOriginalName();
            ($uploadedFile->move($destination, $newFilename));
            $attachment[] = array(
                'original_name' => $uploadedFile->getClientOriginalName(),
                'saved_name'    => $newFilename
            );
        }

        $repository = $entityManager->getRepository(Rfc::class);
        $rfc = $repository->findOneBy(['id' => $request->request->get('rfc_id')]);
        $repository = $entityManager->getRepository(User::class);
        $creator = $repository->findOneBy(['id' => $request->request->get('creator_id')]);

        if ($request->request->get('implementation_status')) {
            $implementation_status_const = $request->request->get('implementation_status');
        } else {
            $implementation_status_const = 'C_STATUS_NEW';
        }

        $repository = $entityManager->getRepository(ImplementationStatus::class);
        $implementation_status = $repository->findOneBy(['const' => $implementation_status_const]);

        $objects = array();
        if (is_array($request->request->get('objects'))){
            foreach ($request->request->get('objects') as $object) {
                $objects[] = $object;
            }
        }

        $persons = array();
        if (is_array($request->request->get('persons'))){
            foreach ($request->request->get('persons') as $person) {
                $persons[] = $person;
            }
        }


        if (!$rfc) {
            throw new \Exception('RfC does not exist');
        }

        $repository = $entityManager->getRepository(RfcWorkflows::class);
        $rfc_workflow = $repository->findOneBy(['id' => $request->request->get('rfc_workflow_id')]);

        $repository = $entityManager->getRepository(User::class);
        $assignee = $repository->findOneBy(['id' => $request->request->get('assignee')]);

        if (!$rfc_workflow) {
            $rfc_workflow = new RfcWorkflows();
            $rfc_workflow->setCreator($creator)
            ->setRfc($rfc);
        }


        if ($request->request->get('delete')) {
            $entityManager->remove($rfc_workflow);
            $entityManager->flush();
        } else {
            $start_date = new \DateTime($request->request->get('start_date'));
            $end_date = new \DateTime($request->request->get('end_date'));

            /*
            don’t do this. see issue #40 from christian pichler, kantonspolizei
            // if Time to work is 0, add one day
            if ($start_date == $end_date) {
                $end_date->add(new \DateInterval('P1D'));
            }
            */

            // Get allready stored attachments
            $stored_attachments = json_decode($rfc_workflow->getAttachment(), true);
            if (json_last_error_msg() and json_last_error_msg() != "No error") {
                if ($rfc_workflow->getAttachment() != '') {
                    $stored_attachments = [['original_name' => $rfc_workflow->getAttachment()]];
                } else {
                    $stored_attachments = array();
                }
            }
            $attachment = array_merge($attachment, $stored_attachments);
            // remove attacments
            if ($request->request->get('removed-attachments')) {
                $removed_attachments = $request->request->get('removed-attachments');
                foreach ($attachment as $index => $stored_attachment) {
                    if (in_array($stored_attachment["original_name"],$removed_attachments)) {
                        unset($attachment[$index]);
                    }
                }
            }

            $rfc_workflow->setStartDate($start_date)
                         ->setEndDate($end_date)
                         ->setTitle($request->request->get('title'))
                         ->setDescription($request->request->get('description'))
                         ->setPersons(json_encode($persons))
                         ->setObjects(json_encode($objects))
                         ->setAssignee($assignee)
                         ->setImplementationStatus($implementation_status)
                         ->setAttachment(json_encode($attachment))
            ;

            if ($request->request->get('parent')) {
                $repository = $entityManager->getRepository(RfcWorkflows::class);
                $parent = $repository->findOneBy(['id' => $request->request->get('parent')]);
                $rfc_workflow->setParent($parent);
            } else {
                $rfc_workflow->setParent(NULL);
            }
            $entityManager->persist($rfc_workflow);
            $entityManager->flush();
        }

        return $this->redirectToRoute('rfc_details', array('id' => $rfc->getId()));
    }

    /**
     * @Route ("/rfc/workflows/start" , name="rfc_workflows_start")
     */
    public function start(Request $request, EntityManagerInterface $entityManager, MailerInterface $mailer, LoggerInterface $logger) {
        $repository = $entityManager->getRepository(Rfc::class);
        $rfc = $repository->findOneBy(['id' => $request->request->get('rfc_id')]);

        $repository = $entityManager->getRepository(RfcWorkflows::class);
        $rfcWorkflows = $repository->findByRfc($rfc);

        $system = new SystemController();
        $system_settings = $system->get_config($entityManager);

        // Check for Workflows w/o Assignees
        foreach ($rfcWorkflows as $rfcWorkflow) {
            // ToDo make something more sensible
            if (!$rfcWorkflow->getAssignee()) {
                return $this->redirectToRoute('rfc_details', array('id' => $rfc->getId()));
                exit();
            }
        }

        // Check if we have at least 1 Workflow in State open or new
        $count_new_or_open = 0;
        foreach ($rfcWorkflows as $workflow) {
            if ($workflow->getImplementationStatus()->getConst() == 'C_STATUS_NEW' || $workflow->getImplementationStatus() == 'C_STATUS_OPEN' ) {
                $count_new_or_open++;
            }
        }


        $zammad = new ZammadController();
        $zammad_config = $zammad->get_config($entityManager);
        if ($zammad_config["active"] == 1) {
            $zammad->remove_tag($rfc->getReferencedTicketid(), 'RfC_Status_Dispatching', $entityManager);
            $zammad->remove_tag($rfc->getReferencedTicketid(), 'RfC_Status_BPO_Feedback', $entityManager); // if we come from BPO State
            $zammad->remove_tag($rfc->getReferencedTicketid(), 'RfC_Status_Classification', $entityManager);
        }

        $repository = $entityManager->getRepository(Status::class);
        if (count($rfcWorkflows) > 0 and $count_new_or_open > 0) {
            $status = $repository->findOneBy(['const' => 'C__STATUS__CMDB']);
        } else {
            // we have no workflows at all set C__STATUS__VALIDATION
            $status = $repository->findOneBy(['const' => 'C__STATUS__VALIDATION']);
            if ($zammad_config["active"] == 1) {
                $zammad->remove_tag($rfc->getReferencedTicketid(), 'RfC_Status_Implementation', $entityManager);
                $zammad->add_tag($rfc->getReferencedTicketid(), 'RfC_Status_Validation', $entityManager);
            }
        }

        $old_status_title = $rfc->getStatus()->getTitle();
        $new_status_title = $status->getTitle();

        $rfc->setStatus($status);

        $rfc_notice_content = <<<EOD
```
Previous state: $old_status_title
New state:      $new_status_title
```
EOD;


        $rfc_notice = new RfcNotices();
        $rfc_notice->setRfc($rfc)
            ->setTitle('Logbook entry')
            ->setContent($rfc_notice_content)
            ->setStatus($status)
            ->setCreated(new \DateTime())
            ->setAuthor($this->getUser())
        ;
        $entityManager->persist($rfc_notice);

        $entityManager->persist($rfc);

        foreach ($rfcWorkflows as $rfcWorkflow) {
            // Send Notification for tasks w/o parent task
            if ((!$rfcWorkflow->getParent() or $system_settings['notify_at_once'])  and $rfcWorkflow->getImplementationStatus()->getConst() == 'C_STATUS_NEW') {
                $this->notify($rfcWorkflow, $entityManager, $request, $mailer, $logger);
            }
        }

        $entityManager->flush();

        return $this->redirectToRoute('rfc_details', array('id' => $rfc->getId()));
    }

    /**
     * @Route ("/rfc/workflows/{id<(\d)+>}", name="rfc_workflows_detail", methods="GET")
     */
    public function details ($id, EntityManagerInterface $entityManager) :Response
    {
        $repository = $entityManager->getRepository(RfcWorkflows::class);
        $rfcWorkflows = $entityManager->find(RfcWorkflows::class, $id);

        $child_tasks = $repository->findBy(['parent' => $rfcWorkflows->getId()]);

        $repository = $entityManager->getRepository(RfcWorkflows::class);
        if ($rfcWorkflows->getParent()) {
            $sibling_tasks = $repository->findBy(['parent' => $rfcWorkflows->getParent()->getId()]);
        } else {
            $sibling_tasks = $repository->findBy(['parent' => NULL, 'rfc' => $rfcWorkflows->getRfc()]);
        }

        $repository = $entityManager->getRepository(ImplementationStatus::class);
        $implementationStatus = $repository->findBy([], ['sort' => 'ASC']);

        // Fetch i-doit affected_components
        $affected_components = array();

        $repository = $entityManager->getRepository(Config::class);
        $items      = $repository->findBy(["group_name" => "i-doit"]);

        $repository = $entityManager->getRepository(Roles::class);
        $role_cm = $repository->findOneBy(['const' => 'C__ROLES__CM']);

        foreach ($items as $item) {
            $config[$item->getConfigKey()] = unserialize($item->getConfigValue());
        }
        $url_parts = parse_url($config['idoit_url']);

        if (isset($url_parts["port"])) {
            $config['port'] = (int)$url_parts["port"];
        } elseif ($url_parts["scheme"] == "https") {
            $config['port'] = 443;
        } else {
            $config['port'] = 80;
        }

        if ($config["api_lang"] == '') $config["api_lang"] = 'en';

        if ($config['username'] != '' && $config['password'] != '') {
            $idoit_config = array(
                API::URL                      => $config['idoit_url'] . '/src/jsonrpc.php',
                API::PORT                     => $config['port'],
                API::KEY                      => $config['api_key'],
                API::USERNAME                 => $config['username'],
                API::PASSWORD                 => $config['password'],
                API::LANGUAGE                 => $config["api_lang"],
                API::BYPASS_SECURE_CONNECTION => true
            );
            $api = new API($idoit_config);
            try {
                $api->login();
            } catch (\Exception $exception) {
                //
            }
            if ($api->isLoggedIn()) {

                if (is_array(json_decode($rfcWorkflows->getObjects())) && count(json_decode($rfcWorkflows->getObjects())) > 0) {
                    $cmdbObjects = new CMDBObjects($api);
                    $affected_components = $cmdbObjects->readByIDs(json_decode($rfcWorkflows->getObjects()));
                }

            }
        }
        // End Fetch i-doit Objects

        // Fetch Object types
        if ($api->isLoggedIn()) {
            $cmdbObjectTypes = new CMDBObjectTypes($api);
            $object_types = $cmdbObjectTypes->read();
        } else {
            $object_types = array();
        }


        // Ticket
        // Ticket association Logic
        if ($rfcWorkflows->getReferencedTicketid()) {
            $zammad = new ZammadController();
            $ticket = $zammad->get_ticket_by_id($rfcWorkflows->getReferencedTicketid(), $entityManager)->getValues();
            $zammad_config = $zammad->get_config($entityManager);
            if ($zammad_config['active']) {
                $ticket['url'] = str_replace('%id%', $ticket['id'], $zammad_config['pattern']);
            } else {
                $ticket['url'] = '';
                $ticket['number'] = '';
            }

        } else {
            $ticket = array(
                "id"       => "",
                "group_id" =>  "",
                "state_id" => "",
                "number"   => "",
                "title"    => "",
                "owner_id" => "",
                "url"      => "",
            );
        }

        // Attachments
        $attachments = json_decode($rfcWorkflows->getAttachment(),true);

        return $this->render('rfc_workflows/show.html.twig', array(
            'RfcWorkflows' => $rfcWorkflows,
            'child_tasks' => $child_tasks,
            'implementationStatus' => $implementationStatus,
            'sibling_tasks' => $sibling_tasks,
            'affected_components' => $affected_components,
            'config' => $config,
            'ticket' => $ticket,
            'object_types' => $object_types,
            'attachments' => $attachments,
        ));
    }
    
    /**
     * @Route ("/rfc/workflows/update", name="rfc_workflows_update", methods="POST")
     */
    public function update (EntityManagerInterface $entityManager, Request $request, MailerInterface $mailer, LoggerInterface $logger) {
        $repository = $entityManager->getRepository(RfcWorkflows::class);
        $rfcWorkflow = $repository->findOneBy(['id' => $request->request->get('rfc_workflow_id')]);

        $repository = $entityManager->getRepository(ImplementationStatus::class);
        $implementationStatus = $repository->findOneBy(['const' => $request->request->get('implementationStatus')]);

        $old_comment = $rfcWorkflow->getComment();

        $objects = array();
        if (is_array($request->request->get('objects'))){
            foreach ($request->request->get('objects') as $object) {
                $objects[] = $object;
            }
        }

        $rfcWorkflow->setImplementationStatus($implementationStatus)
            ->setComment($request->request->get('comment'))
            ->setObjects(json_encode($objects))
        ;
        $entityManager->persist($rfcWorkflow);


        $entityManager->flush();

        // Zammad logic
        $zammad = new ZammadController();
        $zammad_config = $zammad->get_config($entityManager);
        if ($zammad_config["active"] == 1 && $rfcWorkflow->getReferencedTicketid()) {
            //Update referenced Ticket
            if ($old_comment != $request->request->get('comment')) {
                $zammad->create_article($rfcWorkflow->getReferencedTicketid(), '', $request->request->get('comment'), $entityManager);
            }
            if ($implementationStatus->getConst() == 'C_STATUS_OPEN'){
                $zammad->update_ticket($rfcWorkflow->getReferencedTicketid(), $zammad_config["state_id_open"], $rfcWorkflow->getCreator(), $rfcWorkflow->getAssignee(), $entityManager);
            } elseif ($implementationStatus->getConst() == 'C_STATUS_CLOSED' || $implementationStatus->getConst() == 'C_STATUS_ABORTED'){
                $zammad->update_ticket($rfcWorkflow->getReferencedTicketid(), $zammad_config["state_id_closed"], $rfcWorkflow->getCreator(), $rfcWorkflow->getAssignee(), $entityManager);
            }
        }


        if ($implementationStatus->getConst() == 'C_STATUS_CLOSED') {
            $repository = $entityManager->getRepository(RfcWorkflows::class);
            $childWorkflows = $repository->findBy(['parent' => $rfcWorkflow->getId()]);

            $repository = $entityManager->getRepository(RfcWorkflows::class);
            if ($rfcWorkflow->getParent()) {
                $sibling_tasks = $repository->findBy(['parent' => $rfcWorkflow->getParent()->getId()]);
            } else {
                $sibling_tasks = $repository->findBy(['parent' => NULL, 'rfc' => $rfcWorkflow->getRfc()]);
            }

            $count_open_siblings = 0;
            if (is_array($sibling_tasks)) {
                foreach ($sibling_tasks as $sibling_task) {
                    if ($sibling_task->getImplementationStatus()->getConst() == 'C_STATUS_NEW' || $sibling_task->getImplementationStatus()->getConst() == 'C_STATUS_OPEN') {
                        $count_open_siblings++;
                    }

                }
            }

            if (count($childWorkflows) > 0 || $count_open_siblings > 0) {
                foreach ($childWorkflows as $childWorkflow) {
                    if ($childWorkflow->getImplementationStatus()->getConst() == 'C_STATUS_NEW') $this->notify($childWorkflow, $entityManager, $request, $mailer, $logger);
                }
                foreach ($sibling_tasks as $sibling_task) {
                    if ($sibling_task->getImplementationStatus()->getConst() == 'C_STATUS_NEW') $this->notify($sibling_task, $entityManager, $request, $mailer, $logger);
                }
            } else {
                // the last task is closed
                $repository = $entityManager->getRepository(Status::class);
                $status = $repository->findOneBy(['const' => 'C__STATUS__VALIDATION']);
                $rfc = $rfcWorkflow->getRfc();

                $old_status_title = $rfc->getStatus()->getTitle();
                $new_status_title = $status->getTitle();
                $rfc->setStatus($status);

                $rfc_notice_content = <<<EOD
```
Previous state: $old_status_title
New state:      $new_status_title
```
EOD;


                $rfc_notice = new RfcNotices();
                $rfc_notice->setRfc($rfc)
                    ->setTitle('Logbook entry')
                    ->setContent($rfc_notice_content)
                    ->setStatus($status)
                    ->setCreated(new \DateTime())
                    ->setAuthor($this->getUser())
                ;

                $entityManager->persist($rfc_notice);

                $entityManager->persist($rfc);
                $entityManager->flush();
                $this->notify_rfc_creator($rfc, $entityManager, $request, $mailer);
            }
        } else if ($implementationStatus->getConst() == 'C_STATUS_ABORTED') {
            // get childs an set the to aborted too
            $repository = $entityManager->getRepository(RfcWorkflows::class);
            $childWorkflows = $repository->findBy(['parent' => $rfcWorkflow->getId()]);
            if (count($childWorkflows) > 0) {
                foreach ($childWorkflows as $childWorkflow) {
                    if ($childWorkflow->getImplementationStatus()->getConst() == 'C_STATUS_ABORTED') {
                        $childWorkflow->setImplementationStatus($implementationStatus);
                    }
                }
                $repository = $entityManager->getRepository(Status::class);
                $status = $repository->findOneBy(['const' => 'C__STATUS__FAILED']);
                $rfc = $rfcWorkflow->getRfc();

                $old_status_title = $rfc->getStatus()->getTitle();
                $new_status_title = $status->getTitle();
                $rfc->setStatus($status);

                $rfc_notice_content = <<<EOD
```
Previous state: $old_status_title
New state:      $new_status_title
```
EOD;


                $rfc_notice = new RfcNotices();
                $rfc_notice->setRfc($rfc)
                    ->setTitle('Logbook entry')
                    ->setContent($rfc_notice_content)
                    ->setStatus($status)
                    ->setCreated(new \DateTime())
                    ->setAuthor($this->getUser())
                ;

                $entityManager->persist($rfc_notice);

                $entityManager->persist($rfc);
                $entityManager->flush();
                $this->notify_rfc_creator($rfc, $entityManager, $request, $mailer);
            }
        }

        return $this->redirectToRoute('rfc_workflows_detail', array('id' => $rfcWorkflow->getId()));
    }

    /**
     * @Route ("/rfc/workflows/delete/{id<(\d)+>}", name="rfc_workflows_delete", methods="GET")
     */
    public function delete (EntityManagerInterface $entityManager, RfcWorkflows $rfcWorkflows) {
        /** @var RfcWorkflows|null $rfcWorkflows */
        if (!$rfcWorkflows) {
            throw $this->createNotFoundException('Invalid Workflow id');
        }

        if ($rfcWorkflows->getImplementationStatus()->getConst() == 'C_STATUS_NEW') {
            if (!$rfcWorkflows->getCreator() || $rfcWorkflows->getCreator() == $this->getUser()) {
                $rfc_id = $rfcWorkflows->getRfc()->getId();
                $entityManager->remove($rfcWorkflows);
                $entityManager->flush();
            } else {
                throw new \Exception('Only the creator of an Workflow is allowed to delete it');
            }
        } else {
            throw new \Exception('Only in state new a Workflow may be deleted');
        }


        return $this->redirectToRoute('rfc_details', array('id' => $rfc_id));
    }

    public function notify(RfcWorkflows $rfcWorkflow, EntityManagerInterface $entityManager, Request $request, MailerInterface $mailer, LoggerInterface $logger) {
        $deeplink = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . '/rfc/workflows/' . $rfcWorkflow->getId();

        $firstname = $rfcWorkflow->getAssignee()->getFirstname();
        $subject   = '[RfC# ' . $rfcWorkflow->getRfc()->getId() . '] A RfC implementation task was assigned to you';
        $title     = $rfcWorkflow->getTitle();
        $description = $rfcWorkflow->getDescription();
        $start_date  = $rfcWorkflow->getStartDate()->format("d.m.Y");
        $end_date    = $rfcWorkflow->getEndDate()->format("d.m.Y");


        $message = <<<EOD
Hello $firstname,

a Request for Change implemetation task has been assigned to you.

| | |
|-|-|
| **Title:** | $title |
| **Start date:** | $start_date |
| **End date:** | $end_date |


Please open the [RfC Manager]($deeplink) and accept the task.
EOD;

        $message = $this->markdownConverter->convert($message);
        $email = new EmailController();
        $email->send_email($entityManager, $rfcWorkflow->getAssignee(), $message, $subject, $mailer);

        // Zammad Logic
        $zammad = new ZammadController();
        $zammad_config = $zammad->get_config($entityManager);
        if ($zammad_config["active"] == 1) {
            if (!$rfcWorkflow->getReferencedTicketid()) {
                $ticket = $zammad->create_ticket($rfcWorkflow->getTitle(), $rfcWorkflow->getDescription(), $rfcWorkflow->getCreator(), $rfcWorkflow->getAssignee(), $entityManager, $logger);
            }
            if (isset($ticket)) {
                $zammad->add_tag($ticket->getId(), 'RfC_' . $rfcWorkflow->getRfc()->getId(), $entityManager);
                $rfcWorkflow->setReferencedTicketid($ticket->getId());
            } else {
                $rfcWorkflow->setReferencedTicketid($rfcWorkflow->getReferencedTicketid());
            }
            $entityManager->persist($rfcWorkflow);
            $entityManager->flush();
        }

    }

    public function notify_rfc_creator(Rfc $rfc, EntityManagerInterface $entityManager, Request $request, MailerInterface $mailer) {
        $deeplink = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . '/rfc/' . $rfc->getId();

        if ($rfc->getStatus()->getConst() == 'C__STATUS__FAILED') {
            $status_description   = 'failed';
            $request_for_action   = 'review';
        } else {
            $status_description   = 'been completed';
            $request_for_action   = 'validate the implementation';
        }

        $firstname = $rfc->getCreator()->getFirstname();
        $subject   = '[RfC# ' . $rfc->getId() . '] The implementation of your Request for Change has ' . $status_description;
        $title     = $rfc->getTitle();
        $target_date = $rfc->getDate()->format("d.m.Y");
        $end_date    = new \DateTime();
        $end_date = $end_date->format("d.m.Y");


        $message = <<<EOD
Hello $firstname,

the implementation of your Request for Change has $status_description.

| | |
|-|-|
| **Title:** | $title |
| **Target date:** | $target_date |
| **End date:** | $end_date |


Please open the [RfC Manager]($deeplink) to $request_for_action.
EOD;

        $message = $this->markdownConverter->convert($message);
        $email = new EmailController();
        $email->send_email($entityManager, $rfc->getCreator(), $message, $subject, $mailer);

        // Zammad logic
        $zammad = new ZammadController();
        $zammad_config = $zammad->get_config($entityManager);
        if ($zammad_config["active"] == 1 && $rfc->getReferencedTicketid()) {
            //Update referenced Ticket
            if ($rfc->getStatus()->getConst() == 'C__STATUS__VALIDATION'){
                $zammad->remove_tag($rfc->getReferencedTicketid(), 'RfC_Status_Implementation', $entityManager);
                $zammad->add_tag($rfc->getReferencedTicketid(), 'RfC_Status_Validation', $entityManager);
            } elseif ($rfc->getStatus()->getConst() == 'C__STATUS__FAILED'){
                $zammad->remove_tag($rfc->getReferencedTicketid(), 'RfC_Status_Implementation', $entityManager);
                $zammad->add_tag($rfc->getReferencedTicketid(), 'RfC_Status_Failed', $entityManager);
            }
        }
    }
}
