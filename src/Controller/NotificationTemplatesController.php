<?php

namespace App\Controller;

use App\Entity\Notificationtemplates;
use App\Entity\Notificationtypes;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Extra\Markdown\DefaultMarkdown;

class NotificationTemplatesController extends AbstractController
{
    // ToDo: Implement Template Usage
    private DefaultMarkdown $markdownConverter;

    public function __construct()
    {
        $this->markdownConverter = new DefaultMarkdown();
    }
    /**
     * @Route("/notification/templates", name="notification_templates")
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $repository = $entityManager->getRepository(Notificationtemplates::class);
        $notification_templates = $repository->findAll();
        $repository = $entityManager->getRepository(Notificationtypes::class);
        $notification_types = $repository->findAll();

        return $this->render('notification_templates/index.html.twig', [
            'notification_templates' => $notification_templates,
            'notification_types'     => $notification_types
        ]);
    }

    /**
     * @Route ("/notification/templates/save", name="notification_templates_save")
     */
    public function save(EntityManagerInterface $entityManager, Request $request) {
        $message = '';

        $repository = $entityManager->getRepository(Notificationtypes::class);
        $type = $repository->findOneBy(["id" => $request->request->get("type")]);

        $repository = $entityManager->getRepository(Notificationtemplates::class);
        $template = $repository->findOneBy(["id" => $request->request->get("id")]);



        if (!$template) {
            $template = new Notificationtemplates();
        }
        $template->setActive(1);
        $template->setContentDe($this->markdownConverter->convert($request->request->get('content_de')));
        $template->setContentEn($this->markdownConverter->convert($request->request->get("content_en")));
        $template->setType($type);
        $template->setSubject($request->request->get("subject"));

        $entityManager->persist($template);
        $entityManager->flush();

        $message .= '<span class="">Template saved successfully</span>';
        return new JsonResponse(["message" => $message]);
    }
}
