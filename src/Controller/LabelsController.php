<?php

namespace App\Controller;

use App\Entity\Labels;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LabelsController extends AbstractController
{
    /**
     * @Route("/labels", name="app_labels")
     */
    public function index(EntityManagerInterface $entityManager): Response
    {

        $repository = $entityManager->getRepository(User::class);
        $users = $repository->findBy([], ['lastname' => 'ASC']);
        $repository = $entityManager->getRepository(Labels::class);
        $labels = $repository->findAll();

        return $this->render('labels/index.html.twig', [
            'selection_list' => $labels,
            'users'           => $users
        ]);
    }

    /**
     * @Route ("/label/new", name="label_new", methods="GET")
     */
    public function new(EntityManagerInterface $entityManager) {
        $repository = $entityManager->getRepository(Labels::class);

        $label = new Labels();
        $label->setTitle('Untitled')
            ->setDriver('')
            ->setActive(1)
            ->setCreator($this->getUser())
            ->setCreated(new \DateTime())
        ;

        $entityManager->persist($label);
        $entityManager->flush();
        return $this->redirectToRoute('label_details', array('id' => $label->getId()));
    }

    /**
     * @Route ("/label/{id<(\d)+>}", name="label_details", methods={"GET"})
     */
    public function show($id, EntityManagerInterface $entityManager, LoggerInterface $logger){
        $repository = $entityManager->getRepository(Labels::class);
        $label = $repository->find($id);

        /** @var Labels|null $label */
        if (!$label) {
            throw $this->createNotFoundException('This label does not exist');
        }

        if ($label->getZmlCode()){
            $curl = curl_init();
            // adjust print density (8dpmm), label width (4 inches), label height (6 inches), and label index (0) as necessary
            $label_width   = 2;
            $label_height  = 1;
            $print_density = 8;

            if ($label->getPrintDensity() && $label->getLabelHeight() && $label->getLabelWidth()) {
                $label_width   = $label->getLabelWidth()/25.4;
                $label_height  = $label->getLabelHeight()/25.4;
                $print_density = $label->getPrintDensity();
            }
            $api_url = 'http://api.labelary.com/v1/printers/' . $print_density . 'dpmm/labels/' . $label_width . 'x' . $label_height . '/0/';
            curl_setopt($curl, CURLOPT_URL, $api_url);
            curl_setopt($curl, CURLOPT_POST, TRUE);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $label->getZmlCode());
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            $label_image = curl_exec($curl);

            if (curl_getinfo($curl, CURLINFO_HTTP_CODE) == 200) {

            } else {
                //dd("Error: $label_image");
                $logger->error($label_image);
                $label_image = '';
            }

            curl_close($curl);
        } else {
            $label_image = '';
        }

        return $this->render('labels/show.html.twig', array(
            'label' => $label,
            'label_image' => base64_encode($label_image),
        ));
    }

    /**
     * @Route ("/label/save", name="label_save", methods={"POST"})
     */
    public function save(EntityManagerInterface $entityManager, Request $request) {
        $repository = $entityManager->getRepository(Labels::class);
        $label = $repository->findOneBy(['id' => $request->request->get('id')]);
        if (!$label) {
            throw $this->createNotFoundException('This label does not exist');
        }

        $active = 0;
        if ($request->request->get('active') == '1') {
            $active = 1;
        }
        $label->setTitle($request->request->get('title'))
            ->setActive($active)
            ->setDriver($request->request->get('driver'))
            ->setZmlCode($request->request->get('zml_code'))
            ->setRowsOnPage((int) $request->request->get('rows_on_page'))
            ->setColsOnRow((int) $request->request->get('cols_on_row'))
            ->setPageSize((int) $request->request->get('page_size'))
            ->setPageHeight((int) $request->request->get('page_height'))
            ->setPageWidth((int) $request->request->get('page_width'))
            ->setOrientation($request->request->get('orientation'))
            ->setMarginLeft((int) $request->request->get('margin_left'))
            ->setMarginTop((int) $request->request->get('margin_top'))
            ->setQrSize((int) $request->request->get('qr_size'))
            ->setLabelWidth((int) $request->request->get('label_width'))
            ->setLabelHeight((int) $request->request->get('label_height'))
            ->setLogoHeight((int) $request->request->get('logo_height'))
            ->setLogoSrc($request->request->get('logo_src'))
            ->setFontsize1((int) $request->request->get('fontsize_1'))
            ->setFontsize2((int) $request->request->get('fontsize_2'))
            ->setPrintDensity((int) $request->request->get('print_density'))
            ->setAttribute1($request->request->get('attribute_1'))
            ->setAttribute2($request->request->get('attribute_2'))
            ->setAttribute3($request->request->get('attribute_3'))
            ->setAttribute4($request->request->get('attribute_4'))
            ;

        $entityManager->persist($label);
        $entityManager->flush();

        return $this->redirectToRoute('label_details', array('id' => $label->getId()));
    }


}
