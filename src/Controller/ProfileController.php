<?php

namespace App\Controller;

use App\Entity\Departments;
use App\Entity\Roles;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class ProfileController extends AbstractController
{
    /**
     * @Route("/profile", name="profile")
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $repository = $entityManager->getRepository(Departments::class);
        $departments = $repository->findBy([],['title' => 'ASC']);

        return $this->render('profile/index.html.twig', [
            'departments'     => $departments,
        ]);
    }

    /**
     * @Route ("/profile/update", name="profile_update", methods="POST")
     */
    public function update(EntityManagerInterface $entityManager, Request $request, UserPasswordHasherInterface $userPasswordHasher)
    {
        //dd($request->request);

        $repository = $entityManager->getRepository(Departments::class);
        $department = $repository->findOneBy(["id" => $request->request->get("department")]);

        $repository = $entityManager->getRepository(User::class);
        $user       = $repository->findOneBy(["id" => $request->request->get("id")]);

        $user->setDepartment($department);
        $user->setFirstname($request->request->get("firstname"))
            ->setLastname($request->request->get("lastname"))
            ->setEmail($request->request->get("email"))
            ->setLanguage($request->request->get("language"))
        ;

        if (
            $request->request->get("password") &&
            $request->request->get("password") != '' &&
            $request->request->get('confirm_password' == $request->request->get("password"))
        ) {
            $user->setPassword($userPasswordHasher->hashPassword($user, $request->request->get("password")));
        }

        $request->getSession()->set('_locale', $request->request->get("language"));


        $entityManager->persist($user);
        $entityManager->flush();

        return $this->redirectToRoute('profile');

    }

}
