<?php

namespace App\Controller;

use App\Entity\Departments;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DepartmentsController extends AbstractController
{
    /**
     * @Route("/departments", name="departments")
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $repository = $entityManager->getRepository(Departments::class);
        return $this->render('departments/index.html.twig', [
            'selection_list' => $repository->findAll(),
        ]);
    }

    /**
     * @Route ("/departments/save", name="departments_save")
     */
    public function save(EntityManagerInterface $entityManager, Request $request) {
        $message = 'Department saved successfully';
        $repository = $entityManager->getRepository(Departments::class);
        $department = $repository->findOneBy(['id' => $request->request->get("id")]);

        if (!$department) {
            $department = new Departments();
            $department->setLocked(1);
        }

        $department->setTitle($request->request->get("title"));
        $entityManager->persist($department);
        $entityManager->flush();
        $items = $repository->findAll();

        return $this->redirectToRoute('departments');
    }
}
