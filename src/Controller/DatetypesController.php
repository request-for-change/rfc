<?php

namespace App\Controller;

use App\Entity\Costcenters;
use App\Entity\Datetypes;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DatetypesController extends AbstractController
{
    /**
     * @Route("/datetypes", name="datetypes")
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $repository = $entityManager->getRepository(Datetypes::class);
        return $this->render('datetypes/index.html.twig', [
            'selection_list' => $repository->findAll(),
        ]);
    }

    /**
     * @Route ("/datetypes/save", name="datetypes_save")
     */
    public function save(EntityManagerInterface $entityManager, Request $request) {
        $message = '';

        $repository = $entityManager->getRepository(Datetypes::class);
        $date_types = $repository->findOneBy(["id" => $request->request->get("id")]);

        if (!$date_types) {
            $date_types = new Datetypes();
            $date_types->setLocked(0);
        }

        $date_types->setTitle($request->request->get("title"));
        $entityManager->persist($date_types);
        $entityManager->flush();

        $items = $repository->findAll();
        $trs = array();
        foreach ($items as $item) {
            $trs[] = [
                "id" => $item->getId(),
                "title" => $item->getTitle()
            ];

        }

        return $this->redirectToRoute('datetypes');
    }
}
