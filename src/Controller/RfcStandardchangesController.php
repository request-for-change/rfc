<?php

namespace App\Controller;

use App\Entity\Ressourcetypes;
use App\Entity\Rfc;
use App\Entity\RfcStandardchanges;
use App\Entity\Standardchangesteps;
use App\Entity\Units;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RfcStandardchangesController extends AbstractController
{
    /**
     * @Route("/standardchanges", name="standardchanges")
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $repository = $entityManager->getRepository(User::class);
        $users = $repository->findBy([], ['lastname' => 'ASC']);
        $repository = $entityManager->getRepository(RfcStandardchanges::class);
        $rfc_standardchanges = $repository->findBy(['active' => 1], ['title' => 'ASC']);

        return $this->render('rfc_standardchanges/index.html.twig', [
            'selection_list' => $rfc_standardchanges,
            'users'           => $users
        ]);
    }

    /**
     * @Route ("/standardchanges/save", name="standardchanges_save")
     *
     */
    public function save(EntityManagerInterface $entityManager, Request $request){
        $message = '';
        $repository = $entityManager->getRepository(Standardchangesteps::class);
        $standardchangestep = $repository->findOneBy(['const' => 'C__SS_STEP__GENESIS']);

        $repository = $entityManager->getRepository(RfcStandardchanges::class);
        $standardchange = $repository->findOneBy(['id' => $request->request->get("id")]);

        if (!$standardchange) {
            $standardchange = new RfcStandardchanges();
            $standardchange->setCreated(new \DateTime())
                ->setCreator($this->getUser())
                ->setStep($standardchangestep)
                ->setWorkflowdata('')
            ;
        }

        $active = 0;
        if ($request->request->get('active') == '1') {
            $active = 1;
        }

        $standardchange->setTitle($request->request->get('title'))
            ->setActive($active)
            ->setDescription($request->request->get('description'))
            ->setBasedata('')
            ->setClassificationdata('')
            ->setWorkflowdata($request->request->get('workflowdata'))
            ->setClassificationdata(json_encode(array(
                'rtype' => $request->request->get('rtype'),
                'costs' => $request->request->get('costs'),
                'rtime' => $request->request->get('rtime'),
                'runit' => $request->request->get('runit'),
                'hardware_software' => $request->request->get('hardware_software'),
                'riskanalysis' => $request->request->get('riskanalysis')
            )))
        ;
        
        $entityManager->persist($standardchange);
        $entityManager->flush();

        return $this->redirectToRoute('standardchange_details', array('id' => $standardchange->getId()));

    }

    /**
     * @Route ("/standardchange/new", name="standardchange_new", methods="GET")
     */
    public function new(EntityManagerInterface $entityManager) {
        $repository = $entityManager->getRepository(Standardchangesteps::class);
        $standardchangestep = $repository->findOneBy(['const' => 'C__SS_STEP__GENESIS']);

        $standardchange = new RfcStandardchanges();
        $standardchange->setCreated(new \DateTime())
                        ->setCreator($this->getUser())
                        ->setStep($standardchangestep)
                        ->setWorkflowdata('[]')
                        ->setTitle('untitled')
                        ->setDescription('')
                        ->setBasedata('')
                        ->setClassificationdata('')
                        ->setActive(1)
        ;
        $entityManager->persist($standardchange);
        $entityManager->flush();
        return $this->redirectToRoute('standardchange_details', array('id' => $standardchange->getId()));
    }

    /**
     *  @Route("/standardchange/{id<(\d)+>}", name="standardchange_details", methods="GET")
     */
    public function show($id, EntityManagerInterface $entityManager){
        $repository = $entityManager->getRepository(RfcStandardchanges::class);
        $standardchange = $repository->findOneBy(['id' => $id]);

        /** @var RfcStandardchanges|null $standardchange */
        if (!$standardchange) {
            throw $this->createNotFoundException('This standard change does not exist');
        }

        $standardchange_workflows = array();
        foreach (json_decode($standardchange->getWorkflowdata()) as $tasks) {
            if (is_array($tasks)) {
                foreach ($tasks as $task) {
                        $standardchange_workflow[$task->name] = $task->value;
                }
                if ($standardchange_workflow['workflow_id']){
                    $standardchange_workflows[$standardchange_workflow['workflow_id']] = $standardchange_workflow;
                }
            }
        }

        //dd($standardchange_workflows);

        $repository = $entityManager->getRepository(Ressourcetypes::class);
        $ressources = $repository->findAll();
        $repository = $entityManager->getRepository(Units::class);
        $units = $repository->findAll();
        $system = new SystemController();
        $system_config = $system->get_config($entityManager);


        return $this->render('rfc_standardchanges/show.html.twig', array(
            'standardchange' => $standardchange,
            'standardchange_workflows' => $standardchange_workflows,
            'workflows_data' => json_decode($standardchange->getWorkflowdata()),
            'classification_data' => json_decode($standardchange->getClassificationdata()),
            'system_config' => $system_config,
            'units' => $units,
            'ressources' => $ressources,
        ));

    }

}
