<?php

namespace App\Controller;

use App\Entity\Config;
use App\Entity\Labels;
use App\Entity\Roles;
use bheisig\idoitapi\API;
use bheisig\idoitapi\CMDBCategory;
use bheisig\idoitapi\CMDBObject;
use bheisig\idoitapi\CMDBObjects;
use bheisig\idoitapi\CMDBObjectTypes;
use bheisig\idoitapi\CMDBReports;
use bheisig\idoitapi\tests\CMDBReportsTest;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class LabelPrinterController extends AbstractController
{
    public function __construct(RequestStack $requestStack, EntityManagerInterface $entityManager)
    {
        $this->requestStack = $requestStack;
    }

    /**
     * @Route("/label/printer", name="app_label_printer")
     */
    public function index(Request $request, EntityManagerInterface $entityManager): Response
    {
        $sort = [
            'by' => '',
            'direction' => 'asc',
        ];

        $session = $this->requestStack->getSession();

        // Clear session filter if form was sent
        if ($request->query->get('fresh')) {
            $session->remove('filter');
        }

        $filter = $session->get('filter', [
            'object_type'  => '',
            'report'       => '',
            'object_title' => '',
            'workplace'    => '',
        ]);

        $object_types = array();
        $reports      = array();
        $workplaces   = array();
        $objects      = array();

        foreach ($filter as $parameter => $value) {
            if ($request->query->get($parameter)) {
                $filter[$parameter] = $request->query->get($parameter);
            }
        }
        $session->set('filter', $filter);

        //dd($filter);


        // Fetch i-doit Object Groups and affected_components

        $api = $this->get_idoit_api($entityManager);

        if ($api) {
            if ($api->isLoggedIn()) {
                $cmdbObjectTypes = new CMDBObjectTypes($api);
                try {
                    $object_types = $cmdbObjectTypes->read();
                } catch (\Exception $e) {
                    // do nothing
                }

                $cmdbReports = new CMDBReports($api);
                try {
                    $temp_reports = $cmdbReports->listReports();
                    foreach ($temp_reports as $report) {
                        $reports[$report['id']] = $report;
                    }
                } catch (\Exception $e) {
                    // do nothing
                }

                $cmdbObjects = new CMDBObjects($api);
                try {
                    $workplaces = $cmdbObjects->read(['type' => 'C__OBJTYPE__WORKSTATION'], 1000, 0, 'title', CMDBObjects::SORT_DESCENDING);
                } catch (\Exception $e) {
                    // do nothing
                }

                if ($filter['workplace']) { // Only look for assigned Objects
                    try {

                        $cmdbCategory = new CMDBCategory($api);
                        $assigned_objects = $cmdbCategory->read($filter['workplace'], 'C__CATG__ASSIGNED_LOGICAL_UNIT');
                        if (is_array($assigned_objects)) {
                            foreach ($assigned_objects as $assigned_object) {
                                $objects[] = $assigned_object["assigned_object"];
                            }
                        }

                    } catch (\Exception $e) {
                        // do nothing
                    }

                }

                if ($filter['object_title']) { // Look for objects matching the filter
                    $object_title = '%' . $filter['object_title'] . '%';

                    try {
                        if ($filter['object_type']) {
                            $objects = array_merge($objects,$cmdbObjects->read(['type' => $filter['object_type'], 'title' => $object_title], 200, 0, 'title', CMDBObjects::SORT_DESCENDING));
                        } else {
                            $objects = array_merge($objects,$cmdbObjects->read(['title' => $object_title], 200, 0, 'title', CMDBObjects::SORT_DESCENDING));
                        }
                    } catch (\Exception $e) {
                        // do nothing
                    }
                }

                if ($filter['report']) { // Look for objects matching the report
                    try {
                        $report = new CMDBReports($api);
                        $resulting_objects = $report->read($filter['report']);

                        if (is_array($resulting_objects)) {
                            $ids = array();
                            foreach ($resulting_objects as $resulting_object) {
                                $ids[] = $resulting_object['ID'];
                            }
                            $objects = array_merge($objects, $cmdbObjects->read(['ids' => $ids], 200, 0, 'title', CMDBObjects::SORT_DESCENDING));
                        }

                    } catch (\Exception $e) {
                        // do nothing
                    }
                }

            }
        }
        // End Fetch i-doit Objects and Groups


        $repository = $entityManager->getRepository(Labels::class);
        $labels = $repository->findBy(["active" => true], ['title' => "DESC"]);

        return $this->render('label_printer/index.html.twig', [
            'sort'         => $sort,
            'filter'       => $filter,
            'object_types' => $object_types,
            'reports'      => $reports,
            'workplaces'   => $workplaces,
            'objects'      => $objects,
            'labels'       => $labels,
        ]);
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return Response
     * @Route ("/label/printer/print", name="label_printer_print")
     */
    public function print(Request $request, EntityManagerInterface $entityManager, TranslatorInterface $translator){
        $labels = array();
        $zpl = array();
        $api = $this->get_idoit_api($entityManager);
        $message = '';
        $error = '';
        $redirect = '';
        $session = $this->requestStack->getSession();
        $repository = $entityManager->getRepository(Labels::class);
        if (is_array($request->request->get('obj_ids'))) {
            foreach ($request->request->get('obj_ids') as $obj_id) {
                $labels[$obj_id] = $repository->findOneBy(['id' => $request->request->get($obj_id)]);

                if ($labels[$obj_id] and $labels[$obj_id]->getDriver()) {
                    if (isset($previous_driver) and $previous_driver != $labels[$obj_id]->getDriver()) {
                        $error = $translator->trans('Using labels with different types (pdf or zpl) in one print job is not supported.');
                        break;
                    }
                    $previous_driver = $labels[$obj_id]->getDriver();

                    if (isset($previous_label_width) and $previous_label_width != $labels[$obj_id]->getLabelWidth()) {
                        $error = $translator->trans('Using labels with different width in one print job is not supported.');
                        break;
                    }
                    $previous_label_width = $labels[$obj_id]->getLabelWidth();

                    if (isset($previous_label_height) and $previous_label_height != $labels[$obj_id]->getLabelHeight()) {
                        $error = $translator->trans('Using labels with different height in one print job is not supported.');
                        break;
                    }
                    $previous_label_height = $labels[$obj_id]->getLabelHeight();

                    if (isset($previous_page_width) and $previous_page_width != $labels[$obj_id]->getPageWidth()) {
                        $error = $translator->trans('Using labels with different page width in one print job is not supported.');
                        break;
                    }
                    $previous_page_width = $labels[$obj_id]->getPageWidth();

                    if (isset($previous_page_height) and $previous_page_height != $labels[$obj_id]->getPageHeight()) {
                        $error = $translator->trans('Using labels with different page height in one print job is not supported.');
                        break;
                    }
                    $previous_page_height = $labels[$obj_id]->getPageHeight();

                } else {
                    $error = $translator->trans('Choose a label for each selected object.');
                }
            }
        } else {
            $error = $translator->trans('Select at least one object to print a label for.');
        }

        if ($error == '') {
            if (isset($previous_driver) and $previous_driver == "ZPL") {
                $redirect = "printer/zpl";
                $session->set('labels', $labels);
                $session->set('label_image', false);
            } else {
                $redirect = "printer/pdf";
            }

        } else {
            $message = '<span class="">' . $error . '</span>';
        }

        return new JsonResponse([
            'message' => $message,
            'error' => $error,
            'redirect' => $redirect,
        ]);
    }


    /**
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return Response
     * @Route ("/label/printer/zpl", name="label_printer_zpl")
     */
    public function send_zpl(Request $request, EntityManagerInterface $entityManager, TranslatorInterface $translator) {
        $api = $this->get_idoit_api($entityManager);
        $hostaddress = '';
        $session = $this->requestStack->getSession();
        $labels = $session->get('labels', [array()]);
        $zpl_strings = array();
        $idoit_config = $this->get_idoit_config($entityManager);

        foreach ($labels as $obj_id => &$label) {
            $cmdbObject = new CMDBObject($api);
            $idoit_obj = $cmdbObject->read($obj_id);
            $cmdbCategory = new CMDBCategory($api);
            // Serial Number
            $idoit_model = $cmdbCategory->read($obj_id, 'C__CATG__MODEL');
            if (is_array($idoit_model)) {
                $idoit_model = array_shift($idoit_model);
            }
            if (isset($idoit_model['serial'])) {
                $serialno = $idoit_model['serial'];
            } else {
                $serialno = '';
            }
            // Primary IPv4
            $idoit_ip = $cmdbCategory->read($obj_id, 'C__CATG__IP');
            if (is_array($idoit_ip)) {
                $idoit_ip = array_shift($idoit_ip);
            }
            if (isset($idoit_ip['primary_hostaddress']) && isset($idoit_ip['primary_hostaddress']['ref_title'])) {
                $primaryip = $idoit_ip['primary_hostaddress']['ref_title'];
            } else {
                $primaryip = '';
            }
            // delivery_date
            $idoit_accounting = $cmdbCategory->read($obj_id, 'C__CATG__ACCOUNTING');
            if (is_array($idoit_accounting)) {
                $idoit_accounting = array_shift($idoit_accounting);
            }
            if (isset($idoit_accounting['delivery_date'])) {
                $delivery_date = $idoit_accounting['delivery_date'];
            } else {
                $delivery_date = '';
            }

            $zpl_lines = explode("\r\n", $label->getZmlCode());

            $replacements = array(
                "{{obj.id}}" => $idoit_obj["id"],
                "{{obj.title}}" => $idoit_obj["title"],
                "{{serialno}}" => $serialno,
                "{{delivery_date}}" => $delivery_date,
                "{{primaryip}}" => $primaryip,
                "{{idoit.url}}" => $idoit_config['idoit_url']
            );
            foreach ($zpl_lines as &$zpl_line) {
                $zpl_line = strtr($zpl_line, $replacements);
            }
            $zpl_strings[] = implode("\r\n", $zpl_lines);
        }

        $zpl_string = implode("\r\n", $zpl_strings);
        //$zpl_string = array_shift($zpl_strings);

        if (!$session->get('label_image', false)) {
            $curl = curl_init();
            $label_width = 2;
            $label_height = 1;
            $print_density = 8;
            if ($label->getPrintDensity() && $label->getLabelHeight() && $label->getLabelWidth()) {
                $label_width = $label->getLabelWidth() / 25.4;
                $label_height = $label->getLabelHeight() / 25.4;
                $print_density = $label->getPrintDensity();
            }
            $api_url = 'http://api.labelary.com/v1/printers/' . $print_density . 'dpmm/labels/' . $label_width . 'x' . $label_height . '/0/';
            curl_setopt($curl, CURLOPT_URL, $api_url);
            curl_setopt($curl, CURLOPT_POST, TRUE);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $zpl_string);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            $label_image = curl_exec($curl);
            $session->set('label_image', $label_image);
            if (curl_getinfo($curl, CURLINFO_HTTP_CODE) == 200) {

            } else {
                //dd("Error: $label_image");
                $label_image = '';
            }
            curl_close($curl);
        } else {
            $label_image = $session->get('label_image', false);
        }


        if ($request->request->get('hostaddress')) {
            $hostaddress = $request->request->get('hostaddress');

            try {
                $socket = stream_socket_client('tcp://' . $hostaddress . ':6101', $errno, $errstr, 3);
                if ($socket) {
                    // Send data

                    fwrite($socket, $zpl_string);
                    $this->addFlash('success', $translator->trans('Data sent to printer.'));
                }
            } catch (\Exception $e) {
                $this->addFlash('danger', $translator->trans('Could not connect to printer'));
            }
        }
        return $this->render('label_printer/zpl_print.html.twig',[
            'label_image' => base64_encode($label_image),
            'hostaddress' => $hostaddress,
            'label' => $label,
        ]);
    }


    private function get_idoit_api($entityManager) {
        $config = $this->get_idoit_config($entityManager);
        $url_parts = parse_url($config['idoit_url']);

        if (isset($url_parts["port"])) {
            $config['port'] = (int)$url_parts["port"];
        } elseif (isset($url_parts["scheme"]) && $url_parts["scheme"] == "https") {
            $config['port'] = 443;
        } else {
            $config['port'] = 80;
        }

        if ($this->getUser()->getLanguage() == '') {
            $config["api_lang"] = 'en';
        } else {
            $config["api_lang"] = $this->getUser()->getLanguage();
        }

        if ($config['username'] != '' && $config['password'] != '' && $config['idoit_url'] != '') {
            $idoit_config = array(
                API::URL                      => $config['idoit_url'] . '/src/jsonrpc.php',
                API::PORT                     => $config['port'],
                API::KEY                      => $config['api_key'],
                API::USERNAME                 => $config['username'],
                API::PASSWORD                 => $config['password'],
                API::LANGUAGE                 => $config["api_lang"],
                API::BYPASS_SECURE_CONNECTION => true
            );
            $api = new API($idoit_config);
            try {
                $api->login();
                return $api;
            } catch (\Exception $exception) {
                return false;
            }
        }
    }

    private function get_idoit_config($entityManager) {
        $repository = $entityManager->getRepository(Config::class);
        $items      = $repository->findBy(["group_name" => "i-doit"]);

        foreach ($items as $item) {
            $config[$item->getConfigKey()] = unserialize($item->getConfigValue());
        }
        return $config;
    }
}
