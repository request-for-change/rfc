<?php

namespace App\Controller;

use App\Entity\Config;
use App\Repository\ConfigRepository;
use Doctrine\ORM\EntityManagerInterface;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Extra\Markdown\DefaultMarkdown;
use ZammadAPIClient\Client;
use ZammadAPIClient\ResourceType;

class ZammadController extends AbstractController
{

    private DefaultMarkdown $markdownConverter;

    public function __construct()
    {
        $this->markdownConverter = new DefaultMarkdown();
    }

    /**
     * @Route("/zammad", name="zammad")
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $repository = $entityManager->getRepository(Config::class);
        $items = $repository->findBy(["group_name" => "tts"]);

        foreach ($items as $item) {
            $config[$item->getConfigKey()] = unserialize($item->getConfigValue());
        }
        
        return $this->render('zammad/show.html.twig', [
            'config' => $config
        ]);
    }

    /**
     * @Route ("/zammad/save", name="zammad_save")
     */
    public function save(EntityManagerInterface $entityManager, Request $request) {
        $repository = $entityManager->getRepository(Config::class);
        $items = $repository->findBy(["group_name" => "tts"]);

        foreach ($items as $item) {
            $item->setConfigValue(serialize($request->request->get($item->getConfigKey())));
            $entityManager->persist($item);
            $entityManager->flush();
        }

        // Strip the request from the URL
        $url = substr($request->request->get('pattern'), 0, strpos($request->request->get('pattern'), "/#"));

        if ($url) {
            $client = new Client(
                [
                    'url' => $url,
                    'oauth2_token' => $request->request->get('auth_token')
                ]
            );
            $user = $client->resource(ResourceType::USER);
            $user->get('me');
            $login = $user->getValue('login');
        } else {
            $login = false;
        }

        if ($login) {
            $message = '<span class="alert-success">Service Desk Config saved successfully, for user '. $login .'</span>';
        } else {
            $message = '<span class="alert-danger">Login to Service Desk failed</span>';
        }


        return new JsonResponse([
            'message' => $message
        ]);

    }

    public function get_ticket_by_number($ticket_number, EntityManagerInterface $entityManager) {
        $repository = $entityManager->getRepository(Config::class);
        $items = $repository->findBy(["group_name" => "tts"]);

        foreach ($items as $item) {
            $config[$item->getConfigKey()] = unserialize($item->getConfigValue());
        }

        // Strip the request from the URL
        $url = substr($config['pattern'], 0, strpos($config['pattern'], "/#"));


        $client = new Client(
            [
                'url' => $url,
                'oauth2_token' => $config['auth_token']
            ]
        );

        // First we assume the user has given the ticket number. So let us search for this number
        $tickets = $client->resource(ResourceType::TICKET)->search('number:' . $ticket_number);

        // if we find exactly one ticket, return this ticket
        if ( is_array($tickets) and count($tickets) == 1) {
            $ticket = reset($tickets);
            return $ticket;
        } else {
            return false;
        }
    }

    public function get_ticket_by_id($ticket_id, EntityManagerInterface $entityManager) {
        $config = $this->get_config($entityManager);

        // Strip the request from the URL
        $url = substr($config['pattern'], 0, strpos($config['pattern'], "/#"));

        $client = new Client(
            [
                'url' => $url,
                'oauth2_token' => $config['auth_token']
            ]
        );

        $ticket = $client->resource(ResourceType::TICKET)->get($ticket_id);

        return $ticket;
    }

    public function create_ticket($title, $description, $customer, $owner, EntityManagerInterface $entityManager, LoggerInterface $logger) {
        $config = $this->get_config($entityManager);

        $logger->info('***** the Zammad::create_ticket() Methos was called *****');

        // Strip the request from the URL
        $url = substr($config['pattern'], 0, strpos($config['pattern'], "/#"));

        $client = new Client(
            [
                'url' => $url,
                'oauth2_token' => $config['auth_token']
            ]
        );

        // Look for Owner id and create User if not exists
        $zammad_customer = $this->search_user($customer->getEmail(), $entityManager);

        if (!$zammad_customer) {
            $zammad_customer = $this->create_user($customer, $client);
            if (!isset($zammad_customer['id'])) {
                throw new \Exception('Error, could not find and not create customer');
            }
        }

        $zammad_owner = '';
        /*
         * If owner is set AND user is Zammad User AND User is Agent AND Agent has rights in group
         */
        if ($owner) {
            $logger->info('Ticket shall be ' . $owner->getEmail());
            $zammad_user = $this->search_user($owner->getEmail(), $entityManager);
            if ($zammad_user) {
                $logger->info('Intended Ticket Owner is Zammad user with id ' . $zammad_user['id']);
                if(in_array('Agent',$zammad_user['roles'])) {
                    $logger->info('Intended Ticket Owner has the role Agent in Zammad');
                    if (array_key_exists($config['workflows_group'], $zammad_user['groups'])) {
                        $logger->info('Intended Ticket Owner is member of the intended Zammad group, where the ticket will be created');
                        $zammad_owner = $zammad_user['id'];
                    } else {
                        $logger->info('Intended Ticket Owner is not a member of the intended Zammad group, where the ticket will be created');
                    }
                } else {
                    $logger->info('Intended Ticket Owner is not an Agent in Zammad');
                }
            } else {
                $logger->info('Intended Ticket Owner is not known as Zammad user');
            }
        } else {
            $logger->info('No owner defined');
        }

        $ticket_data = [
            "group"       => $config['workflows_group'],
            "priority_id" => 1,
            "state_id"    => 1,
            "title"       => $title,
            "customer_id" => $zammad_customer['id'],
            "owner_id"    => $zammad_owner,
            "article"     => [
                "subject" => $title,
                "body"    => $this->markdownConverter->convert($description),
                "content_type" => "text/html",
            ],
        ];

        $ticket = $client->resource( ResourceType::TICKET );
        $ticket->setValues($ticket_data);
        $ticket->save();

        return $ticket;

    }

    public function create_user($user, $client) {
        $user_data = [
            'login' => $user->getEmail(),
            'email' => $user->getEmail(),
            'firstname' => $user->getFirstname(),
            'lastname'  => $user->getLastname(),
        ];
        $zammad_user = $client->resource( ResourceType::USER);
        $zammad_user->setValues($user_data);
        $zammad_user->save();
        return $zammad_user->getValues();
    }

    public function update_ticket($ticket_id, $state_id, $customer, $owner, EntityManagerInterface $entityManager) {
        // Look for Owner id and create User if not exists
        $customer = $this->search_user($customer->getEmail(), $entityManager);
        $owner = $this->search_user($owner->getEmail(), $entityManager);

        if ($customer && $owner) {
            $ticket_data = [
                "id" => $ticket_id,
                "state_id" => $state_id,
                "customer_id" => $customer['id'],
                "owner_id" => $owner ? $owner['id'] : '',
            ];
            $config = $this->get_config($entityManager);// Strip the request from the URL
            $url = substr($config['pattern'], 0, strpos($config['pattern'], "/#"));
            $client = new Client(
                [
                    'url' => $url,
                    'oauth2_token' => $config['auth_token']
                ]
            );
            $ticket = $client->resource(ResourceType::TICKET);
            $ticket->setValues($ticket_data);
            $ticket->save();
            return $ticket;
        } else {
            // ToDo: Add create customer code
        }
        return false;
    }

    public function create_article($ticket_id, $title, $description, EntityManagerInterface $entityManager) {
        $config = $this->get_config($entityManager);
        $url = substr($config['pattern'], 0, strpos($config['pattern'], "/#"));
        $client = new Client(
            [
                'url' => $url,
                'oauth2_token' => $config['auth_token']
            ]
        );
        $article = $client->resource(ResourceType::TICKET_ARTICLE);
        $article_data = [
            "ticket_id"=> $ticket_id,
            "to"=> "",
            "cc"=> "",
            "subject"=> $title,
            "body"=> $this->markdownConverter->convert($description),
            "content_type"=> "text/html",
            "type"=> "note",
            "internal"=> false,
        ];
        $article->setValues($article_data);
        $article->save();

        return $article;

    }

    public function search_user($email, EntityManagerInterface $entityManager) {
        $config = $this->get_config($entityManager);
        $url = substr($config['pattern'], 0, strpos($config['pattern'], "/#"));
        $client = new Client(
            [
                'url' => $url,
                'oauth2_token' => $config['auth_token']
            ]
        );

        $users = $client->resource(ResourceType::USER)->search($email);
        if (is_array($users) and count($users) > 0) {
            foreach ($users as $user) {
                return $user->getValues();
            }
        } else {
            //throw new \Exception('no user with email address ' . $email .' found in zammad');
            return false;
        }
        return false;
    }

    public function add_tag($ticket_id, $content, EntityManagerInterface $entityManager) {
        $config = $this->get_config($entityManager);
        $url = substr($config['pattern'], 0, strpos($config['pattern'], "/#"));
        $client = new Client(
            [
                'url' => $url,
                'oauth2_token' => $config['auth_token']
            ]
        );
        $tag = $client->resource(ResourceType::TAG);

        try {
            $tag->add($ticket_id, $content);
        } catch (\Exception $e) {
            // toDo: Add debug code
        }
    }

    public function remove_tag($ticket_id, $content, EntityManagerInterface $entityManager) {
        $config = $this->get_config($entityManager);
        $url = substr($config['pattern'], 0, strpos($config['pattern'], "/#"));
        $client = new Client(
            [
                'url' => $url,
                'oauth2_token' => $config['auth_token']
            ]
        );
        $tag = $client->resource(ResourceType::TAG);

        try {
            $tag->remove($ticket_id, $content);
        } catch (\Exception $e) {
            // toDo: Add debug code
        }
    }

    public function get_config(EntityManagerInterface $entityManager) {
        $repository = $entityManager->getRepository(Config::class);
        $items = $repository->findBy(["group_name" => "tts"]);

        foreach ($items as $item) {
            $config[$item->getConfigKey()] = unserialize($item->getConfigValue());
        }

        return $config;
    }
}
