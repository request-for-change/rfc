<?php

namespace App\Controller;

use App\Entity\BusinessProcesses;
use App\Entity\Config;
use App\Entity\Roles;
use App\Entity\User;
use bheisig\idoitapi\API;
use bheisig\idoitapi\CMDBObjects;
use bheisig\idoitapi\CMDBObjectTypes;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BusinessProcessesController extends AbstractController
{
    /**
     * @Route("/businessprocesses", name="businessprocesses")
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $repository = $entityManager->getRepository(User::class);
        $users = $repository->findBy([], ["lastname" => "ASC"]);

        $repository = $entityManager->getRepository(BusinessProcesses::class);
        $business_processes = $repository->findBy([], ["title" => "ASC"]);
        return $this->render('business_processes/index.html.twig', [
            'selection_list' => $business_processes,
            'users' => $users
        ]);
    }

    /**
     * @Route ("/businessprocesses/save", name="businessprocesses_save")
     */
    public function save(EntityManagerInterface $entityManager, Request $request) {
        $message = '';

        $repository = $entityManager->getRepository(User::class);
        $responsible = $repository->findOneBy(["id" => $request->request->get('responsible')]);

        $repository = $entityManager->getRepository(BusinessProcesses::class);
        $business_process = $repository->findOneBy(["id" => $request->request->get("id")]);

        if (!$business_process) {
            $business_process = new BusinessProcesses();
            $business_process->setLocked(0);
        }

        $business_process->setTitle($request->request->get("title"));
        $business_process->setResponsible($responsible);
        $entityManager->persist($business_process);
        $entityManager->flush();

        if ($responsible = $business_process->getResponsible()) {
            $responsible_string = $business_process->getResponsible()->getFirstname() .' '. $business_process->getResponsible()->getLastname() .' ('. $business_process->getResponsible()->getUsername() .')';
        } else {
            $responsible_string = '';
        }

        $tr = [
            "id" => $business_process->getId(),
            "title" => $business_process->getTitle(),
            "responsible" => $responsible_string,
            "serialized" => $business_process->serialized()
        ];

        $message .= '<span class="">Business process saved successfully</span>';
        return new JsonResponse([
            "message"    => $message,
            "business_process" => $tr
        ]);
    }

    /**
     * @Route ("/businessprocesses/sync", name="process_sync")
     */
    public function sync(EntityManagerInterface $entityManager, LoggerInterface $logger) {
        // Fetch i-doit processes
        $repository = $entityManager->getRepository(Config::class);
        $items      = $repository->findBy(["group_name" => "i-doit"]);

        foreach ($items as $item) {
            $config[$item->getConfigKey()] = unserialize($item->getConfigValue());
        }
        $url_parts = parse_url($config['idoit_url']);

        if (isset($url_parts["port"])) {
            $config['port'] = (int)$url_parts["port"];
        } elseif (isset($url_parts["scheme"]) && $url_parts["scheme"] == "https") {
            $config['port'] = 443;
        } else {
            $config['port'] = 80;
        }


        if ($this->getUser()->getLanguage() == '') {
            $config["api_lang"] = 'en';
        } else {
            $config["api_lang"] = $this->getUser()->getLanguage();
        };

        if ($config['username'] != '' && $config['password'] != '' && $config['idoit_url'] != '') {
            $idoit_config = array(
                API::URL                      => $config['idoit_url'] . '/src/jsonrpc.php',
                API::PORT                     => $config['port'],
                API::KEY                      => $config['api_key'],
                API::USERNAME                 => $config['username'],
                API::PASSWORD                 => $config['password'],
                API::LANGUAGE                 => $config["api_lang"],
                API::BYPASS_SECURE_CONNECTION => true
            );
            $api = new API($idoit_config);
            try {
                $api->login();
            } catch (\Exception $exception) {
                $logger->error($exception->getMessage());
            }
            if ($api->isLoggedIn()) {
                $cmdbObjects = new CMDBObjects($api);
                $idoit_processes = $cmdbObjects->readByType('C__SHD__PROCESS');
                if (is_array($idoit_processes)) {
                    foreach ($idoit_processes as $process) {
                        $repository = $entityManager->getRepository(BusinessProcesses::class);
                        $business_process = $repository->findOneBy(["title" => $process["id"]]);
                        if (!$business_process) {
                            $business_process = new BusinessProcesses();
                            $business_process->setTitle($process["title"]);

                            $entityManager->persist($business_process);
                            $entityManager->flush();
                        }
                    }
                }

            } else {
                $object_types = array();
            }
        }
        // End Fetch i-doit Object Groups

        return $this->redirectToRoute('businessprocesses');

    }
}
