<?php

namespace App\Controller;

use App\Entity\Config;
use App\Entity\EvidenceCollections;
use App\Entity\Roles;
use bheisig\idoitapi\API;
use bheisig\idoitapi\CMDBObjects;
use bheisig\idoitapi\CMDBObjectTypes;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EvidenceCollectionsController extends AbstractController
{
    /**
     * @Route("/evidence/collections", name="evidence_collections")
     */
    public function index(EntityManagerInterface $entityManager, PaginatorInterface $paginator, Request $request): Response
    {
        $filter = array('title' => '');
        foreach ($filter as $parameter => $value) {
            $filter[$parameter] = $request->query->get($parameter);
        }
        $filter['active'] = 1;

        $repository = $entityManager->getRepository(EvidenceCollections::class);
        $queryBuilder = $repository->getWithSearchQueryBuilder($filter);

        $pagination = $paginator->paginate(
            $queryBuilder, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );
        $pagination->setTemplate('@KnpPaginator/Pagination/twitter_bootstrap_v4_pagination.html.twig');
        //dd($pagination->getItems()[0]->getNetworkscans()->getValues());

        return $this->render('evidence_collections/index.html.twig', [
            'filter'               => $filter,
            'evidence_collections' => $pagination,
        ]);
    }

    /**
     * @param EntityManagerInterface $entityManager
     * @Route ("/evidence/collection/new", name="evidence_collection_new" , methods="GET")
     */
    public function new(EntityManagerInterface $entityManager) {
        $evidence_collection = new EvidenceCollections();

        $evidence_collection->setTitle('untitled')
            ->setCreator($this->getUser())
            ->setCreated(new \DateTime())
            ->setUpdated(new \DateTime())
            ->setDescription('')
            ->setObjId(NULL)
            ->setActive(1)
            ;
        $entityManager->persist($evidence_collection);
        $entityManager->flush();
        return $this->redirectToRoute('evidence_collection_show', array('id' => $evidence_collection->getId()));
    }

    /**
     * @param EntityManagerInterface $entityManager
     * @Route ("/evidence/collection/{id<(\d)+>}", name="evidence_collection_show", methods="GET")
     */
    public function show($id, EntityManagerInterface $entityManager) {
        $evidenceCollection = $entityManager->getRepository(EvidenceCollections::class)->find($id);
        /** @var EvidenceCollections|null $evidenceCollection */
        if (!$evidenceCollection) {
            throw $this->createNotFoundException('This evidence collection does not exist');
        }

        $l3networks = $this->fetch_l3networks($entityManager);

        return $this->render('evidence_collections/show.html.twig',[
            'evidence_collection' => $evidenceCollection,
            'l3networks' => $l3networks,
        ]);
    }

    /**
     * @Route ("/evidence/collection/save}", name="evidence_collection_save", methods="POST")
     */
    public function save(EntityManagerInterface $entityManager, Request $request) {
        $repository = $entityManager->getRepository(EvidenceCollections::class);
        $evidenceCollection = $repository->findOneBy(['id' => $request->request->get('id')]);

        if (!$evidenceCollection) {
            throw $this->createNotFoundException('This evidence collection does not exist');
        }
        $evidenceCollection->setActive($request->request->get('active'))
            ->setUpdated(new \DateTime())
            ->setTitle($request->request->get('title'))
            ->setDescription($request->request->get('description'))
            ->setObjId(intval($request->request->get('obj_id')))
        ;

        $entityManager->persist($evidenceCollection);
        $entityManager->flush();

        $l3networks = $this->fetch_l3networks($entityManager);
        return $this->render('evidence_collections/show.html.twig',[
            'evidence_collection' => $evidenceCollection,
            'l3networks' => $l3networks,
        ]);
    }

    private function fetch_l3networks(EntityManagerInterface $entityManager) {
        // Fetch i-doit Object Groups and affected_components

        $repository = $entityManager->getRepository(Config::class);
        $items      = $repository->findBy(["group_name" => "i-doit"]);

        foreach ($items as $item) {
            $config[$item->getConfigKey()] = unserialize($item->getConfigValue());
        }
        $url_parts = parse_url($config['idoit_url']);

        if (isset($url_parts["port"])) {
            $config['port'] = (int)$url_parts["port"];
        } elseif (isset($url_parts["scheme"]) && $url_parts["scheme"] == "https") {
            $config['port'] = 443;
        } else {
            $config['port'] = 80;
        }


        if ($this->getUser()->getLanguage() == '') {
            $config["api_lang"] = 'en';
        } else {
            $config["api_lang"] = $this->getUser()->getLanguage();
        };

        if ($config['username'] != '' && $config['password'] != '' && $config['idoit_url'] != '') {
            $idoit_config = array(
                API::URL                      => $config['idoit_url'] . '/src/jsonrpc.php',
                API::PORT                     => $config['port'],
                API::KEY                      => $config['api_key'],
                API::USERNAME                 => $config['username'],
                API::PASSWORD                 => $config['password'],
                API::LANGUAGE                 => $config["api_lang"],
                API::BYPASS_SECURE_CONNECTION => true
            );
            $api = new API($idoit_config);
            try {
                $api->login();
            } catch (\Exception $exception) {
                //
            }
            if ($api->isLoggedIn()) {
                $cmdbObjects = new CMDBObjects($api);
                $objects = $cmdbObjects->readByType('C__OBJTYPE__LAYER3_NET');

            } else {
                $objects = array();
            }
        } else {
            $objects = array();
        }

        // End Fetch i-doit Object Groups
        return $objects;
    }

}
