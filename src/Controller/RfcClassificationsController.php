<?php

namespace App\Controller;

use App\Entity\Authorizations;
use App\Entity\Categories;
use App\Entity\Costcenters;
use App\Entity\Ressourcetypes;
use App\Entity\Rfc;
use App\Entity\RfcBpoFeedback;
use App\Entity\RfcClassifications;
use App\Entity\RfcNotices;
use App\Entity\RfcStandardchanges;
use App\Entity\Status;
use App\Entity\Units;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Bundle\MarkdownBundle\MarkdownParserInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Extra\Markdown\DefaultMarkdown;

class RfcClassificationsController extends AbstractController
{
    private DefaultMarkdown $markdownConverter;

    public function __construct()
    {
        $this->markdownConverter = new DefaultMarkdown();
    }
    /**
     * @Route("/rfc/classifications", name="rfc_classifications")
     */
    public function index(): Response
    {
        return $this->render('rfc_classifications/index.html.twig', [
            'controller_name' => 'RfcClassificationsController',
        ]);
    }

    /**
     * @Route ("/rfc/classifications/save", name="rfc_classifications_save", methods="POST")
     */
    public function save(EntityManagerInterface $entityManager, Request $request, MailerInterface $mailer, TranslatorInterface $translator) {
        $repository = $entityManager->getRepository(Rfc::class);
        $rfc = $repository->findOneBy(['id' => $request->request->get('rfc_id')]);
        $repository = $entityManager->getRepository(Ressourcetypes::class);
        $rtype = $repository->findOneBy(['const' => $request->request->get('rtype')]);
        if (!$rtype) {
            $rtype = $repository->findOneBy(['const' => 'C__RTYPE__MERGE']);
        }
        $repository = $entityManager->getRepository(Units::class);
        $runit = $repository->findOneBy(['const' => $request->request->get('runit')]);
        if (!$runit) {
            $runit = $repository->findOneBy(['const' => 'C__UNIT__HOUR']);
        }
        $repository = $entityManager->getRepository(Categories::class);
        $category = $repository->findOneBy(['const' => $request->request->get('category')]);
        if(!$category) {
            $category = $repository->findOneBy(['const' => 'C__CATEGORY__ONE']);
        }
        $repository = $entityManager->getRepository(RfcStandardchanges::class);
        $standardchange = $repository->findOneBy(['id' => $request->request->get('standardchange')]);
        $rtime = (int)$request->request->get('rtime');
        $riskanalysis = $request->request->get('riskanalysis');

        $zammad = new ZammadController();
        $zammad_config = $zammad->get_config($entityManager);

        $rfc_notices = new RfcNotices();

        $requested_status = $request->request->get('status');
        $requested_authorizationtype = $request->request->get('authorizationtype');
        if ($requested_status == 'C__STATUS__CAB' &&
            ($request->request->get('category') == 'C__CATEGORY__DEFAULT' || $request->request->get('category') == 'C__CATEGORY__EMERGENCY')) {
            $requested_status = 'C__STATUS__CABOK';
            $requested_authorizationtype = 'C__AUTHTYPE__CM';
        }
        if (($requested_status == 'C__STATUS__DRAFT' || $requested_status == 'C__STATUS__BPO')  &&
            ($request->request->get('category') == 'C__CATEGORY__DEFAULT' || $request->request->get('category') == 'C__CATEGORY__EMERGENCY')) {
            $requested_authorizationtype = 'C__AUTHTYPE__CM';
        }

        if ($zammad_config["active"] == 1 && $requested_status == 'C__STATUS__CAB') {
            $zammad->remove_tag($rfc->getReferencedTicketid(), 'RfC_Status_Classification', $entityManager);
            $zammad->remove_tag($rfc->getReferencedTicketid(), 'RfC_Status_Dispatching', $entityManager);
            $zammad->add_tag($rfc->getReferencedTicketid(), 'RfC_Status_Authorization', $entityManager);
        }

        if ($zammad_config["active"] == 1 && $requested_status == 'C__STATUS__CABOK') {
            $zammad->remove_tag($rfc->getReferencedTicketid(), 'RfC_Status_Classification', $entityManager);
            $zammad->remove_tag($rfc->getReferencedTicketid(), 'RfC_Status_Dispatching', $entityManager);
            $zammad->add_tag($rfc->getReferencedTicketid(), 'RfC_Status_Implementation', $entityManager);
        }

        $repository = $entityManager->getRepository(Authorizations::class);
        $authorizationtype = $repository->findOneBy(['const' => $requested_authorizationtype]);
        if (!$authorizationtype) {
            $authorizationtype = $repository->findOneBy(['const' => 'C__AUTHTYPE__CM']);
        }
        $repository = $entityManager->getRepository(Status::class);
        $status = $repository->findOneBy(['const' => $requested_status]);


        if (!$rfc) {
            throw new \Exception('RfC does not exits');
        } else {
            // rfc exists -> assigning new status
            $old_status_const = $rfc->getStatus()->getConst();
            $old_status_title = $rfc->getStatus()->getTitle();
            $new_status_const = $status->getConst();
            $new_status_title = $status->getTitle();

            //set RfC Status
            $rfc->setStatus($status);

            $repository = $entityManager->getRepository(RfcClassifications::class);
            $rfc_classification = $repository->findOneBy(['rfc' => $rfc]);

            // If classification does not exist yet, initialise one
            if (!$rfc_classification) {
                $rfc_classification = new RfcClassifications();
                $rfc_classification
                    ->setClassificator($this->getUser())
                    ->setRfc($rfc)
                ;
            }


            $rfc_classification->setRiskanalysis($riskanalysis)
                ->setRtime($rtime)
                ->setRtype($rtype)
                ->setRunit($runit)
                ->setCosts((int)$request->request->get('costs'))
                ->setHardwareSoftware($request->request->get('hardware_software'))
                ->setAuthorizationtype($authorizationtype)
                ->setStandardchange($standardchange)
                ->setCabconferenceDate(new \DateTime($request->request->get('cabconference_date')))
                ->setCategory($category);

            $entityManager->persist($rfc_classification);
            $entityManager->persist($rfc);

            $posted_message = $request->request->get('message');
            $tranlated_new_status = $translator->trans($new_status_title);
            $rfc_notice_content = <<<EOD
$posted_message

```
Previous state: $old_status_title
New state:      $tranlated_new_status
```
EOD;


            $rfc_notice = new RfcNotices();
            $rfc_notice->setRfc($rfc)
                ->setTitle('Logbook entry')
                ->setContent($rfc_notice_content)
                ->setStatus($status)
                ->setCreated(new \DateTime())
                ->setAuthor($this->getUser())
            ;
            $entityManager->persist($rfc_notice);


            if ($old_status_const != $new_status_const) {
                $deeplink = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . '/rfc/' . $rfc->getId();
                if ($old_status_const == 'C__STATUS__TRANSFERED' && $new_status_const == 'C__STATUS__DRAFT') {
                    /*
                     * Send E-Mail to Creator
                     */
                    $creator = $rfc->getCreator();
                    $firstname = $creator->getFirstname();

                    $posted_message = $request->request->get('message');

                    $message = <<<EOD
$posted_message

Please open the [RfC Manager]($deeplink) and add all necessary information.
EOD;
                    $message = $this->markdownConverter->convert($message);
                    $email = new EmailController();
                    $email->send_email($entityManager, $creator, $message, '[RfC# ' . $rfc->getId() . '] A Request for Change needs your attention', $mailer);


                    if ($zammad_config["active"] == 1) {
                        $zammad->remove_tag($rfc->getReferencedTicketid(), 'RfC_Status_Classification', $entityManager);
                    }

                } elseif ($old_status_const == 'C__STATUS__TRANSFERED' && $new_status_const == 'C__STATUS__BPO') {
                    /*
                     * Send E-Mail to BPO
                     */

                    $repository = $entityManager->getRepository(RfcBpoFeedback::class);
                    $rfc_bpo_feedback = $repository->findOneBy(['rfc' => $rfc]);

                    if (!$rfc_bpo_feedback) {
                        $rfc_bpo_feedback = new RfcBpoFeedback();
                        $rfc_bpo_feedback->setRfc($rfc);
                    }

                    if ($request->request->get('recipient') == 'CONST_BPO') {
                        $recipient = $rfc->getBusinessProcess()->getResponsible();

                    } elseif ($request->request->get('recipient') == 'CONST_RESPONSIBLE') {
                        $recipient = $rfc->getCostcenter()->getResponsible();

                    } elseif ($request->request->get('recipient') == 'CONST_CISO') {
                        $system = new SystemController();
                        $system_config = $system->get_config($entityManager);
                        $repository = $entityManager->getRepository(User::class);
                        $recipient = $repository->findOneBy(['id' => $system_config['ciso']]);

                    } else {
                        $repository = $entityManager->getRepository(User::class);
                        $recipient = $repository->findOneBy(['id'=>$request->request->get('recipient')]);
                    }

                    $rfc_bpo_feedback->setDelegatee($recipient);
                    $entityManager->persist($rfc_bpo_feedback);



                    $message = $request->request->get('message');

                    $message .= <<<EOD


Please open the [RfC Manager]($deeplink) and provide your feedback.
EOD;


                    $message = $this->markdownConverter->convert($message);
                    $email = new EmailController();
                    $email->send_email($entityManager, $recipient, $message, '[RfC# ' . $rfc->getId() . '] Your feedback is required for a Request for Change', $mailer);


                    if ($zammad_config["active"] == 1) {
                        $zammad->remove_tag($rfc->getReferencedTicketid(), 'RfC_Status_Classification', $entityManager);
                        $zammad->add_tag($rfc->getReferencedTicketid(), 'RfC_Status_BPO_Feedback', $entityManager);
                    }

                    $rfc_notices
                        ->setTitle('Request for feedback')
                        ->setAuthor($this->getUser())
                        ->setRfc($rfc)
                        ->setContent($message)
                        ->setStatus($status)
                        ->setCreated(new \DateTime())
                    ;

                }
            }
            $entityManager->flush();
        }
        return $this->redirectToRoute('rfc_details', array('id' => $rfc->getId()));
    }

    /**
     * @Route ("/rfc/classifications/dispatch", name="rfc_classifications_dispatch", methods="POST")
     */
    public function dispatch(EntityManagerInterface $entityManager, Request $request, MailerInterface $mailer) {
        $repository = $entityManager->getRepository(Rfc::class);
        $rfc = $repository->findOneBy(['id' => $request->request->get('rfc_id')]);
        $repository = $entityManager->getRepository(Status::class);
        $status = $repository->findOneBy(['const' => $request->request->get('status')]);
        $repository = $entityManager->getRepository(User::class);
        $classificator = $repository->findOneBy(['id' => $request->request->get('classificator')]);
        $repository = $entityManager->getRepository(Costcenters::class);
        $costcenter = $repository->findOneBy(['id' => $request->request->get('costcenter')]);

        if (!$rfc) {
            throw new \Exception('RfC does not exits');
        } else {
            //set RfC Status

            $rfc->setCostcenter($costcenter);

            $repository = $entityManager->getRepository(RfcClassifications::class);
            $rfc_classification = $repository->findOneBy(['rfc' => $rfc]);

            if ($status->getConst() == 'C__STATUS__TRANSFERED') {

                // Special treatment of Status, if old Status of RfC is not C__STATUS__INCOME. In this case we won’t change the status
                if ($rfc->getStatus()->getConst() != 'C__STATUS__INCOME') {
                    $status = $rfc->getStatus();
                }

                if ($classificator) {
                    $rfc->setStatus($status);
                    if (!$rfc_classification) {
                        $rfc_classification = new RfcClassifications();
                        $repository = $entityManager->getRepository(Ressourcetypes::class);
                        $rtype = $repository->findOneBy(['const' => 'C__RTYPE__MERGE']);
                        $repository = $entityManager->getRepository(Units::class);
                        $runit = $repository->findOneBy(['const' => 'C__UNIT__HOUR']);
                        $repository = $entityManager->getRepository(Authorizations::class);
                        $authorizationtype = $repository->findOneBy(['const' => 'C__AUTHTYPE__CM']);
                        $repository = $entityManager->getRepository(Categories::class);
                        $category = $repository->findOneBy(['const' => 'C__CATEGORY__ONE']);

                        $rfc_classification->setRfc($rfc)
                            ->setRiskanalysis('')
                            ->setRtype($rtype)
                            ->setRtime(0)
                            ->setRunit($runit)
                            ->setAuthorizationtype($authorizationtype)
                            ->setCategory($category)
                            ->setClassificator($classificator);
                    } else {
                        $rfc_classification->setClassificator($classificator);
                    }
                    $entityManager->persist($rfc_classification);
                    $rf_notice = new RfcNotices();
                    $rf_notice->setRfc($rfc)
                        ->setTitle($request->request->get('title'))
                        ->setContent($request->request->get('message'))
                        ->setStatus($status)
                        ->setCreated(new \DateTime())
                        ->setAuthor($this->getUser());
                    $deeplink = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . '/rfc/' . $rfc->getId();
                    $posted_message = $request->request->get('message');
                    $message = <<<EOD
$posted_message

Please open the [RfC Manager]($deeplink) and classify this Request for Change.
EOD;
                    $message = $this->markdownConverter->convert($message);
                    $email = new EmailController();
                    $email->send_email($entityManager, $classificator, $message, '[RfC# ' . $rfc->getId() . '] A RfC has been assigned to you', $mailer);
                    $zammad = new ZammadController();
                    $zammad_config = $zammad->get_config($entityManager);
                    if ($zammad_config["active"] == 1) {
                        $zammad->remove_tag($rfc->getReferencedTicketid(), 'RfC_Status_Dispatching', $entityManager);
                        $zammad->remove_tag($rfc->getReferencedTicketid(), 'RfC_Status_BPO_Feedback', $entityManager); // if we com from BPO State
                        $zammad->add_tag($rfc->getReferencedTicketid(), 'RfC_Status_Classification', $entityManager);
                        $zammad->update_ticket($rfc->getReferencedTicketid(), $zammad_config['state_id_open'], $rfc->getCreator(), $rfc_classification->getClassificator(), $entityManager);
                        $zammad->create_article($rfc->getReferencedTicketid(), $request->request->get('title'), $request->request->get('message'), $entityManager);
                    }
                } else {
                    return $this->redirectToRoute('rfc_details', array('id' => $rfc->getId()));
                }
                // endif C__STATUS__TRANSFERED
            } elseif ($status->getConst() == 'C__STATUS__CABKO' or $status->getConst() == 'C__STATUS__DRAFT') { // if Status is  C__STATUS__CABKO (rejected)
                $rfc->setStatus($status);

                $rf_notice = new RfcNotices();
                $rf_notice->setRfc($rfc)
                    ->setTitle($request->request->get('title'))
                    ->setContent($request->request->get('message'))
                    ->setStatus($status)
                    ->setCreated(new \DateTime())
                    ->setAuthor($this->getUser());

                $deeplink = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . '/rfc/' . $rfc->getId();
                $posted_message = $request->request->get('message');
                $message = <<<EOD
$posted_message

Open in [RfC Manager]($deeplink) .
EOD;
                $message = $this->markdownConverter->convert($message);
                $email = new EmailController();
                $email->send_email($entityManager, $rfc->getCreator(), $message, '[RfC# ' . $rfc->getId() . '] has been rejected or needs further information', $mailer);
                $zammad = new ZammadController();
                $zammad_config = $zammad->get_config($entityManager);
                if ($zammad_config["active"] == 1) {
                    $zammad->remove_tag($rfc->getReferencedTicketid(), 'RfC_Status_Dispatching', $entityManager);
                    $zammad->remove_tag($rfc->getReferencedTicketid(), 'RfC_Status_BPO_Feedback', $entityManager); // if we come from BPO State
                    $zammad->add_tag($rfc->getReferencedTicketid(), 'RfC_Status_Rejected', $entityManager);
                    $zammad->update_ticket($rfc->getReferencedTicketid(), $zammad_config['state_id_open'], $rfc->getCreator(), $this->getUser(), $entityManager);
                    $zammad->create_article($rfc->getReferencedTicketid(), $request->request->get('title'), $request->request->get('message'), $entityManager);
                }
            } elseif ($status->getConst() == 'C__STATUS__INCOME') { // Keep Status, just change the Costcenter (Section)
                $costcenter = $rfc->getCostcenter();
                $responsible = $costcenter->getResponsible();

                $rf_notice = new RfcNotices();
                $rf_notice->setRfc($rfc)
                    ->setTitle($request->request->get('title'))
                    ->setContent($request->request->get('message'))
                    ->setStatus($status)
                    ->setCreated(new \DateTime())
                    ->setAuthor($this->getUser());

                $deeplink = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . '/rfc/' . $rfc->getId();
                $posted_message = $request->request->get('message');
                $message = <<<EOD
$posted_message

Open in [RfC Manager]($deeplink) .
EOD;
                $message = $this->markdownConverter->convert($message);
                $email = new EmailController();
                $email->send_email($entityManager, $responsible, $message, '[RfC# ' . $rfc->getId() . '] A Request for Change in your section has been submitted', $mailer);

            } else { // end if C__STATUS__INCOME
                throw new \Exception('This Status to Status change is not allowed');
            }

            $entityManager->persist($rf_notice);
            $entityManager->persist($rfc);
            $entityManager->flush();


        }

        return $this->redirectToRoute('rfc_details', array('id' => $rfc->getId()));
    }


}
