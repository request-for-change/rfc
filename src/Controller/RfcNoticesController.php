<?php

namespace App\Controller;

use App\Entity\Notificationtemplates;
use App\Entity\Notificationtypes;
use App\Entity\Rfc;
use App\Entity\RfcNotices;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RfcNoticesController extends AbstractController
{
    /**
     * @Route("/rfc/notices", name="rfc_notices")
     */
    public function index(): Response
    {
        return $this->render('rfc_notices/index.html.twig', [
            'controller_name' => 'RfcNoticesController',
        ]);
    }

    /**
     * @Route ("/rfc/notices/save", name="rfc_notices_save")
     */
    public function save(EntityManagerInterface $entityManager, Request $request) {

        $repository = $entityManager->getRepository(User::class);
        $author = $repository->findOneBy(['id' => $request->request->get('author_id')]);

        $repository= $entityManager->getRepository(Rfc::class);
        $rfc = $repository->findOneBy(['id' => $request->request->get('rfc_id')]);

        $rfc_notice = new RfcNotices();
        $rfc_notice->setAuthor($author)
            ->setCreated(new \DateTime())
            ->setStatus($rfc->getStatus())
            ->setRfc($rfc)
            ->setTitle($request->request->get('title'))
            ->setContent($request->request->get('content'));

        $entityManager->persist($rfc_notice);
        $entityManager->flush();

        return $this->redirectToRoute('rfc_details', array('id' => $rfc->getId()));
    }
}
