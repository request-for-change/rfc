<?php

namespace App\Controller;

use App\Entity\Rfc;
use App\Entity\RfcBpoFeedback;
use App\Entity\RfcNotices;
use App\Entity\User;
use App\Repository\RfcBpoFeedbackRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Bundle\MarkdownBundle\MarkdownParserInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;

class RfcBpoFeedbackController extends AbstractController
{
    /**
     * @Route("/rfc/bpo/feedback", name="rfc_bpo_feedback")
     */
    public function index(): Response
    {
        return $this->render('rfc_bpo_feedback/index.html.twig', [
            'controller_name' => 'RfcBpoFeedbackController',
        ]);
    }

    /**
     * @Route ("/rfc/bpo/delegation", name="rfc_bpo_delegation", methods="POST")
     */
    public function delegation(EntityManagerInterface $entityManager, Request $request, MailerInterface $mailer, MarkdownParserInterface $markdownParser) {
        $repository = $entityManager->getRepository(Rfc::class);
        $rfc = $repository->findOneBy(['id' => $request->request->get('rfc_id')]);
        $repository = $entityManager->getRepository(User::class);
        $delegatee = $repository->findOneBy(['id' => $request->request->get('delegatee')]);
        $repository = $entityManager->getRepository(RfcBpoFeedback::class);
        $rfc_bpo_feedback = $repository->findOneBy(['rfc' => $rfc]);


        if (!$rfc_bpo_feedback) {
            $rfc_bpo_feedback = new RfcBpoFeedback();
            $rfc_bpo_feedback->setRfc($rfc);
        }

        $rfc_bpo_feedback->setDelegatee($delegatee);
        $entityManager->persist($rfc_bpo_feedback);

        $rfc_notice = new RfcNotices();
        $rfc_notice->setRfc($rfc);
        $rfc_notice->setTitle('BPO feedback delegation');
        $rfc_notice->setStatus($rfc->getStatus());
        $rfc_notice->setAuthor($this->getUser());
        $rfc_notice->setCreated(new \DateTime());
        $rfc_notice->setContent($request->request->get('message'));
        $entityManager->persist($rfc_notice);

        $entityManager->flush();

        $deeplink = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . '/rfc/' . $rfc->getId();

        $message = $request->request->get('message');
        $message = <<<EOD
$message

Please open the [RfC Manager]($deeplink) and classify this Request for Change.
EOD;

        $message = $markdownParser->transformMarkdown($message);
        $email = new EmailController();
        $email->send_email($entityManager, $delegatee, $message, '[RfC# '. $rfc->getId() .'] A RfC has been assigned to you', $mailer);



        return $this->redirectToRoute('rfc_details', array('id' => $rfc->getId()));


    }
}
