<?php

namespace App\Controller;

use App\Entity\Units;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UnitsController extends AbstractController
{
    /**
     * @Route("/units", name="units")
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $repository = $entityManager->getRepository(Units::class);
        return $this->render('units/index.html.twig', [
            'selection_list' => $repository->findAll(),
        ]);
    }

    /**
     * @Route ("/units/save", name="units_save")
     */
    public function save(EntityManagerInterface $entityManager, Request $request) {
        $message = '';
        $repository = $entityManager->getRepository(Units::class);

        $units = $repository->findOneBy(["id" => $request->request->get("id")]);
        if (!$units) {
            $units = new Units();
            $units->setLocked(0);
        }

        $units->setTitle($request->request->get("title"));
        if (!$units->getConst()) {
            $units->setConst('C__UNIT__' . strtoupper(str_replace(' ', '_', $request->request->get("title"))));
        }
        $entityManager->persist($units);
        $entityManager->flush();

        $items = $repository->findAll();

        $trs = array();
        foreach ($items as $item) {
            $trs[] = [
                "id" => $item->getId(),
                "title" => $item->getTitle()
            ];

        }

        $message .= '<span class="">Unit saved successfully</span>';

        return new JsonResponse([
            "message" => $message,
            "trs"     => $trs
        ]);
    }
}
