<?php

namespace App\Controller;

use App\Entity\Config;
use App\Entity\Departments;
use App\Entity\User;
use bheisig\idoitapi\API;
use bheisig\idoitapi\CMDBCategory;
use bheisig\idoitapi\CMDBObject;
use bheisig\idoitapi\CMDBObjects;
use bheisig\idoitapi\Idoit;
use Doctrine\Migrations\Configuration\EntityManager\ExistingEntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IdoitController extends AbstractController
{
    private $config = array(
        "idoit_url"              => "",
        "api_key"                => "",
        "roles"                  => "",
        "overwrite_proxy"        => "",
        "affectedComponentOType" => "",
        "username"               => "",
        "password"               => "",
        "port"                   => "",
        "api_lang"               => "en",
        "bypass_secure_connection" => "",
    );

    /**
     * @Route("/idoit", name="idoit")
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $repository = $entityManager->getRepository(Config::class);
        $items      = $repository->findBy(["group_name" => "i-doit"]);

        foreach ($items as $item) {
            $this->config[$item->getConfigKey()] = unserialize($item->getConfigValue());
        }

        $url_parts = parse_url($this->config['idoit_url']);

        if (isset($url_parts["port"])) {
            $this->config['port'] = (int)$url_parts["port"];
        } elseif (isset($url_parts["scheme"]) && $url_parts["scheme"] == "https") {
            $this->config['port'] = 443;
        } else {
            $this->config['port'] = 80;
        }

        if ($this->getUser()->getLanguage() == '') {
            $this->config["api_lang"] = 'en';
        } else {
            $this->config["api_lang"] = $this->getUser()->getLanguage();
        }


        return $this->render('idoit/show.html.twig', [
            'config' => $this->config
        ]);
    }

    /**
     * @Route ("/idoit/save", name="idoit_save", methods="POST")
     */
    public function save(Request $request, EntityManagerInterface $entityManager, LoggerInterface $logger) {

        $repository = $entityManager->getRepository(Config::class);
        $items      = $repository->findBy(["group_name" => "i-doit"]);

        foreach ($this->config as $config_key => $value) {
            $config = $repository->findOneBy([
                "group_name" => "i-doit",
                "config_key" => $config_key
            ]);

            if (!$config) {
                $config = new Config();
                $config->setGroupName('i-doit')
                       ->setConfigKey($config_key)
                       ->setConfigValue(serialize($request->request->get($config_key)))
                       ->setLabel('')
                       ->setFieldType('text')
                       ->setDate(new \DateTime());
            } else {
                $config->setConfigValue(serialize($request->request->get($config_key)));
            }

            $this->config[$config_key] = $request->request->get($config_key);
            $entityManager->persist($config);
        }

        $entityManager->flush();

        if ($request->request->get('variant') == 'sync_contacts') {

            $this->sync_contacts($entityManager, $logger);
        }

        return $this->redirectToRoute('idoit');
    }

    public function login($credentials, EntityManagerInterface $entityManager, LoggerInterface $logger) {
        $repository = $entityManager->getRepository(Config::class);
        $items      = $repository->findBy(["group_name" => "i-doit"]);

        $logger->info('Logging in to i-doit API');

        foreach ($items as $item) {
            $config[$item->getConfigKey()] = unserialize($item->getConfigValue());
        }

        $url_parts = parse_url($config['idoit_url']);

        if (isset($url_parts["port"])) {
            $config['port'] = (int)$url_parts["port"];
        } elseif (isset($url_parts["scheme"]) && $url_parts["scheme"] == "https") {
            $config['port'] = 443;
        } else {
            $config['port'] = 80;
        }

        if ($config['idoit_url'] != '') {
            $idoit_config = array(
                API::URL => $config['idoit_url'] . '/src/jsonrpc.php',
                API::PORT => $config['port'],
                API::KEY => $config['api_key'],
                API::USERNAME => $credentials["username"],
                API::PASSWORD => $credentials["password"],
                API::LANGUAGE => 'en',
                API::BYPASS_SECURE_CONNECTION => true
            );

            $api = new API($idoit_config);
            try {
                $api->login();
            } catch (\Exception $exception) {
                die('could not login: ' . $exception->getMessage());
            }


            return $api->isLoggedIn();
        } else {
            return false;
        }

    }

    /**
     * @Route ("/idoit/get_objects", name="idoit_get_objects")
     */
    public function get_objects(EntityManagerInterface $entityManager, Request $request, LoggerInterface $logger) {
        $objects = array();
        $api = $this->get_api($entityManager, $logger);

        if ($api) {
            $cmdbObjects = new CMDBObjects($api);
            $objects = $cmdbObjects->read(['type' => $request->request->get('object_type')], 2000, 0, 'title', CMDBObjects::SORT_DESCENDING);
        }



        return new JsonResponse($objects);
    }

    /**
     * @Route("/idoit/get_object", name="idoit_get_object")
     * @return JsonResponse|void
     */
    public function get_object(EntityManagerInterface $entityManager, Request $request, LoggerInterface $logger) {
        $api = $this->get_api($entityManager, $logger);
        $object = [];
        if ($api) {
            $cmdObject = new CMDBObject($api);
            try {
                $object = $cmdObject->read($request->request->get('objid'));
            } catch (\Exception $e) {
                $object['error'] = $e;
            }
            return new JsonResponse($object);
        }
    }

    private function sync_contacts(EntityManagerInterface $entityManager, LoggerInterface $logger) {
        $objects = array();
        $persons = array();
        $api = $this->get_api($entityManager, $logger);

        if ($api) {
            $cmdbObjects = new CMDBObjects($api);
            $objects = $cmdbObjects->read(['type' => 'C__OBJTYPE__PERSON'], 10000, 0, 'title', CMDBObjects::SORT_DESCENDING);

            $cmdbCategory = new CMDBCategory($api);
            foreach ($objects as $object) {
                $person = array();
                $loginCategory = $cmdbCategory->read($object["id"],'C__CATS__PERSON_LOGIN');
                $person['login'] = $loginCategory[0]['title'];
                $personCategory = $cmdbCategory->read($object["id"], 'C__CATS__PERSON_MASTER');
                $person['first_name'] = $personCategory[0]['first_name'];
                $person['last_name'] = $personCategory[0]['last_name'];
                $person['mail'] = $personCategory[0]['mail'];


                if ($person['login']) {
                    try {
                        $repository = $entityManager->getRepository(User::class);
                        $user = $repository->findOneBy([
                            'username' => $person['login']
                        ]);
                        if (!$user) {
                            $user = new User();
                            $repository = $entityManager->getRepository(Departments::class);
                            $department = $repository->findBy([], ['title' => 'ASC'], 1);
                            $user->setDepartment($department[0]);
                        }
                        $user->setEmail($person["mail"])
                            ->setFirstname($person["first_name"])
                            ->setLastname($person["last_name"])
                            ->setUsername($person["login"])
                            ->setPassword('')
                            ->setLogins(0)
                            ->setLastLogin(0);
                        $entityManager->persist($user);
                    } catch (\Exception $e) {
                        // ToDo: something sensible if User can’t be added or updated
                        $logger->error('Error while syncing contacts: ' . $e);
                    }
                }
            }
            $entityManager->flush();
        } else {
            $logger->error('Error, could not create api');
        }


    }

    public function get_api(EntityManagerInterface $entityManager, LoggerInterface $logger) {
        $repository = $entityManager->getRepository(Config::class);
        $items      = $repository->findBy(["group_name" => "i-doit"]);

        foreach ($items as $item) {
            $this->config[$item->getConfigKey()] = unserialize($item->getConfigValue());
        }

        $url_parts = parse_url($this->config['idoit_url']);

        if (isset($url_parts["port"])) {
            $this->config['port'] = (int)$url_parts["port"];
        } elseif (isset($url_parts["scheme"]) && $url_parts["scheme"] == "https") {
            $this->config['port'] = 443;
        } else {
            $this->config['port'] = 80;
        }

        if ($this->getUser()->getLanguage() == '') {
            $this->config["api_lang"] = 'en';
        } else {
            $this->config["api_lang"] = $this->getUser()->getLanguage();
        }

        if ($this->config['username'] != '' && $this->config['password'] != '') {
            $idoit_config = array(
                API::URL                      => $this->config['idoit_url'] . '/src/jsonrpc.php',
                API::PORT                     => $this->config['port'],
                API::KEY                      => $this->config['api_key'],
                API::USERNAME                 => $this->config['username'],
                API::PASSWORD                 => $this->config['password'],
                API::LANGUAGE                 => $this->config["api_lang"],
                API::BYPASS_SECURE_CONNECTION => true
            );
            $api = new API($idoit_config);
            try {
                $api->login();
            } catch (\Exception $exception) {
                $logger->error('Could not create i-doit API Object: ' . $exception);
            }
            if ($api->isLoggedIn()) {
                return $api;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
