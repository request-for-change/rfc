<?php

namespace App\Entity;

use App\Repository\RfcClassificationsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RfcClassificationsRepository::class)
 */
class RfcClassifications
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $riskanalysis;

    /**
     * @ORM\ManyToOne(targetEntity=Ressourcetypes::class, inversedBy="rfcClassifications")
     * @ORM\JoinColumn(nullable=false)
     */
    private $rtype;

    /**
     * @ORM\Column(type="integer")
     */
    private $rtime;

    /**
     * @ORM\ManyToOne(targetEntity=Units::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $runit;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $costs;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $hardware_software;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $classificator;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $cabconference_date;

    /**
     * @ORM\ManyToOne(targetEntity=Categories::class, inversedBy="rfcClassifications")
     * @ORM\JoinColumn(nullable=false)
     */
    private $category;

    /**
     * @ORM\OneToOne(targetEntity=Rfc::class, inversedBy="rfc_classification")
     * @ORM\JoinColumn(nullable=false)
     */
    private $rfc;

    /**
     * @ORM\ManyToOne(targetEntity=Authorizations::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $authorizationtype;

    /**
     * @ORM\ManyToOne(targetEntity=RfcStandardchanges::class)
     */
    private $standardchange;

    public function __construct()
    {

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRiskanalysis(): ?string
    {
        return $this->riskanalysis;
    }

    public function setRiskanalysis(string $riskanalysis): self
    {
        $this->riskanalysis = $riskanalysis;

        return $this;
    }

    public function getRtype(): ?Ressourcetypes
    {
        return $this->rtype;
    }

    public function setRtype(?Ressourcetypes $rtype): self
    {
        $this->rtype = $rtype;

        return $this;
    }

    public function getRtime(): ?int
    {
        return $this->rtime;
    }

    public function setRtime(int $rtime): self
    {
        $this->rtime = $rtime;

        return $this;
    }

    public function getRunit(): ?Units
    {
        return $this->runit;
    }

    public function setRunit(?Units $runit): self
    {
        $this->runit = $runit;

        return $this;
    }

    public function getCosts(): ?int
    {
        return $this->costs;
    }

    public function setCosts(?int $costs): self
    {
        $this->costs = $costs;

        return $this;
    }

    public function getHardwareSoftware(): ?string
    {
        return $this->hardware_software;
    }

    public function setHardwareSoftware(?string $hardware_software): self
    {
        $this->hardware_software = $hardware_software;

        return $this;
    }

    public function getClassificator(): ?User
    {
        return $this->classificator;
    }

    public function setClassificator(?User $classificator): self
    {
        $this->classificator = $classificator;

        return $this;
    }

    public function getCabconferenceDate(): ?\DateTimeInterface
    {
        return $this->cabconference_date;
    }

    public function setCabconferenceDate(?\DateTimeInterface $cabconference_date): self
    {
        $this->cabconference_date = $cabconference_date;

        return $this;
    }

    public function getCategory(): ?Categories
    {
        return $this->category;
    }

    public function setCategory(?Categories $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getRfc(): ?Rfc
    {
        return $this->rfc;
    }

    public function setRfc(?Rfc $rfc): self
    {
        $this->rfc = $rfc;

        return $this;
    }

    public function getAuthorizationtype(): ?Authorizations
    {
        return $this->authorizationtype;
    }

    public function setAuthorizationtype(?Authorizations $authorizationtype): self
    {
        $this->authorizationtype = $authorizationtype;

        return $this;
    }

    public function getStandardchange(): ?RfcStandardchanges
    {
        return $this->standardchange;
    }

    public function setStandardchange(?RfcStandardchanges $standardchange): self
    {
        $this->standardchange = $standardchange;

        return $this;
    }
}
