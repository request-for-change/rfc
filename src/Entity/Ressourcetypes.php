<?php

namespace App\Entity;

use App\Repository\RessourcetypesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Encoder\JsonEncode;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @ORM\Entity(repositoryClass=RessourcetypesRepository::class)
 * @ORM\Table (name="ressource_types")
 */
class Ressourcetypes
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $const;

    /**
     * @ORM\Column(type="smallint")
     */
    private $locked;

    /**
     * @ORM\OneToMany(targetEntity=RfcClassifications::class, mappedBy="rtype")
     */
    private $rfcClassifications;

    public function __construct()
    {
        $this->rfcClassifications = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getConst(): ?string
    {
        return $this->const;
    }

    public function setConst(?string $const): self
    {
        $this->const = $const;

        return $this;
    }

    public function getLocked(): ?int
    {
        return $this->locked;
    }

    public function setLocked(int $locked): self
    {
        $this->locked = $locked;

        return $this;
    }

    /**
     * @return Collection|RfcClassifications[]
     */
    public function getRfcClassifications(): Collection
    {
        return $this->rfcClassifications;
    }

    public function addRfcClassification(RfcClassifications $rfcClassification): self
    {
        if (!$this->rfcClassifications->contains($rfcClassification)) {
            $this->rfcClassifications[] = $rfcClassification;
            $rfcClassification->setRtype($this);
        }

        return $this;
    }

    public function removeRfcClassification(RfcClassifications $rfcClassification): self
    {
        if ($this->rfcClassifications->removeElement($rfcClassification)) {
            // set the owning side to null (unless already changed)
            if ($rfcClassification->getRtype() === $this) {
                $rfcClassification->setRtype(null);
            }
        }

        return $this;
    }

    public function serialized()
    {
        $encoder = new JsonEncode();
        $normalizer = new ObjectNormalizer();
        $serializer = new Serializer(array($normalizer),array($encoder));

        return $serializer->serialize([
            "id"     => $this->id,
            "title"  => $this->title
        ], 'json');
    }
}
