<?php

namespace App\Entity;

use App\Repository\RfcWorkflowsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Encoder\JsonEncode;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @ORM\Entity(repositoryClass=RfcWorkflowsRepository::class)
 */
class RfcWorkflows
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    public $id;

    /**
     * @ORM\ManyToOne(targetEntity=Rfc::class, inversedBy="rfcWorkflows")
     */
    private $rfc;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     */
    private $creator;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\ManyToOne(targetEntity=RfcWorkflows::class, inversedBy="childs")
     */
    public $parent;

    /**
     * @ORM\OneToMany(targetEntity=RfcWorkflows::class, mappedBy="parent")
     */
    private $childs;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="date")
     */
    private $start_date;

    /**
     * @ORM\Column(type="date")
     */
    private $end_date;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cc;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $notification;

    /**
     * @ORM\Column(type="text")
     */
    private $objects;

    /**
     * @ORM\Column(type="text")
     */
    private $persons;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $referenced_ticketid;

    /**
     * @ORM\ManyToOne(targetEntity=ImplementationStatus::class)
     */
    private $implementation_status;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     */
    private $assignee;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $attachment;

    public function __construct()
    {
        $this->childs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRfc(): ?Rfc
    {
        return $this->rfc;
    }

    public function setRfc(?Rfc $rfc): self
    {
        $this->rfc = $rfc;

        return $this;
    }

    public function getCreator(): ?User
    {
        return $this->creator;
    }

    public function setCreator(?User $creator): self
    {
        $this->creator = $creator;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getChilds(): Collection
    {
        return $this->childs;
    }

    public function addChild(self $child): self
    {
        if (!$this->childs->contains($child)) {
            $this->childs[] = $child;
            $child->setParent($this);
        }

        return $this;
    }

    public function removeChild(self $child): self
    {
        if ($this->childs->removeElement($child)) {
            // set the owning side to null (unless already changed)
            if ($child->getParent() === $this) {
                $child->setParent(null);
            }
        }

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->start_date;
    }

    public function setStartDate(\DateTimeInterface $start_date): self
    {
        $this->start_date = $start_date;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->end_date;
    }

    public function setEndDate(\DateTimeInterface $end_date): self
    {
        $this->end_date = $end_date;

        return $this;
    }

    public function getCc(): ?string
    {
        return $this->cc;
    }

    public function setCc(?string $cc): self
    {
        $this->cc = $cc;

        return $this;
    }

    public function getNotification(): ?\DateTimeInterface
    {
        return $this->notification;
    }

    public function setNotification(?\DateTimeInterface $notification): self
    {
        $this->notification = $notification;

        return $this;
    }

    public function getObjects(): ?string
    {
        return $this->objects;
    }

    public function setObjects(string $objects): self
    {
        $this->objects = $objects;

        return $this;
    }

    public function getPersons(): ?string
    {
        return $this->persons;
    }

    public function setPersons(string $persons): self
    {
        $this->persons = $persons;

        return $this;
    }

    public function serialized()
    {
        $encoder = new JsonEncode();
        $normalizer = new ObjectNormalizer();
        $serializer = new Serializer(array($normalizer),array($encoder));

        if ($this->parent) {
            $parent = $this->parent->id;
        } else {
            $parent = NULL;
        }

        if($this->assignee) {
            $assignee = $this->assignee->getId();
        } else {
            $assignee = NULL;
        }

        return $serializer->serialize([
            "id"          => $this->id,
            "title"       => $this->title,
            "persons"     => json_decode($this->persons),
            "objects"     => json_decode($this->objects),
            "attachments" => json_decode($this->attachment),
            "start_date"  => $this->start_date->format("Y-m-d"),
            "end_date"    => $this->end_date->format("Y-m-d"),
            "description" => $this->description,
            "parent"      => $parent,
            "assignee"    => $assignee
        ], 'json');
    }

    public function getReferencedTicketid(): ?string
    {
        return $this->referenced_ticketid;
    }

    public function setReferencedTicketid(?string $referenced_ticketid): self
    {
        $this->referenced_ticketid = $referenced_ticketid;

        return $this;
    }

    public function getImplementationStatus(): ?ImplementationStatus
    {
        return $this->implementation_status;
    }

    public function setImplementationStatus(?ImplementationStatus $implementation_status): self
    {
        $this->implementation_status = $implementation_status;

        return $this;
    }

    public function getAssignee(): ?User
    {
        return $this->assignee;
    }

    public function setAssignee(?User $assignee): self
    {
        $this->assignee = $assignee;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getAttachment(): ?string
    {
        return $this->attachment;
    }

    public function setAttachment(?string $attachment): self
    {
        $this->attachment = $attachment;

        return $this;
    }
}
