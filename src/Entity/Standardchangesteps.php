<?php

namespace App\Entity;

use App\Repository\StandardchangestepsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StandardchangestepsRepository::class)
 * @ORM\Table (name="standardchange_steps")
 */
class Standardchangesteps
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $const;

    /**
     * @ORM\Column(type="smallint")
     */
    private $locked;

    /**
     * @ORM\OneToMany(targetEntity=RfcStandardchanges::class, mappedBy="step")
     */
    private $rfcStandardchanges;

    public function __construct()
    {
        $this->rfcStandardchanges = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getConst(): ?string
    {
        return $this->const;
    }

    public function setConst(?string $const): self
    {
        $this->const = $const;

        return $this;
    }

    public function getLocked(): ?int
    {
        return $this->locked;
    }

    public function setLocked(int $locked): self
    {
        $this->locked = $locked;

        return $this;
    }

    /**
     * @return Collection|RfcStandardchanges[]
     */
    public function getRfcStandardchanges(): Collection
    {
        return $this->rfcStandardchanges;
    }

    public function addRfcStandardchange(RfcStandardchanges $rfcStandardchange): self
    {
        if (!$this->rfcStandardchanges->contains($rfcStandardchange)) {
            $this->rfcStandardchanges[] = $rfcStandardchange;
            $rfcStandardchange->setStep($this);
        }

        return $this;
    }

    public function removeRfcStandardchange(RfcStandardchanges $rfcStandardchange): self
    {
        if ($this->rfcStandardchanges->removeElement($rfcStandardchange)) {
            // set the owning side to null (unless already changed)
            if ($rfcStandardchange->getStep() === $this) {
                $rfcStandardchange->setStep(null);
            }
        }

        return $this;
    }
}
