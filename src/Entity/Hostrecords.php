<?php

namespace App\Entity;

use App\Repository\HostrecordsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=HostrecordsRepository::class)
 */
class Hostrecords
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $ipv4_addr;

    /**
     * @ORM\ManyToOne(targetEntity=Networkscans::class, inversedBy="hostrecords")
     * @ORM\JoinColumn(nullable=false)
     */
    private $networkscan;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $mac_addr;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $hostname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $vendor;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $objid;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIpv4Addr(): ?int
    {
        return $this->ipv4_addr;
    }

    public function setIpv4Addr(?int $ipv4_addr): self
    {
        $this->ipv4_addr = $ipv4_addr;

        return $this;
    }

    public function getNetworkscan(): ?Networkscans
    {
        return $this->networkscan;
    }

    public function setNetworkscan(?Networkscans $networkscan): self
    {
        $this->networkscan = $networkscan;

        return $this;
    }

    public function getMacAddr(): ?string
    {
        return $this->mac_addr;
    }

    public function setMacAddr(string $mac_addr): self
    {
        $this->mac_addr = $mac_addr;

        return $this;
    }

    public function getHostname(): ?string
    {
        return $this->hostname;
    }

    public function setHostname(?string $hostname): self
    {
        $this->hostname = $hostname;

        return $this;
    }

    public function getVendor(): ?string
    {
        return $this->vendor;
    }

    public function setVendor(?string $vendor): self
    {
        $this->vendor = $vendor;

        return $this;
    }

    public function getObjid(): ?int
    {
        return $this->objid;
    }

    public function setObjid(?int $objid): self
    {
        $this->objid = $objid;

        return $this;
    }
}
