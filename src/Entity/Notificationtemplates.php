<?php

namespace App\Entity;

use App\Repository\NotificationtemplatesRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Encoder\JsonEncode;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use League\HTMLToMarkdown\HtmlConverter;

/**
 * @ORM\Entity(repositoryClass=NotificationtemplatesRepository::class)
 * @ORM\Table(name="notification_templates")
 */
class Notificationtemplates
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $subject;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $content_en;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $content_de;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $active;

    /**
     * @ORM\ManyToOne(targetEntity=Notificationtypes::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $type;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setSubject(string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function getContentEn(): ?string
    {
        return $this->content_en;
    }

    public function setContentEn(?string $content_en): self
    {
        $this->content_en = $content_en;

        return $this;
    }

    public function getContentDe(): ?string
    {
        return $this->content_de;
    }

    public function setContentDe(?string $content_de): self
    {
        $this->content_de = $content_de;

        return $this;
    }

    public function getActive(): ?int
    {
        return $this->active;
    }

    public function setActive(?int $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getType(): ?Notificationtypes
    {
        return $this->type;
    }

    public function setType(?Notificationtypes $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function serialized()
    {
        $encoder = new JsonEncode();
        $normalizer = new ObjectNormalizer();
        $notification_type = $this->getType();

        $serializer = new Serializer(array($normalizer),array($encoder));

        $converter = new HtmlConverter();

        return $serializer->serialize([
            "id" => $this->id,
            "subject"         => $this->subject,
            "content_de"      => strip_tags($converter->convert($this->content_de)),
            //"content_de"      => $this->content_de,
            "content_en"      => strip_tags($converter->convert($this->content_en)),
            //"content_en"      => $this->content_en,
            "active"          => $this->active,
            "type"            => $notification_type->getId()
        ], 'json');
    }
}
