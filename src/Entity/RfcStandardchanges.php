<?php

namespace App\Entity;

use App\Repository\RfcStandardchangesRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Encoder\JsonEncode;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @ORM\Entity(repositoryClass=RfcStandardchangesRepository::class)
 */
class RfcStandardchanges
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\ManyToOne(targetEntity=Standardchangesteps::class, inversedBy="rfcStandardchanges")
     * @ORM\JoinColumn(nullable=false)
     */
    private $step;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     */
    private $creator;

    /**
     * @ORM\Column(type="text")
     */
    private $basedata;

    /**
     * @ORM\Column(type="text")
     */
    private $classificationdata;

    /**
     * @ORM\Column(type="text")
     */
    private $workflowdata;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(type="smallint")
     */
    private $active;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getStep(): ?Standardchangesteps
    {
        return $this->step;
    }

    public function setStep(?Standardchangesteps $step): self
    {
        $this->step = $step;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCreator(): ?User
    {
        return $this->creator;
    }

    public function setCreator(?User $creator): self
    {
        $this->creator = $creator;

        return $this;
    }

    public function getBasedata(): ?string
    {
        return $this->basedata;
    }

    public function setBasedata(string $basedata): self
    {
        $this->basedata = $basedata;

        return $this;
    }

    public function getClassificationdata(): ?string
    {
        return $this->classificationdata;
    }

    public function setClassificationdata(string $classificationdata): self
    {
        $this->classificationdata = $classificationdata;

        return $this;
    }

    public function getWorkflowdata(): ?string
    {
        return $this->workflowdata;
    }

    public function setWorkflowdata(string $workflowdata): self
    {
        $this->workflowdata = $workflowdata;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getActive(): ?int
    {
        return $this->active;
    }

    public function setActive(int $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function serialized()
    {
        $encoder = new JsonEncode();
        $normalizer = new ObjectNormalizer();
        $serializer = new Serializer(array($normalizer),array($encoder));

        $creator = $this->getCreator();
        if ($creator) {
            $creator = $creator->getId();
        } else {
            $creator = NULL;
        }

        return $serializer->serialize([
            "id"     => $this->id,
            "title"  => $this->title,
            "description" => $this->description,
            "creator" => $creator,
            "active"  => $this->active,
            "workflows" => $this->workflowdata
        ], 'json');
    }
}
