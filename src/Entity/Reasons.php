<?php

namespace App\Entity;

use App\Repository\ReasonsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Encoder\JsonEncode;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @ORM\Entity(repositoryClass=ReasonsRepository::class)
 */
class Reasons
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $const;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $fields = [];

    /**
     * @ORM\Column(type="smallint")
     */
    private $locked;

    /**
     * @ORM\OneToMany(targetEntity=Rfc::class, mappedBy="reason")
     */
    private $rfcs;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isDefault;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isActive;

    public function __construct()
    {
        $this->rfcs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getConst(): ?string
    {
        return $this->const;
    }

    public function setConst(?string $const): self
    {
        $this->const = $const;

        return $this;
    }

    public function getFields(): ? array
    {
        $fields = $this->fields;
        if (!is_array($fields)) {
            $fields = array();
        }
        return $fields;

    }

    public function setFields(?array $fields): self
    {
        $this->fields = ($fields);

        return $this;
    }

    public function getLocked(): ?int
    {
        return $this->locked;
    }

    public function setLocked(int $locked): self
    {
        $this->locked = $locked;

        return $this;
    }

    /**
     * @return Collection|Rfc[]
     */
    public function getRfcs(): Collection
    {
        return $this->rfcs;
    }

    public function addRfc(Rfc $rfc): self
    {
        if (!$this->rfcs->contains($rfc)) {
            $this->rfcs[] = $rfc;
            $rfc->setReason($this);
        }

        return $this;
    }

    public function removeRfc(Rfc $rfc): self
    {
        if ($this->rfcs->removeElement($rfc)) {
            // set the owning side to null (unless already changed)
            if ($rfc->getReason() === $this) {
                $rfc->setReason(null);
            }
        }

        return $this;
    }

    public function serialized()
    {
        $encoder = new JsonEncode();
        $normalizer = new ObjectNormalizer();
        $serializer = new Serializer(array($normalizer),array($encoder));

        return $serializer->serialize([
            "id"     => $this->id,
            "title"  => $this->title,
            "fields" => $this->getFields()
        ], 'json');
    }

    public function isIsDefault(): ?bool
    {
        return $this->isDefault;
    }

    public function setIsDefault(?bool $isDefault): self
    {
        $this->isDefault = $isDefault;

        return $this;
    }

    public function isIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(?bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }
}
