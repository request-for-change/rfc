<?php

namespace App\Entity;

use App\Repository\StatusRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StatusRepository::class)
 */
class Status
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $const;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="smallint")
     */
    private $locked;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $sort;

    /**
     * @ORM\OneToMany(targetEntity=Rfc::class, mappedBy="status")
     */
    private $rfcs;

    public function __construct()
    {
        $this->rfcs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getConst(): ?string
    {
        return $this->const;
    }

    public function setConst(string $const): self
    {
        $this->const = $const;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getLocked(): ?int
    {
        return $this->locked;
    }

    public function setLocked(int $locked): self
    {
        $this->locked = $locked;

        return $this;
    }

    public function getSort(): ?int
    {
        return $this->sort;
    }

    public function setSort(?int $sort): self
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * @return Collection|Rfc[]
     */
    public function getRfcs(): Collection
    {
        return $this->rfcs;
    }

    public function addRfc(Rfc $rfc): self
    {
        if (!$this->rfcs->contains($rfc)) {
            $this->rfcs[] = $rfc;
            $rfc->setStatus($this);
        }

        return $this;
    }

    public function removeRfc(Rfc $rfc): self
    {
        if ($this->rfcs->removeElement($rfc)) {
            // set the owning side to null (unless already changed)
            if ($rfc->getStatus() === $this) {
                $rfc->setStatus(null);
            }
        }

        return $this;
    }
}
