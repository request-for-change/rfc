<?php

namespace App\Entity;

use App\Repository\EvidenceCollectionsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EvidenceCollectionsRepository::class)
 */
class EvidenceCollections
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $creator;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $obj_id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $active;

    /**
     * @ORM\OneToMany(targetEntity=Networkscans::class, mappedBy="evidence_collection", orphanRemoval=true)
     */
    private $networkscans;

    public function __construct()
    {
        $this->networkscans = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getCreator(): ?User
    {
        return $this->creator;
    }

    public function setCreator(?User $creator): self
    {
        $this->creator = $creator;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(?\DateTimeInterface $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

    public function getObjId(): ?int
    {
        return $this->obj_id;
    }

    public function setObjId(?int $obj_id): self
    {
        $this->obj_id = $obj_id;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getActive(): ?int
    {
        return $this->active;
    }

    public function setActive(?int $active): self
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return Collection|Networkscans[]
     */
    public function getNetworkscans(): Collection
    {
        return $this->networkscans;
    }

    public function addNetworkscan(Networkscans $networkscan): self
    {
        if (!$this->networkscans->contains($networkscan)) {
            $this->networkscans[] = $networkscan;
            $networkscan->setEvidenceCollection($this);
        }

        return $this;
    }

    public function removeNetworkscan(Networkscans $networkscan): self
    {
        if ($this->networkscans->removeElement($networkscan)) {
            // set the owning side to null (unless already changed)
            if ($networkscan->getEvidenceCollection() === $this) {
                $networkscan->setEvidenceCollection(null);
            }
        }

        return $this;
    }
}
