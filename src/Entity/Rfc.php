<?php

namespace App\Entity;

use App\Repository\RfcRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RfcRepository::class)
 */
class Rfc
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $referenced_ticketid;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $consequences;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $affected_components;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $customfields;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $commentary;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $affected_componentsstore;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     */
    private $creator;

    /**
     * @ORM\ManyToOne(targetEntity=Departments::class, inversedBy="rfcs")
     */
    private $department;

    /**
     * @ORM\ManyToOne(targetEntity=Costcenters::class, inversedBy="rfcs")
     */
    private $costcenter;

    /**
     * @ORM\ManyToOne(targetEntity=Reasons::class, inversedBy="rfcs")
     */
    private $reason;

    /**
     * @ORM\ManyToOne(targetEntity=Status::class, inversedBy="rfcs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity=Priorities::class, inversedBy="rfcs")
     */
    private $priority;

    /**
     * @ORM\ManyToOne(targetEntity=Projects::class, inversedBy="rfcs")
     */
    private $project;

    /**
     * @ORM\OneToMany(targetEntity=RfcNotices::class, mappedBy="rfc", orphanRemoval=true)
     */
    private $rfcNotices;

    /**
     * @ORM\OneToMany(targetEntity=RfcWorkflows::class, mappedBy="rfc")
     */
    private $rfcWorkflows;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $attachment;

    /**
     * @ORM\ManyToOne(targetEntity=Datetypes::class)
     */
    private $datetype;

    /**
     * @ORM\ManyToOne(targetEntity=BusinessProcesses::class)
     */
    private $business_process;

    /**
     * @ORM\OneToOne(targetEntity=RfcClassifications::class, mappedBy="rfc")
     */
    private $rfc_classification;

    /**
     * @ORM\OneToMany(targetEntity=RfcBpoFeedback::class, mappedBy="rfc", orphanRemoval=true)
     */
    private $rfcBpoFeedback;

    public function __construct()
    {
        $this->rfcNotices = new ArrayCollection();
        $this->rfcWorkflows = new ArrayCollection();
        $this->rfcBpoFeedback = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getReferencedTicketid(): ?string
    {
        return $this->referenced_ticketid;
    }

    public function setReferencedTicketid(?string $referenced_ticketid): self
    {
        $this->referenced_ticketid = $referenced_ticketid;

        return $this;
    }

    public function getConsequences(): ?string
    {
        return $this->consequences;
    }

    public function setConsequences(?string $consequences): self
    {
        $this->consequences = $consequences;

        return $this;
    }

    public function getAffectedComponents(): ?string
    {
        return $this->affected_components;
    }

    public function setAffectedComponents(string $affected_components): self
    {
        $this->affected_components = $affected_components;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getCustomfields(): ?string
    {
        return $this->customfields;
    }

    public function setCustomfields(?string $customfields): self
    {
        $this->customfields = $customfields;

        return $this;
    }

    public function getCommentary(): ?string
    {
        return $this->commentary;
    }

    public function setCommentary(?string $commentary): self
    {
        $this->commentary = $commentary;

        return $this;
    }

    public function getAffectedComponentsstore(): ?string
    {
        return $this->affected_componentsstore;
    }

    public function setAffectedComponentsstore(?string $affected_componentsstore): self
    {
        $this->affected_componentsstore = $affected_componentsstore;

        return $this;
    }

    public function getCreator(): ?User
    {
        return $this->creator;
    }

    public function setCreator(?User $creator): self
    {
        $this->creator = $creator;

        return $this;
    }

    public function getDepartment(): ?Departments
    {
        return $this->department;
    }

    public function setDepartment(?Departments $department): self
    {
        $this->department = $department;

        return $this;
    }

    public function getCostcenter(): ?Costcenters
    {
        return $this->costcenter;
    }

    public function setCostcenter(?Costcenters $costcenter): self
    {
        $this->costcenter = $costcenter;

        return $this;
    }

    public function getReason(): ?Reasons
    {
        return $this->reason;
    }

    public function setReason(?Reasons $reason): self
    {
        $this->reason = $reason;

        return $this;
    }

    public function getStatus(): ?Status
    {
        return $this->status;
    }

    public function setStatus(?Status $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getPriority(): ?Priorities
    {
        return $this->priority;
    }

    public function setPriority(?Priorities $priority): self
    {
        $this->priority = $priority;

        return $this;
    }

    public function getProject(): ?Projects
    {
        return $this->project;
    }

    public function setProject(?Projects $project): self
    {
        $this->project = $project;

        return $this;
    }

    /**
     * @return Collection|RfcNotices[]
     */
    public function getRfcNotices(): Collection
    {
        return $this->rfcNotices;
    }

    public function addRfcNotice(RfcNotices $rfcNotice): self
    {
        if (!$this->rfcNotices->contains($rfcNotice)) {
            $this->rfcNotices[] = $rfcNotice;
            $rfcNotice->setRfc($this);
        }

        return $this;
    }

    public function removeRfcNotice(RfcNotices $rfcNotice): self
    {
        if ($this->rfcNotices->removeElement($rfcNotice)) {
            // set the owning side to null (unless already changed)
            if ($rfcNotice->getRfc() === $this) {
                $rfcNotice->setRfc(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|RfcWorkflows[]
     */
    public function getRfcWorkflows(): Collection
    {
        return $this->rfcWorkflows;
    }

    public function addRfcWorkflow(RfcWorkflows $rfcWorkflow): self
    {
        if (!$this->rfcWorkflows->contains($rfcWorkflow)) {
            $this->rfcWorkflows[] = $rfcWorkflow;
            $rfcWorkflow->setRfc($this);
        }

        return $this;
    }

    public function removeRfcWorkflow(RfcWorkflows $rfcWorkflow): self
    {
        if ($this->rfcWorkflows->removeElement($rfcWorkflow)) {
            // set the owning side to null (unless already changed)
            if ($rfcWorkflow->getRfc() === $this) {
                $rfcWorkflow->setRfc(null);
            }
        }

        return $this;
    }

    public function getAttachment(): ?string
    {
        return $this->attachment;
    }

    public function setAttachment(?string $attachment): self
    {
        $this->attachment = $attachment;

        return $this;
    }

    public function getDatetype(): ?Datetypes
    {
        return $this->datetype;
    }

    public function setDatetype(?Datetypes $datetype): self
    {
        $this->datetype = $datetype;

        return $this;
    }

    public function getBusinessProcess(): ?BusinessProcesses
    {
        return $this->business_process;
    }

    public function setBusinessProcess(?BusinessProcesses $business_process): self
    {
        $this->business_process = $business_process;

        return $this;
    }

    public function getRfcClassification(): ?RfcClassifications
    {
        return $this->rfc_classification;
    }

    public function setRfcClassification(?RfcClassifications $rfcClassifications): self
    {
        $this->business_process = $rfcClassifications;

        return $this;
    }

    /**
     * @return Collection|RfcBpoFeedback[]
     */
    public function getRfcBpoFeedback(): Collection
    {
        return $this->rfcBpoFeedback;
    }

    public function addRfcBpoFeedback(RfcBpoFeedback $rfcBpoFeedback): self
    {
        if (!$this->rfcBpoFeedback->contains($rfcBpoFeedback)) {
            $this->rfcBpoFeedback[] = $rfcBpoFeedback;
            $rfcBpoFeedback->setRfc($this);
        }

        return $this;
    }

    public function removeRfcBpoFeedback(RfcBpoFeedback $rfcBpoFeedback): self
    {
        if ($this->rfcBpoFeedback->removeElement($rfcBpoFeedback)) {
            // set the owning side to null (unless already changed)
            if ($rfcBpoFeedback->getRfc() === $this) {
                $rfcBpoFeedback->setRfc(null);
            }
        }

        return $this;
    }
}
