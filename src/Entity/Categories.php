<?php

namespace App\Entity;

use App\Repository\CategoriesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CategoriesRepository::class)
 */
class Categories
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $const;

    /**
     * @ORM\Column(type="smallint")
     */
    private $locked;

    /**
     * @ORM\OneToMany(targetEntity=RfcClassifications::class, mappedBy="category")
     */
    private $rfcClassifications;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    public function __construct()
    {
        $this->rfcClassifications = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getConst(): ?string
    {
        return $this->const;
    }

    public function setConst(?string $const): self
    {
        $this->const = $const;

        return $this;
    }

    public function getLocked(): ?int
    {
        return $this->locked;
    }

    public function setLocked(int $locked): self
    {
        $this->locked = $locked;

        return $this;
    }

    /**
     * @return Collection|RfcClassifications[]
     */
    public function getRfcClassifications(): Collection
    {
        return $this->rfcClassifications;
    }

    public function addRfcClassification(RfcClassifications $rfcClassification): self
    {
        if (!$this->rfcClassifications->contains($rfcClassification)) {
            $this->rfcClassifications[] = $rfcClassification;
            $rfcClassification->setCategory($this);
        }

        return $this;
    }

    public function removeRfcClassification(RfcClassifications $rfcClassification): self
    {
        if ($this->rfcClassifications->removeElement($rfcClassification)) {
            // set the owning side to null (unless already changed)
            if ($rfcClassification->getCategory() === $this) {
                $rfcClassification->setCategory(null);
            }
        }

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
