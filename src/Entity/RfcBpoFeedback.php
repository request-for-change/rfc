<?php

namespace App\Entity;

use App\Repository\RfcBpoFeedbackRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RfcBpoFeedbackRepository::class)
 */
class RfcBpoFeedback
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     */
    private $delegatee;

    /**
     * @ORM\ManyToOne(targetEntity=Rfc::class, inversedBy="rfcBpoFeedback")
     * @ORM\JoinColumn(nullable=false)
     */
    private $rfc;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDelegatee(): ?User
    {
        return $this->delegatee;
    }

    public function setDelegatee(?User $delegatee): self
    {
        $this->delegatee = $delegatee;

        return $this;
    }

    public function getRfc(): ?Rfc
    {
        return $this->rfc;
    }

    public function setRfc(?Rfc $rfc): self
    {
        $this->rfc = $rfc;

        return $this;
    }
}
