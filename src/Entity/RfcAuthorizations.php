<?php

namespace App\Entity;

use App\Repository\RfcAuthorizationsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RfcAuthorizationsRepository::class)
 */
class RfcAuthorizations
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=RfcClassifications::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $classification;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $author;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $establishment;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $target_date;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(type="integer")
     */
    private $decision;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $attachment;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClassification(): ?RfcClassifications
    {
        return $this->classification;
    }

    public function setClassification(RfcClassifications $classification): self
    {
        $this->classification = $classification;

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getEstablishment(): ?string
    {
        return $this->establishment;
    }

    public function setEstablishment(string $establishment): self
    {
        $this->establishment = $establishment;

        return $this;
    }

    public function getTargetDate(): ?\DateTimeInterface
    {
        return $this->target_date;
    }

    public function setTargetDate($target_date): self
    {
        $this->target_date = $target_date;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getDecision(): ?int
    {
        return $this->decision;
    }

    public function setDecision(int $decision): self
    {
        $this->decision = $decision;

        return $this;
    }

    public function getAttachment(): ?string
    {
        return $this->attachment;
    }

    public function setAttachment(?string $attachment): self
    {
        $this->attachment = $attachment;

        return $this;
    }
}
