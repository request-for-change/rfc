<?php

namespace App\Entity;

use App\Repository\LabelsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LabelsRepository::class)
 */
class Labels
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $driver;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $zml_code;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $hostaddress;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $rows_on_page;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $cols_on_row;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $page_size;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $orientation;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $margin_left;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $margin_top;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $qr_size;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $label_width;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $label_height;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $logo_src;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $logo_height;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $fontsize_1;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $fontsize_2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $attribute_1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $attribute_2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $attribute_3;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $attribute_4;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $creator;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $print_density;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $page_width;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $page_height;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $host;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $port;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDriver(): ?string
    {
        return $this->driver;
    }

    public function setDriver(string $driver): self
    {
        $this->driver = $driver;

        return $this;
    }

    public function getZmlCode(): ?string
    {
        return $this->zml_code;
    }

    public function setZmlCode(?string $zml_code): self
    {
        $this->zml_code = $zml_code;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getHostaddress(): ?string
    {
        return $this->hostaddress;
    }

    public function setHostaddress(?string $hostaddress): self
    {
        $this->hostaddress = $hostaddress;

        return $this;
    }

    public function getRowsOnPage(): ?int
    {
        return $this->rows_on_page;
    }

    public function setRowsOnPage(?int $rows_on_page): self
    {
        $this->rows_on_page = $rows_on_page;

        return $this;
    }

    public function getColsOnRow(): ?int
    {
        return $this->cols_on_row;
    }

    public function setColsOnRow(?int $cols_on_row): self
    {
        $this->cols_on_row = $cols_on_row;

        return $this;
    }

    public function getPageSize(): ?string
    {
        return $this->page_size;
    }

    public function setPageSize(?string $page_size): self
    {
        $this->page_size = $page_size;

        return $this;
    }

    public function getOrientation(): ?string
    {
        return $this->orientation;
    }

    public function setOrientation(?string $orientation): self
    {
        $this->orientation = $orientation;

        return $this;
    }

    public function getMarginLeft(): ?int
    {
        return $this->margin_left;
    }

    public function setMarginLeft(?int $margin_left): self
    {
        $this->margin_left = $margin_left;

        return $this;
    }

    public function getMarginTop(): ?int
    {
        return $this->margin_top;
    }

    public function setMarginTop(?int $margin_top): self
    {
        $this->margin_top = $margin_top;

        return $this;
    }

    public function getQrSize(): ?int
    {
        return $this->qr_size;
    }

    public function setQrSize(?int $qr_size): self
    {
        $this->qr_size = $qr_size;

        return $this;
    }

    public function getLabelWidth(): ?int
    {
        return $this->label_width;
    }

    public function setLabelWidth(?int $label_width): self
    {
        $this->label_width = $label_width;

        return $this;
    }

    public function getLabelHeight(): ?int
    {
        return $this->label_height;
    }

    public function setLabelHeight(?int $label_height): self
    {
        $this->label_height = $label_height;

        return $this;
    }

    public function getLogoSrc(): ?string
    {
        return $this->logo_src;
    }

    public function setLogoSrc(?string $logo_src): self
    {
        $this->logo_src = $logo_src;

        return $this;
    }

    public function getLogoHeight(): ?int
    {
        return $this->logo_height;
    }

    public function setLogoHeight(?int $logo_height): self
    {
        $this->logo_height = $logo_height;

        return $this;
    }

    public function getFontsize1(): ?int
    {
        return $this->fontsize_1;
    }

    public function setFontsize1(?int $fontsize_1): self
    {
        $this->fontsize_1 = $fontsize_1;

        return $this;
    }

    public function getFontsize2(): ?int
    {
        return $this->fontsize_2;
    }

    public function setFontsize2(?int $fontsize_2): self
    {
        $this->fontsize_2 = $fontsize_2;

        return $this;
    }

    public function getAttribute1(): ?string
    {
        return $this->attribute_1;
    }

    public function setAttribute1(?string $attribute_1): self
    {
        $this->attribute_1 = $attribute_1;

        return $this;
    }

    public function getAttribute2(): ?string
    {
        return $this->attribute_2;
    }

    public function setAttribute2(?string $attribute_2): self
    {
        $this->attribute_2 = $attribute_2;

        return $this;
    }

    public function getAttribute3(): ?string
    {
        return $this->attribute_3;
    }

    public function setAttribute3(?string $attribute_3): self
    {
        $this->attribute_3 = $attribute_3;

        return $this;
    }

    public function getAttribute4(): ?string
    {
        return $this->attribute_4;
    }

    public function setAttribute4(?string $attribute_4): self
    {
        $this->attribute_4 = $attribute_4;

        return $this;
    }

    public function isActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getCreator(): ?User
    {
        return $this->creator;
    }

    public function setCreator(?User $creator): self
    {
        $this->creator = $creator;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getPrintDensity(): ?int
    {
        return $this->print_density;
    }

    public function setPrintDensity(?int $print_density): self
    {
        $this->print_density = $print_density;

        return $this;
    }

    public function getPageWidth(): ?int
    {
        return $this->page_width;
    }

    public function setPageWidth(?int $page_width): self
    {
        $this->page_width = $page_width;

        return $this;
    }

    public function getPageHeight(): ?int
    {
        return $this->page_height;
    }

    public function setPageHeight(?int $page_height): self
    {
        $this->page_height = $page_height;

        return $this;
    }

    public function getHost(): ?string
    {
        return $this->host;
    }

    public function setHost(?string $host): self
    {
        $this->host = $host;

        return $this;
    }

    public function getPort(): ?int
    {
        return $this->port;
    }

    public function setPort(?int $port): self
    {
        $this->port = $port;

        return $this;
    }
}
