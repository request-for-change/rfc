<?php

namespace App\Entity;

use App\Repository\ProjectsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Encoder\JsonEncode;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @ORM\Entity(repositoryClass=ProjectsRepository::class)
 */
class Projects
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $const;

    /**
     * @ORM\Column(type="smallint")
     */
    private $locked;

    /**
     * @ORM\OneToMany(targetEntity=Rfc::class, mappedBy="project")
     */
    private $rfcs;

    public function __construct()
    {
        $this->rfcs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getConst(): ?string
    {
        return $this->const;
    }

    public function setConst(?string $const): self
    {
        $this->const = $const;

        return $this;
    }

    public function getLocked(): ?int
    {
        return $this->locked;
    }

    public function setLocked(int $locked): self
    {
        $this->locked = $locked;

        return $this;
    }

    /**
     * @return Collection|Rfc[]
     */
    public function getRfcs(): Collection
    {
        return $this->rfcs;
    }

    public function addRfc(Rfc $rfc): self
    {
        if (!$this->rfcs->contains($rfc)) {
            $this->rfcs[] = $rfc;
            $rfc->setProject($this);
        }

        return $this;
    }

    public function removeRfc(Rfc $rfc): self
    {
        if ($this->rfcs->removeElement($rfc)) {
            // set the owning side to null (unless already changed)
            if ($rfc->getProject() === $this) {
                $rfc->setProject(null);
            }
        }

        return $this;
    }

    public function serialized()
    {
        $encoder = new JsonEncode();
        $normalizer = new ObjectNormalizer();
        $serializer = new Serializer(array($normalizer),array($encoder));

        return $serializer->serialize([
            "id"     => $this->id,
            "title"  => $this->title
        ], 'json');
    }
}
