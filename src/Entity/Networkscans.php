<?php

namespace App\Entity;

use App\Repository\NetworkscansRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=NetworkscansRepository::class)
 */
class Networkscans
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=EvidenceCollections::class, inversedBy="networkscans")
     * @ORM\JoinColumn(nullable=false)
     */
    private $evidence_collection;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $creator;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @ORM\OneToMany(targetEntity=Hostrecords::class, mappedBy="networkscan", orphanRemoval=true)
     */
    private $hostrecords;

    public function __construct()
    {
        $this->hostrecords = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEvidenceCollection(): ?EvidenceCollections
    {
        return $this->evidence_collection;
    }

    public function setEvidenceCollection(?EvidenceCollections $evidence_collection): self
    {
        $this->evidence_collection = $evidence_collection;

        return $this;
    }

    public function getCreator(): ?User
    {
        return $this->creator;
    }

    public function setCreator(?User $creator): self
    {
        $this->creator = $creator;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getIpv4Addr(): ?int
    {
        return $this->ipv4_addr;
    }

    public function setIpv4Addr(?int $ipv4_addr): self
    {
        $this->ipv4_addr = $ipv4_addr;

        return $this;
    }

    public function getMacAddr(): ?string
    {
        return $this->mac_addr;
    }

    public function setMacAddr(?string $mac_addr): self
    {
        $this->mac_addr = $mac_addr;

        return $this;
    }

    public function getHostname(): ?string
    {
        return $this->hostname;
    }

    public function setHostname(?string $hostname): self
    {
        $this->hostname = $hostname;

        return $this;
    }

    /**
     * @return Collection|Hostrecords[]
     */
    public function getHostrecords(): Collection
    {
        return $this->hostrecords;
    }

    public function addHostrecord(Hostrecords $hostrecord): self
    {
        if (!$this->hostrecords->contains($hostrecord)) {
            $this->hostrecords[] = $hostrecord;
            $hostrecord->setNetworkscan($this);
        }

        return $this;
    }

    public function removeHostrecord(Hostrecords $hostrecord): self
    {
        if ($this->hostrecords->removeElement($hostrecord)) {
            // set the owning side to null (unless already changed)
            if ($hostrecord->getNetworkscan() === $this) {
                $hostrecord->setNetworkscan(null);
            }
        }

        return $this;
    }
}
