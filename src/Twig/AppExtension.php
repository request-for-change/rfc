<?php
/**
 * don’t panic it-services
 * User: christianwally
 * Date: 06.12.21
 * Time: 09:36
 */


namespace App\Twig;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;


class AppExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('long2ip', [$this, 'convertInt2Ip']),
        ];
    }

    public function convertInt2Ip($integer) {
        return long2ip($integer);
    }

}