<?php

namespace App\Security;

use AllowDynamicProperties;
use App\Controller\IdoitController;
use App\Controller\LdapController;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\CsrfTokenBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\CustomCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\PasswordCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Util\TargetPathTrait;


class LoginFormAuthenticator extends AbstractAuthenticator
{
    use TargetPathTrait;

    private const LOGIN_ROUTE = 'rfc_login';

    private EntityManagerInterface $entityManager;
    private UrlGeneratorInterface $urlGenerator;
    private CsrfTokenManagerInterface $csrfTokenManager;
    /**
     * @var UserPasswordHasherInterface
     */
    private UserPasswordHasherInterface $userPasswordHasher;
    /**
     * @var UserRepository
     */
    private UserRepository $userRepository;
    private LoggerInterface $logger;

    public function __construct(UserRepository $userRepository, UrlGeneratorInterface $urlGenerator, CsrfTokenManagerInterface $csrfTokenManager,EntityManagerInterface $entityManager, UserPasswordHasherInterface $userPasswordHasher, LoggerInterface $logger)
    {
        $this->entityManager = $entityManager;
        $this->userRepository = $userRepository;
        $this->urlGenerator = $urlGenerator;
        $this->csrfTokenManager = $csrfTokenManager;
        $this->userPasswordHasher = $userPasswordHasher;
        $this->logger = $logger;
    }

    public function supports(Request $request): ?bool
    {
        if ($this->getLoginUrl($request) === $request->getRequestUri() && $request->isMethod('POST')) {
            return true;
        } else {
            return false;
        }

    }

    public function authenticate(Request $request): Passport
    {
        $credentials = $this->getCredentials($request);


        $token = new CsrfToken('authenticate', $credentials['csrf_token']);
        if (!$this->csrfTokenManager->isTokenValid($token)) {
            throw new InvalidCsrfTokenException();
        }


        $userLoader = function ($userIdentifier) {
            $user = $this->userRepository->findOneBy(['username' => $userIdentifier]);
            if (!$user or $user->getLocked()) {
                throw new UserNotFoundException();
            }
            return $user;
        };

        $userBadge = new UserBadge($credentials['username'], $userLoader);

        return new Passport($userBadge,
            new CustomCredentials(function ($credentials, PasswordAuthenticatedUserInterface $user){
                return $this->checkCredentials($credentials, $user, $this->logger);
            }, $credentials)
        );
    }

    public function getCredentials(Request $request): array
    {
        $credentials = [
            'username' => $request->request->get('username'),
            'password' => $request->request->get('password'),
            'csrf_token' => $request->request->get('_csrf_token'),
        ];
        $request->getSession()->set(
            Security::LAST_USERNAME,
            $credentials['username']
        );

        return $credentials;
    }



    public function checkCredentials($credentials, PasswordAuthenticatedUserInterface $user): bool
    {
        // Check the user's password or other credentials and return true or false
        // If there are no credentials to check, you can just return true
        $idoit = new IdoitController();
        $ldap = new LdapController();

        $authenticated = false;

        // first we try to authenticate against
        if ($credentials['password'] != '' && $this->userPasswordHasher->isPasswordValid($user, $credentials['password']))
        {
            /** @var User $user */

            $logins = $user->getLogins();
            $user->setLogins($logins+1);
            $user->setLastLogin(time());
            $this->entityManager->persist($user);
            $this->entityManager->flush();
            $authenticated = true;
            $this->logger->info('User was successfully authenticated against local User');
        }
        // if local User was wrong we try to authenticate against i-doit
        elseif ($idoit->login($credentials, $this->entityManager, $this->logger))
        {
            /** @var User $user */
            $authenticated = true;
            $user->setPassword($this->userPasswordHasher->hashPassword($user, $credentials['password']));
            $logins = $user->getLogins();
            $user->setLogins($logins+1);
            $user->setLastLogin(time());
            $this->entityManager->persist($user);
            $this->entityManager->flush();
            $this->logger->info('User was successfully authenticated against i-doit');
        }
        // if neither local authentication nor i-dot works, we try with ldap
        elseif ($ldap->login($this->entityManager, $credentials))
        {
            /** @var User $user */

            $authenticated = true;
            $user->setPassword($this->userPasswordHasher->hashPassword($user, $credentials['password']));
            $logins = $user->getLogins();
            $user->setLogins($logins+1);
            $user->setLastLogin(time());
            $this->entityManager->persist($user);
            $this->entityManager->flush();
            $this->logger->info('User was successfully authenticated against LDAP');
        }


        return $authenticated;

    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        // Set Language
        if ($token->getuser()->getLanguage()) {
            $request->getSession()->set('_locale', $token->getuser()->getLanguage());
        }


        if ($targetPath = $this->getTargetPath($request->getSession(), $firewallName)) {
            return new RedirectResponse($targetPath);
        }

        // For example : return new RedirectResponse($this->urlGenerator->generate('some_route'));
        return new RedirectResponse($this->urlGenerator->generate('rfc_index'));
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        $request->getSession()->set(Security::AUTHENTICATION_ERROR, $exception);

        return new RedirectResponse($this->getLoginUrl());
    }


    protected function getLoginUrl()
    {
        return $this->urlGenerator->generate(self::LOGIN_ROUTE);
    }
}
