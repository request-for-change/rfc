<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211115080428 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE networkscans (id INT AUTO_INCREMENT NOT NULL, evidence_collection_id INT NOT NULL, creator_id INT NOT NULL, created DATETIME NOT NULL, INDEX IDX_CDC413B55BFA936 (evidence_collection_id), INDEX IDX_CDC413B61220EA6 (creator_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE networkscans ADD CONSTRAINT FK_CDC413B55BFA936 FOREIGN KEY (evidence_collection_id) REFERENCES evidence_collections (id)');
        $this->addSql('ALTER TABLE networkscans ADD CONSTRAINT FK_CDC413B61220EA6 FOREIGN KEY (creator_id) REFERENCES users (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE networkscans');
        $this->addSql('ALTER TABLE rfc_workflows CHANGE attachment attachment TEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
