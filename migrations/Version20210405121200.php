<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210405121200 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('INSERT INTO `config` (`group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES ("system","system_url","","N;",NULL,NULL,"text","2021-04-05 12:00:00")');
        $this->addSql('INSERT INTO `config` (`group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES ("system","four_eyes","","N;",NULL,NULL,"integer","2021-04-05 12:00:00")');
        $this->addSql('INSERT INTO `config` (`group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES ("system","notify_at_once","","N;",NULL,NULL,"integer","2021-04-05 12:00:00")');
        $this->addSql('INSERT INTO `config` (`group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES ("system","section_required","","N;",NULL,NULL,"integer","2021-04-05 12:00:00")');
        $this->addSql('INSERT INTO `config` (`group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES ("system","process_required","","N;",NULL,NULL,"integer","2021-04-05 12:00:00")');
        $this->addSql('INSERT INTO `config` (`group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES ("system","currency","","N;",NULL,NULL,"text","2021-04-05 12:00:00")');

    }

    public function down(Schema $schema) : void
    {

    }
}
