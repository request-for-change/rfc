<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220803060927 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE labels ADD creator_id INT NOT NULL, ADD active TINYINT(1) NOT NULL, ADD created DATETIME NOT NULL');
        $this->addSql('ALTER TABLE labels ADD CONSTRAINT FK_B5D1021161220EA6 FOREIGN KEY (creator_id) REFERENCES users (id)');
        $this->addSql('CREATE INDEX IDX_B5D1021161220EA6 ON labels (creator_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE labels DROP FOREIGN KEY FK_B5D1021161220EA6');
        $this->addSql('DROP INDEX IDX_B5D1021161220EA6 ON labels');
        $this->addSql('ALTER TABLE labels DROP creator_id, DROP active, DROP created');
    }
}
