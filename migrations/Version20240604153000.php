<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add Value to Config
 */
final class Version20240604153000 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('INSERT INTO `config` (`group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES ("system","optional_submission_description","Make Description at Submission optional","N;",NULL,NULL,"integer","2024-06-04 15:30:00")');
        $this->addSql('INSERT INTO `config` (`group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES ("system","optional_submission_consequences","Make Consequences at Submission optional","N;",NULL,NULL,"integer","2024-06-04 15:30:00")');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
