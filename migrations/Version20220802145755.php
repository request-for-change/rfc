<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220802145755 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE labels (id INT AUTO_INCREMENT NOT NULL, driver VARCHAR(255) NOT NULL, zml_code LONGTEXT DEFAULT NULL, title VARCHAR(255) NOT NULL, hostaddress VARCHAR(255) DEFAULT NULL, rows_on_page INT DEFAULT NULL, cols_on_row INT DEFAULT NULL, page_size VARCHAR(255) DEFAULT NULL, orientation VARCHAR(255) DEFAULT NULL, margin_left INT DEFAULT NULL, margin_top INT DEFAULT NULL, qr_size INT DEFAULT NULL, label_width INT DEFAULT NULL, label_height INT DEFAULT NULL, logo_src VARCHAR(255) DEFAULT NULL, logo_height INT DEFAULT NULL, fontsize_1 INT DEFAULT NULL, fontsize_2 INT DEFAULT NULL, attribute_1 VARCHAR(255) DEFAULT NULL, attribute_2 VARCHAR(255) DEFAULT NULL, attribute_3 VARCHAR(255) DEFAULT NULL, attribute_4 VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE labels');
    }
}
