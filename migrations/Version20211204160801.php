<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211204160801 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE hostrecords (id INT AUTO_INCREMENT NOT NULL, networkscan_id INT NOT NULL, ipv4_addr BIGINT DEFAULT NULL, mac_addr VARCHAR(255) DEFAULT NULL, hostname VARCHAR(255) DEFAULT NULL, INDEX IDX_41B97E2EB3074A36 (networkscan_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE hostrecords ADD CONSTRAINT FK_41B97E2EB3074A36 FOREIGN KEY (networkscan_id) REFERENCES networkscans (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE hostrecords');
        $this->addSql('ALTER TABLE networkscans DROP ipv4_addr, DROP mac_addr, DROP hostname');
    }
}
