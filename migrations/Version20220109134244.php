<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220109134244 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add signature value to config table';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('INSERT INTO `config` (`group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES ("email","signature","signature","N;",NULL,NULL,"text","2022-01-09 14:44:00")');

    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
