<?php
// $this->addSql('INSERT INTO `field_types` (`id`, `title`, `const`, `locked`, `renderer`) VALUES (4,"Datum","C__FIELDTYPE__DATEPICKER",0,"Component_View_Renderer_DatePicker")');

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Manualy created to add values in standardchange_steps
 */
final class Version20210709100000 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('INSERT INTO `standardchange_steps` (`id`, `title`, `const`, `locked`) VALUES (1,"Draft","C__SS_STEP__GENESIS",1)');
        $this->addSql('INSERT INTO `standardchange_steps` (`id`, `title`, `const`, `locked`) VALUES (2,"Classification","C__SS_STEP__CLASSIFICATION",1)');
        $this->addSql('INSERT INTO `standardchange_steps` (`id`, `title`, `const`, `locked`) VALUES (3,"Implemetation","C__SS_STEP__IMPLEMENTATION",1)');
    }

    public function down(Schema $schema): void
    {
        //
    }
}