<?php
/**
 * don’t panic it-services
 * User: christianwally
 * Date: 29.10.21
 * Time: 08:08
 */
declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Manualy created to add values in config table
 */
final class Version20211029080000 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('INSERT INTO `config` (`group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES ("tts","state_id_open","","' . addslashes(serialize("2")) .'",NULL,NULL,"text","' . date('Y-m-d h:m:s') .'")');
        $this->addSql('INSERT INTO `config` (`group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES ("tts","state_id_closed","","' . addslashes(serialize("4")) .'",NULL,NULL,"text","' . date('Y-m-d h:m:s') .'")');
    }

    public function down(Schema $schema): void
    {
        //
    }
}
