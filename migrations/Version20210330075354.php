<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210330075354 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE authorizations (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, const VARCHAR(255) NOT NULL, locked SMALLINT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE business_processes (id INT AUTO_INCREMENT NOT NULL, responsible_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, const VARCHAR(255) DEFAULT NULL, locked SMALLINT DEFAULT NULL, INDEX IDX_BD4C9833602AD315 (responsible_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE categories (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, const VARCHAR(255) DEFAULT NULL, locked SMALLINT NOT NULL, description VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE config (id INT AUTO_INCREMENT NOT NULL, group_name VARCHAR(255) NOT NULL, config_key VARCHAR(255) NOT NULL, label VARCHAR(255) NOT NULL, config_value LONGTEXT DEFAULT NULL, `default` LONGTEXT DEFAULT NULL, rules LONGTEXT DEFAULT NULL, field_type VARCHAR(255) NOT NULL, date DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cost_centers (id INT AUTO_INCREMENT NOT NULL, responsible_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, const VARCHAR(255) DEFAULT NULL, locked SMALLINT NOT NULL, INDEX IDX_6123530E602AD315 (responsible_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE date_types (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, const VARCHAR(255) DEFAULT NULL, locked SMALLINT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE decisions (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, const VARCHAR(255) DEFAULT NULL, locked SMALLINT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE departments (id INT AUTO_INCREMENT NOT NULL, manager_id INT DEFAULT NULL, title VARCHAR(255) DEFAULT NULL, locked SMALLINT NOT NULL, const VARCHAR(255) DEFAULT NULL, INDEX IDX_16AEB8D4783E3463 (manager_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE field_types (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, const VARCHAR(255) NOT NULL, locked SMALLINT NOT NULL, renderer VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE implementation_status (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, const VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, locked SMALLINT NOT NULL, sort INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE notification_templates (id INT AUTO_INCREMENT NOT NULL, type_id INT NOT NULL, subject VARCHAR(255) NOT NULL, content_en LONGTEXT DEFAULT NULL, content_de LONGTEXT DEFAULT NULL, active SMALLINT DEFAULT NULL, INDEX IDX_C9C13AD1C54C8C93 (type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE notification_types (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, const VARCHAR(255) DEFAULT NULL, locked SMALLINT NOT NULL, class VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE priorities (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, const VARCHAR(255) DEFAULT NULL, locked SMALLINT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE projects (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, const VARCHAR(255) DEFAULT NULL, locked SMALLINT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reasons (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, const VARCHAR(255) DEFAULT NULL, fields LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', locked SMALLINT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reports (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, data LONGTEXT DEFAULT NULL, INDEX IDX_F11FA745A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ressource_types (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, const VARCHAR(255) DEFAULT NULL, locked SMALLINT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rfc (id INT AUTO_INCREMENT NOT NULL, creator_id INT DEFAULT NULL, department_id INT DEFAULT NULL, costcenter_id INT DEFAULT NULL, reason_id INT DEFAULT NULL, status_id INT NOT NULL, priority_id INT DEFAULT NULL, project_id INT DEFAULT NULL, datetype_id INT DEFAULT NULL, business_process_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, date DATE DEFAULT NULL, referenced_ticketid VARCHAR(255) DEFAULT NULL, consequences LONGTEXT DEFAULT NULL, affected_components LONGTEXT DEFAULT NULL, created DATETIME NOT NULL, customfields LONGTEXT DEFAULT NULL, commentary LONGTEXT DEFAULT NULL, affected_componentsstore LONGTEXT DEFAULT NULL, attachment VARCHAR(255) DEFAULT NULL, INDEX IDX_4F2899EF61220EA6 (creator_id), INDEX IDX_4F2899EFAE80F5DF (department_id), INDEX IDX_4F2899EFC1D2F5A2 (costcenter_id), INDEX IDX_4F2899EF59BB1592 (reason_id), INDEX IDX_4F2899EF6BF700BD (status_id), INDEX IDX_4F2899EF497B19F9 (priority_id), INDEX IDX_4F2899EF166D1F9C (project_id), INDEX IDX_4F2899EFD8CB54F3 (datetype_id), INDEX IDX_4F2899EFBE61FDDF (business_process_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rfc_authorizations (id INT AUTO_INCREMENT NOT NULL, classification_id INT NOT NULL, author_id INT NOT NULL, establishment VARCHAR(255) NOT NULL, target_date DATE NOT NULL, created DATETIME NOT NULL, decision INT NOT NULL, attachment VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_4F94EAF42A86559F (classification_id), INDEX IDX_4F94EAF4F675F31B (author_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rfc_bpo_feedback (id INT AUTO_INCREMENT NOT NULL, delegatee_id INT DEFAULT NULL, rfc_id INT NOT NULL, INDEX IDX_AB8EE87F5BC97D (delegatee_id), INDEX IDX_AB8EE8751C9CB4B (rfc_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rfc_classifications (id INT AUTO_INCREMENT NOT NULL, rtype_id INT NOT NULL, runit_id INT NOT NULL, classificator_id INT NOT NULL, category_id INT NOT NULL, rfc_id INT NOT NULL, authorizationtype_id INT NOT NULL, standardchange_id INT DEFAULT NULL, riskanalysis LONGTEXT NOT NULL, rtime INT NOT NULL, costs INT DEFAULT NULL, hardware_software LONGTEXT DEFAULT NULL, cabconference_date DATE DEFAULT NULL, INDEX IDX_C44F1EADD3771A69 (rtype_id), INDEX IDX_C44F1EADEE86E6F7 (runit_id), INDEX IDX_C44F1EAD19236334 (classificator_id), INDEX IDX_C44F1EAD12469DE2 (category_id), UNIQUE INDEX UNIQ_C44F1EAD51C9CB4B (rfc_id), INDEX IDX_C44F1EAD21F34D72 (authorizationtype_id), INDEX IDX_C44F1EAD8DDF9FBB (standardchange_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rfc_notices (id INT AUTO_INCREMENT NOT NULL, rfc_id INT NOT NULL, status_id INT DEFAULT NULL, author_id INT DEFAULT NULL, title VARCHAR(255) DEFAULT NULL, content LONGTEXT DEFAULT NULL, created DATETIME NOT NULL, INDEX IDX_B98CC4E51C9CB4B (rfc_id), INDEX IDX_B98CC4E6BF700BD (status_id), INDEX IDX_B98CC4EF675F31B (author_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rfc_standardchanges (id INT AUTO_INCREMENT NOT NULL, step_id INT NOT NULL, creator_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, basedata LONGTEXT NOT NULL, classificationdata LONGTEXT NOT NULL, workflowdata LONGTEXT NOT NULL, created DATETIME NOT NULL, active SMALLINT NOT NULL, INDEX IDX_467D214A73B21E9C (step_id), INDEX IDX_467D214A61220EA6 (creator_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rfc_workflows (id INT AUTO_INCREMENT NOT NULL, rfc_id INT DEFAULT NULL, creator_id INT DEFAULT NULL, parent_id INT DEFAULT NULL, implementation_status_id INT DEFAULT NULL, assignee_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, start_date DATE NOT NULL, end_date DATE NOT NULL, cc VARCHAR(255) DEFAULT NULL, notification DATE DEFAULT NULL, objects LONGTEXT NOT NULL, persons LONGTEXT NOT NULL, referenced_ticketid VARCHAR(255) DEFAULT NULL, comment LONGTEXT DEFAULT NULL, INDEX IDX_9A2CC03551C9CB4B (rfc_id), INDEX IDX_9A2CC03561220EA6 (creator_id), INDEX IDX_9A2CC035727ACA70 (parent_id), INDEX IDX_9A2CC035753532DC (implementation_status_id), INDEX IDX_9A2CC03559EC7D60 (assignee_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE roles (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, const VARCHAR(255) DEFAULT NULL, title VARCHAR(255) DEFAULT NULL, locked SMALLINT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE roles_users (role_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_3D80FB2CD60322AC (role_id), INDEX IDX_3D80FB2CA76ED395 (user_id), PRIMARY KEY(role_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE standardchange_steps (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, const VARCHAR(255) DEFAULT NULL, locked SMALLINT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE status (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, const VARCHAR(255) NOT NULL, description VARCHAR(255) DEFAULT NULL, locked SMALLINT NOT NULL, sort INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE units (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, const VARCHAR(255) DEFAULT NULL, locked SMALLINT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_tokens (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, user_agent VARCHAR(255) NOT NULL, token VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, created INT NOT NULL, expires INT NOT NULL, INDEX IDX_CF080AB3A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users (id INT AUTO_INCREMENT NOT NULL, department_id INT DEFAULT NULL, username VARCHAR(180) NOT NULL, email VARCHAR(255) DEFAULT NULL, password VARCHAR(255) DEFAULT NULL, logins INT DEFAULT NULL, last_login INT NOT NULL, firstname VARCHAR(255) DEFAULT NULL, lastname VARCHAR(255) DEFAULT NULL, language VARCHAR(255) DEFAULT NULL, title VARCHAR(255) DEFAULT NULL, const VARCHAR(255) DEFAULT NULL, locked SMALLINT DEFAULT NULL, UNIQUE INDEX UNIQ_1483A5E9F85E0677 (username), INDEX IDX_1483A5E9AE80F5DF (department_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE business_processes ADD CONSTRAINT FK_BD4C9833602AD315 FOREIGN KEY (responsible_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE cost_centers ADD CONSTRAINT FK_6123530E602AD315 FOREIGN KEY (responsible_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE departments ADD CONSTRAINT FK_16AEB8D4783E3463 FOREIGN KEY (manager_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE notification_templates ADD CONSTRAINT FK_C9C13AD1C54C8C93 FOREIGN KEY (type_id) REFERENCES notification_types (id)');
        $this->addSql('ALTER TABLE reports ADD CONSTRAINT FK_F11FA745A76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE rfc ADD CONSTRAINT FK_4F2899EF61220EA6 FOREIGN KEY (creator_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE rfc ADD CONSTRAINT FK_4F2899EFAE80F5DF FOREIGN KEY (department_id) REFERENCES departments (id)');
        $this->addSql('ALTER TABLE rfc ADD CONSTRAINT FK_4F2899EFC1D2F5A2 FOREIGN KEY (costcenter_id) REFERENCES cost_centers (id)');
        $this->addSql('ALTER TABLE rfc ADD CONSTRAINT FK_4F2899EF59BB1592 FOREIGN KEY (reason_id) REFERENCES reasons (id)');
        $this->addSql('ALTER TABLE rfc ADD CONSTRAINT FK_4F2899EF6BF700BD FOREIGN KEY (status_id) REFERENCES status (id)');
        $this->addSql('ALTER TABLE rfc ADD CONSTRAINT FK_4F2899EF497B19F9 FOREIGN KEY (priority_id) REFERENCES priorities (id)');
        $this->addSql('ALTER TABLE rfc ADD CONSTRAINT FK_4F2899EF166D1F9C FOREIGN KEY (project_id) REFERENCES projects (id)');
        $this->addSql('ALTER TABLE rfc ADD CONSTRAINT FK_4F2899EFD8CB54F3 FOREIGN KEY (datetype_id) REFERENCES date_types (id)');
        $this->addSql('ALTER TABLE rfc ADD CONSTRAINT FK_4F2899EFBE61FDDF FOREIGN KEY (business_process_id) REFERENCES business_processes (id)');
        $this->addSql('ALTER TABLE rfc_authorizations ADD CONSTRAINT FK_4F94EAF42A86559F FOREIGN KEY (classification_id) REFERENCES rfc_classifications (id)');
        $this->addSql('ALTER TABLE rfc_authorizations ADD CONSTRAINT FK_4F94EAF4F675F31B FOREIGN KEY (author_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE rfc_bpo_feedback ADD CONSTRAINT FK_AB8EE87F5BC97D FOREIGN KEY (delegatee_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE rfc_bpo_feedback ADD CONSTRAINT FK_AB8EE8751C9CB4B FOREIGN KEY (rfc_id) REFERENCES rfc (id)');
        $this->addSql('ALTER TABLE rfc_classifications ADD CONSTRAINT FK_C44F1EADD3771A69 FOREIGN KEY (rtype_id) REFERENCES ressource_types (id)');
        $this->addSql('ALTER TABLE rfc_classifications ADD CONSTRAINT FK_C44F1EADEE86E6F7 FOREIGN KEY (runit_id) REFERENCES units (id)');
        $this->addSql('ALTER TABLE rfc_classifications ADD CONSTRAINT FK_C44F1EAD19236334 FOREIGN KEY (classificator_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE rfc_classifications ADD CONSTRAINT FK_C44F1EAD12469DE2 FOREIGN KEY (category_id) REFERENCES categories (id)');
        $this->addSql('ALTER TABLE rfc_classifications ADD CONSTRAINT FK_C44F1EAD51C9CB4B FOREIGN KEY (rfc_id) REFERENCES rfc (id)');
        $this->addSql('ALTER TABLE rfc_classifications ADD CONSTRAINT FK_C44F1EAD21F34D72 FOREIGN KEY (authorizationtype_id) REFERENCES authorizations (id)');
        $this->addSql('ALTER TABLE rfc_classifications ADD CONSTRAINT FK_C44F1EAD8DDF9FBB FOREIGN KEY (standardchange_id) REFERENCES rfc_standardchanges (id)');
        $this->addSql('ALTER TABLE rfc_notices ADD CONSTRAINT FK_B98CC4E51C9CB4B FOREIGN KEY (rfc_id) REFERENCES rfc (id)');
        $this->addSql('ALTER TABLE rfc_notices ADD CONSTRAINT FK_B98CC4E6BF700BD FOREIGN KEY (status_id) REFERENCES status (id)');
        $this->addSql('ALTER TABLE rfc_notices ADD CONSTRAINT FK_B98CC4EF675F31B FOREIGN KEY (author_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE rfc_standardchanges ADD CONSTRAINT FK_467D214A73B21E9C FOREIGN KEY (step_id) REFERENCES standardchange_steps (id)');
        $this->addSql('ALTER TABLE rfc_standardchanges ADD CONSTRAINT FK_467D214A61220EA6 FOREIGN KEY (creator_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE rfc_workflows ADD CONSTRAINT FK_9A2CC03551C9CB4B FOREIGN KEY (rfc_id) REFERENCES rfc (id)');
        $this->addSql('ALTER TABLE rfc_workflows ADD CONSTRAINT FK_9A2CC03561220EA6 FOREIGN KEY (creator_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE rfc_workflows ADD CONSTRAINT FK_9A2CC035727ACA70 FOREIGN KEY (parent_id) REFERENCES rfc_workflows (id)');
        $this->addSql('ALTER TABLE rfc_workflows ADD CONSTRAINT FK_9A2CC035753532DC FOREIGN KEY (implementation_status_id) REFERENCES implementation_status (id)');
        $this->addSql('ALTER TABLE rfc_workflows ADD CONSTRAINT FK_9A2CC03559EC7D60 FOREIGN KEY (assignee_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE roles_users ADD CONSTRAINT FK_3D80FB2CD60322AC FOREIGN KEY (role_id) REFERENCES roles (id)');
        $this->addSql('ALTER TABLE roles_users ADD CONSTRAINT FK_3D80FB2CA76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE user_tokens ADD CONSTRAINT FK_CF080AB3A76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE users ADD CONSTRAINT FK_1483A5E9AE80F5DF FOREIGN KEY (department_id) REFERENCES departments (id)');

        $this->addSql('INSERT INTO `categories` VALUES (1,"Default","C__CATEGORY__DEFAULT",1,"Standard Change: pre-authorized with well known risks")');
        $this->addSql('INSERT INTO `categories` VALUES (2,"Category 1","C__CATEGORY__ONE",1,"Category 1: Low risk for business processes")');
        $this->addSql('INSERT INTO `categories` VALUES (3,"Category 2","C__CATEGORY__TWO",1,"Category 2: Medium risk for business processes")');
        $this->addSql('INSERT INTO `categories` VALUES (4,"Category 3","C__CATEGORY__THREE",1,"Category 3: Major risk for business processes")');
        $this->addSql('INSERT INTO `categories` VALUES (5,"Emergency","C__CATEGORY__EMERGENCY",1,"Emergency Change")');

        $this->addSql('INSERT INTO `status` VALUES (1,"Dispatching","C__STATUS__INCOME","Wating for dispatching by section responsible",1,2)');
        $this->addSql('INSERT INTO `status` VALUES (2,"Authorization","C__STATUS__CAB","Wating for authorization",1,5)');
        $this->addSql('INSERT INTO `status` VALUES (3,"CM-released","C__STATUS__CMOK","Unused in RfC 2",1,NULL)');
        $this->addSql('INSERT INTO `status` VALUES (4,"Implementation","C__STATUS__CABOK","Authorized",1,7)');
        $this->addSql('INSERT INTO `status` VALUES (5,"CM-rejected","C__STATUS__CMKO","Unused in RfC 2",1,NULL)');
        $this->addSql('INSERT INTO `status` VALUES (6,"Rejected","C__STATUS__CABKO","Rejected",1,6)');
        $this->addSql('INSERT INTO `status` VALUES (7,"Implementation in progress","C__STATUS__CMDB","Implementation in progress",1,8)');
        $this->addSql('INSERT INTO `status` VALUES (8,"Finished","C__STATUS__COMPLETE","Finished",1,11)');
        $this->addSql('INSERT INTO `status` VALUES (9,"Aborted","C__STATUS__CANCEL","Aborted",1,10)');
        $this->addSql('INSERT INTO `status` VALUES (10,"Classification","C__STATUS__TRANSFERED","Wating for classification",1,3)');
        $this->addSql('INSERT INTO `status` VALUES (11,"Draft","C__STATUS__DRAFT","Draft",1,1)');
        $this->addSql('INSERT INTO `status` VALUES (13,"Validation","C__STATUS__VALIDATION","Wating for validation",1,9)');
        $this->addSql('INSERT INTO `status` VALUES (14,"Feedback from BPO","C__STATUS__BPO","Waiting for feedback from process owner",1,4)');
        $this->addSql('INSERT INTO `status` VALUES (15,"Implementation failed","C__STATUS__FAILED","The creator has marked the RfC as failed",1,12)');

        $this->addSql('INSERT INTO `implementation_status` VALUES (1,"New","C_STATUS_NEW",NULL,1,1)');
        $this->addSql('INSERT INTO `implementation_status` VALUES (2,"Open","C_STATUS_OPEN",NULL,1,2)');
        $this->addSql('INSERT INTO `implementation_status` VALUES (5,"Closed","C_STATUS_CLOSED",NULL,1,3)');
        $this->addSql('INSERT INTO `implementation_status` VALUES (7,"Aborted","C_STATUS_ABORTED",NULL,1,4)');

        $this->addSql('INSERT INTO `departments` (`id`, `title`, `manager_id`, `locked`, `const`) VALUES (1,"defaut",NULL,1,NULL)');
        
        $this->addSql('INSERT INTO `users` (id, department_id, username, password, language, last_login) VALUES (1,1,"admin","$2y$13$QjaLyhaNGIbenTyO40scVegRqHZhf0FPezQ.k5eO8r01a0YG238YS","en", 0);');

        $this->addSql('INSERT INTO `roles` VALUES (1,"login","Login privileges, granted after account confirmation","C__ROLES__LOGIN",NULL,0)');
        $this->addSql('INSERT INTO `roles` VALUES (2,"admin","Administrative user, has access to everything.","C__ROLES__ADMIN",NULL,0)');
        $this->addSql('INSERT INTO `roles` VALUES (3,"changemanager","","C__ROLES__CM",NULL,0)');
        $this->addSql('INSERT INTO `roles` VALUES (4,"cabmember","","C__ROLES__CAB",NULL,0)');

        $this->addSql('INSERT INTO `roles_users` (user_id, role_id) VALUES (1,1)');
        $this->addSql('INSERT INTO `roles_users` (user_id, role_id) VALUES (1,2)');




        $this->addSql('INSERT INTO `reasons` VALUES (1,"Project","C__REASON__PROJECT","",1)');
        $this->addSql('INSERT INTO `reasons` VALUES (2,"Ticket","C__REASON__TICKET","",1)');
        $this->addSql('INSERT INTO `reasons` VALUES (3,"Miscellaneous","C__REASON__MISC","",1)');


        $this->addSql('INSERT INTO `config` (`id`, `group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES (1,"i-doit","api_key","","N;",NULL,NULL,"text","2017-01-27 07:54:59")');
        $this->addSql('INSERT INTO `config` (`id`, `group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES (2,"i-doit","idoit_url","","N;",NULL,NULL,"text","2017-01-27 07:54:59")');
        $this->addSql('INSERT INTO `config` (`id`, `group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES (3,"i-doit","roles","","N;",NULL,NULL,"text","2017-01-27 07:55:23")');
        $this->addSql('INSERT INTO `config` (`id`, `group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES (4,"i-doit","api_lang","","N;",NULL,NULL,"text","2017-01-27 07:55:34")');
        $this->addSql('INSERT INTO `config` (`id`, `group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES (5,"i-doit","overwrite_proxy","","N;",NULL,NULL,"text","2017-01-27 07:55:34")');
        $this->addSql('INSERT INTO `config` (`id`, `group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES (6,"i-doit","affectedComponentOType","","N;",NULL,NULL,"text","2017-01-27 07:55:34")');
        $this->addSql('INSERT INTO `config` (`id`, `group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES (7,"system","language","","N;",NULL,NULL,"text","2017-01-27 12:27:46")');
        $this->addSql('INSERT INTO `config` (`id`, `group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES (8,"email","driver","","N;",NULL,NULL,"text","2017-01-27 12:28:37")');
        $this->addSql('INSERT INTO `config` (`id`, `group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES (9,"email","from","","N;",NULL,NULL,"text","2017-01-27 12:28:37")');
        $this->addSql('INSERT INTO `config` (`id`, `group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES (10,"email","send_notification","","N;",NULL,NULL,"text","2017-01-27 12:28:37")');
        $this->addSql('INSERT INTO `config` (`id`, `group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES (11,"email","options","","N;",NULL,NULL,"text","2017-01-27 12:28:37")');
        $this->addSql('INSERT INTO `config` (`id`, `group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES (12,"tts","available_driver","","N;",NULL,NULL,"text","2017-03-13 07:29:33")');
        $this->addSql('INSERT INTO `config` (`id`, `group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES (13,"tts","active","","N;",NULL,NULL,"text","2017-07-03 05:45:48")');
        $this->addSql('INSERT INTO `config` (`id`, `group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES (14,"tts","pattern","","s:0:\"\";",NULL,NULL,"text","2018-01-04 13:24:39")');
        $this->addSql('INSERT INTO `config` (`id`, `group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES (15,"tts","driver","","N;",NULL,NULL,"text","2018-01-04 13:24:39")');
        $this->addSql('INSERT INTO `config` (`id`, `group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES (17,"tts","auth_token","","s:0:\"\";",NULL,NULL,"text","2020-02-26 09:24:26")');
        $this->addSql('INSERT INTO `config` (`id`, `group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES (18,"tts","tickets_workflow","","N;",NULL,NULL,"text","2020-02-26 09:24:26")');
        $this->addSql('INSERT INTO `config` (`id`, `group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES (19,"tts","tickets_notification","","N;",NULL,NULL,"text","2020-02-26 09:24:26")');
        $this->addSql('INSERT INTO `config` (`id`, `group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES (20,"tts","workflows_group","","s:0:\"\";",NULL,NULL,"text","2020-02-26 09:24:26")');
        $this->addSql('INSERT INTO `config` (`id`, `group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES (21,"i-doit","username","","N;",NULL,NULL,"text","2021-02-10 11:08:52")');
        $this->addSql('INSERT INTO `config` (`id`, `group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES (22,"i-doit","password","","N;",NULL,NULL,"text","2021-02-10 11:08:52")');
        $this->addSql('INSERT INTO `config` (`id`, `group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES (23,"i-doit","port","","N;",NULL,NULL,"text","2021-02-10 11:08:52")');
        $this->addSql('INSERT INTO `config` (`id`, `group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES (24,"email","sendmail","","N;",NULL,NULL,"text","2021-02-10 13:10:39")');
        $this->addSql('INSERT INTO `config` (`id`, `group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES (25,"email","hostname","","N;",NULL,NULL,"text","2021-02-10 13:10:39")');
        $this->addSql('INSERT INTO `config` (`id`, `group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES (26,"email","port","","s:3:\"587\";",NULL,NULL,"text","2021-02-10 13:10:39")');
        $this->addSql('INSERT INTO `config` (`id`, `group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES (27,"email","username","","N;",NULL,NULL,"text","2021-02-10 13:10:39")');
        $this->addSql('INSERT INTO `config` (`id`, `group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES (28,"email","password","","N;",NULL,NULL,"text","2021-02-10 13:10:39")');
        $this->addSql('INSERT INTO `config` (`id`, `group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES (44,"ldap","driver","","N;",NULL,NULL,"text","2021-03-09 11:54:15")');
        $this->addSql('INSERT INTO `config` (`id`, `group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES (45,"ldap","hostname","","N;",NULL,NULL,"text","2021-03-09 11:54:15")');
        $this->addSql('INSERT INTO `config` (`id`, `group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES (46,"ldap","base_dn","","N;",NULL,NULL,"text","2021-03-09 11:54:15")');
        $this->addSql('INSERT INTO `config` (`id`, `group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES (47,"ldap","filter","","N;",NULL,NULL,"text","2021-03-09 11:54:15")');
        $this->addSql('INSERT INTO `config` (`id`, `group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES (48,"ldap","port","","s:3:\"389\";",NULL,NULL,"text","2021-03-09 11:54:15")');
        $this->addSql('INSERT INTO `config` (`id`, `group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES (49,"ldap","ssl","","N;",NULL,NULL,"text","2021-03-09 11:54:15")');
        $this->addSql('INSERT INTO `config` (`id`, `group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES (50,"ldap","rdn","","N;",NULL,NULL,"text","2021-03-09 11:54:15")');
        $this->addSql('INSERT INTO `config` (`id`, `group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES (51,"ldap","password","","N;",NULL,NULL,"text","2021-03-09 11:54:15")');
        $this->addSql('INSERT INTO `config` (`id`, `group_name`, `config_key`, `label`, `config_value`, `default`, `rules`, `field_type`, `date`) VALUES (52,"ldap","active","","N;",NULL,NULL,"text","2021-03-09 11:54:15")');

        $this->addSql('INSERT INTO `ressource_types` (`id`, `title`, `const`, `locked`) VALUES (1,"internal","C__RTYPE__INTERNAL",1)');
        $this->addSql('INSERT INTO `ressource_types` (`id`, `title`, `const`, `locked`) VALUES (2,"external","C__RTYPE__EXTERNAL",1)');
        $this->addSql('INSERT INTO `ressource_types` (`id`, `title`, `const`, `locked`) VALUES (3,"internal/external","C__RTYPE__MERGE",1)');

        $this->addSql('INSERT INTO `units` (`id`, `title`, `const`, `locked`) VALUES (1,"Hour(s)","C__UNIT__HOUR",1)');
        $this->addSql('INSERT INTO `units` (`id`, `title`, `const`, `locked`) VALUES (2,"Day(s)","C__UNIT__DAY",1)');
        $this->addSql('INSERT INTO `units` (`id`, `title`, `const`, `locked`) VALUES (3,"Month(s)","C__UNIT__MONTH",1)');
        $this->addSql('INSERT INTO `units` (`id`, `title`, `const`, `locked`) VALUES (4,"Year(s)","C__UNIT__YEAR",1)');

        $this->addSql('INSERT INTO `date_types` (`id`, `title`, `const`, `locked`) VALUES (1,"Deadline","C__DATETYPE__FIX",1)');
        $this->addSql('INSERT INTO `date_types` (`id`, `title`, `const`, `locked`) VALUES (2,"Desired date","C__DATETYPE__WISH",1)');

        $this->addSql('INSERT INTO `priorities` (`id`, `title`, `const`, `locked`) VALUES (1,"Low","C__PRIORITY__LOW",1)');
        $this->addSql('INSERT INTO `priorities` (`id`, `title`, `const`, `locked`) VALUES (2,"Medium","C__PRIORITY__MIDDLE",1)');
        $this->addSql('INSERT INTO `priorities` (`id`, `title`, `const`, `locked`) VALUES (3,"High","C__PRIORITY__HIGH",1)');
        $this->addSql('INSERT INTO `priorities` (`id`, `title`, `const`, `locked`) VALUES (4,"Urgent","C__PRIORITY__URGENT",1)');

        $this->addSql('INSERT INTO `authorizations` (`id`, `title`, `const`, `locked`) VALUES (1,"by CM","C__AUTHTYPE__CM",1)');
        $this->addSql('INSERT INTO `authorizations` (`id`, `title`, `const`, `locked`) VALUES (2,"by CAB","C__AUTHTYPE__CAB",1)');
        $this->addSql('INSERT INTO `decisions` (`id`, `title`, `const`, `locked`) VALUES (1,"rejected","C__DECISION__REJECTED",1)');
        $this->addSql('INSERT INTO `decisions` (`id`, `title`, `const`, `locked`) VALUES (2,"approved","C__DECISION__APPROVED",1)');

        $this->addSql('INSERT INTO `field_types` (`id`, `title`, `const`, `locked`, `renderer`) VALUES (1,"Textfeld","C__FIELDTYPE__TEXTFIELD",0,"Component_View_Renderer_Text")');
        $this->addSql('INSERT INTO `field_types` (`id`, `title`, `const`, `locked`, `renderer`) VALUES (2,"Checkbox","C__FIELDTYPE__CHECKBOX",0,"Component_View_Renderer_CheckBox")');
        $this->addSql('INSERT INTO `field_types` (`id`, `title`, `const`, `locked`, `renderer`) VALUES (3,"Textbereich","C__FIELDTYPE__TEXTAREA",0,"Component_View_Renderer_TextArea")');
        $this->addSql('INSERT INTO `field_types` (`id`, `title`, `const`, `locked`, `renderer`) VALUES (4,"Datum","C__FIELDTYPE__DATEPICKER",0,"Component_View_Renderer_DatePicker")');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE rfc_classifications DROP FOREIGN KEY FK_C44F1EAD21F34D72');
        $this->addSql('ALTER TABLE rfc DROP FOREIGN KEY FK_4F2899EFBE61FDDF');
        $this->addSql('ALTER TABLE rfc_classifications DROP FOREIGN KEY FK_C44F1EAD12469DE2');
        $this->addSql('ALTER TABLE rfc DROP FOREIGN KEY FK_4F2899EFC1D2F5A2');
        $this->addSql('ALTER TABLE rfc DROP FOREIGN KEY FK_4F2899EFD8CB54F3');
        $this->addSql('ALTER TABLE rfc DROP FOREIGN KEY FK_4F2899EFAE80F5DF');
        $this->addSql('ALTER TABLE users DROP FOREIGN KEY FK_1483A5E9AE80F5DF');
        $this->addSql('ALTER TABLE rfc_workflows DROP FOREIGN KEY FK_9A2CC035753532DC');
        $this->addSql('ALTER TABLE notification_templates DROP FOREIGN KEY FK_C9C13AD1C54C8C93');
        $this->addSql('ALTER TABLE rfc DROP FOREIGN KEY FK_4F2899EF497B19F9');
        $this->addSql('ALTER TABLE rfc DROP FOREIGN KEY FK_4F2899EF166D1F9C');
        $this->addSql('ALTER TABLE rfc DROP FOREIGN KEY FK_4F2899EF59BB1592');
        $this->addSql('ALTER TABLE rfc_classifications DROP FOREIGN KEY FK_C44F1EADD3771A69');
        $this->addSql('ALTER TABLE rfc_bpo_feedback DROP FOREIGN KEY FK_AB8EE8751C9CB4B');
        $this->addSql('ALTER TABLE rfc_classifications DROP FOREIGN KEY FK_C44F1EAD51C9CB4B');
        $this->addSql('ALTER TABLE rfc_notices DROP FOREIGN KEY FK_B98CC4E51C9CB4B');
        $this->addSql('ALTER TABLE rfc_workflows DROP FOREIGN KEY FK_9A2CC03551C9CB4B');
        $this->addSql('ALTER TABLE rfc_authorizations DROP FOREIGN KEY FK_4F94EAF42A86559F');
        $this->addSql('ALTER TABLE rfc_classifications DROP FOREIGN KEY FK_C44F1EAD8DDF9FBB');
        $this->addSql('ALTER TABLE rfc_workflows DROP FOREIGN KEY FK_9A2CC035727ACA70');
        $this->addSql('ALTER TABLE roles_users DROP FOREIGN KEY FK_3D80FB2CD60322AC');
        $this->addSql('ALTER TABLE rfc_standardchanges DROP FOREIGN KEY FK_467D214A73B21E9C');
        $this->addSql('ALTER TABLE rfc DROP FOREIGN KEY FK_4F2899EF6BF700BD');
        $this->addSql('ALTER TABLE rfc_notices DROP FOREIGN KEY FK_B98CC4E6BF700BD');
        $this->addSql('ALTER TABLE rfc_classifications DROP FOREIGN KEY FK_C44F1EADEE86E6F7');
        $this->addSql('ALTER TABLE business_processes DROP FOREIGN KEY FK_BD4C9833602AD315');
        $this->addSql('ALTER TABLE cost_centers DROP FOREIGN KEY FK_6123530E602AD315');
        $this->addSql('ALTER TABLE departments DROP FOREIGN KEY FK_16AEB8D4783E3463');
        $this->addSql('ALTER TABLE reports DROP FOREIGN KEY FK_F11FA745A76ED395');
        $this->addSql('ALTER TABLE rfc DROP FOREIGN KEY FK_4F2899EF61220EA6');
        $this->addSql('ALTER TABLE rfc_authorizations DROP FOREIGN KEY FK_4F94EAF4F675F31B');
        $this->addSql('ALTER TABLE rfc_bpo_feedback DROP FOREIGN KEY FK_AB8EE87F5BC97D');
        $this->addSql('ALTER TABLE rfc_classifications DROP FOREIGN KEY FK_C44F1EAD19236334');
        $this->addSql('ALTER TABLE rfc_notices DROP FOREIGN KEY FK_B98CC4EF675F31B');
        $this->addSql('ALTER TABLE rfc_standardchanges DROP FOREIGN KEY FK_467D214A61220EA6');
        $this->addSql('ALTER TABLE rfc_workflows DROP FOREIGN KEY FK_9A2CC03561220EA6');
        $this->addSql('ALTER TABLE rfc_workflows DROP FOREIGN KEY FK_9A2CC03559EC7D60');
        $this->addSql('ALTER TABLE roles_users DROP FOREIGN KEY FK_3D80FB2CA76ED395');
        $this->addSql('ALTER TABLE user_tokens DROP FOREIGN KEY FK_CF080AB3A76ED395');
        $this->addSql('DROP TABLE authorizations');
        $this->addSql('DROP TABLE business_processes');
        $this->addSql('DROP TABLE categories');
        $this->addSql('DROP TABLE config');
        $this->addSql('DROP TABLE cost_centers');
        $this->addSql('DROP TABLE date_types');
        $this->addSql('DROP TABLE decisions');
        $this->addSql('DROP TABLE departments');
        $this->addSql('DROP TABLE field_types');
        $this->addSql('DROP TABLE implementation_status');
        $this->addSql('DROP TABLE notification_templates');
        $this->addSql('DROP TABLE notification_types');
        $this->addSql('DROP TABLE priorities');
        $this->addSql('DROP TABLE projects');
        $this->addSql('DROP TABLE reasons');
        $this->addSql('DROP TABLE reports');
        $this->addSql('DROP TABLE ressource_types');
        $this->addSql('DROP TABLE rfc');
        $this->addSql('DROP TABLE rfc_authorizations');
        $this->addSql('DROP TABLE rfc_bpo_feedback');
        $this->addSql('DROP TABLE rfc_classifications');
        $this->addSql('DROP TABLE rfc_notices');
        $this->addSql('DROP TABLE rfc_standardchanges');
        $this->addSql('DROP TABLE rfc_workflows');
        $this->addSql('DROP TABLE roles');
        $this->addSql('DROP TABLE roles_users');
        $this->addSql('DROP TABLE standardchange_steps');
        $this->addSql('DROP TABLE status');
        $this->addSql('DROP TABLE units');
        $this->addSql('DROP TABLE user_tokens');
        $this->addSql('DROP TABLE users');
    }
}
