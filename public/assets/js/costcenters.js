$(".chosen-select").chosen({width: "100%", allow_single_deselect: true});

var tr = '';

$(".clickable").click( function () {
    $("option:selected").removeAttr("selected");
    $("#inputId").val($(this).data("item").id);
    $("#inputTitle").val($(this).data("item").title);
    $("#inputResponsible").val($(this).data("item").responsible);
    $("#inputResponsible").trigger("chosen:updated");
    tr = $(this);
});

$(".new").click( function () {
    $("#formSelectionList").trigger("reset");
    $("option:selected").removeAttr("selected");
    $("#inputId").val('');
    $("#inputTitle").val('');
    $("#inputResponsible").val('');
    $("#inputResponsible").trigger("chosen:updated");
    $('table tr:last').after('<tr class="clickable d-flex"><td  class="col-1"></td><td  class="col-5"></td><td  class="col-6"></td></tr>');
    tr = $('table tr:last');
    $(tr).appendTo($(".table"));
});

$("form").submit(function(e){
    e.preventDefault();

    $.ajax({
        type: "POST",
        data: $(this).serialize(),
        url: $(this).attr('action'),
        success: function(data)
        {
            $(".toast-body").html(data.message);
            $('.toast').toast('show');
            $('.modal').modal('toggle');
            //html = '<tr class="clickable d-flex"><td  class="col-1">'+ data.costcenter.id +'</td><td  class="col-5">'+ data.costcenter.title +'</td><td  class="col-6">'+ data.costcenter.responsible +'</td></tr>';
            //tr.replaceWith(html).data("toggle", "modal").attr('data-toggle', 'modal');
            tr.find("td:eq(0)").text(data.costcenter.id);
            tr.find("td:eq(1)").text(data.costcenter.title);
            tr.find("td:eq(2)").text(data.costcenter.responsible);
            tr.data("toggle", "modal");
            tr.data("target", "#Details");
            tr.data("item", JSON.parse(data.costcenter.serialized));
            $(this).reset;
        }
    });
});
