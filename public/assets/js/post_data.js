$(document).ready(function(){
    $("form").submit(function(e){
        e.preventDefault();

        $.ajax({
            type: "POST",
            data: $(this).serialize(),
            url: $(this).attr('action'),
            success: function(data)
            {
                $(".toast-body").html(data.message);
                $('.toast').toast('show');
                $('.modal').modal('toggle');

                if (typeof refresh_items !== "undefined") {
                    refresh_items(data);
                }
            }
        });


    });

});
