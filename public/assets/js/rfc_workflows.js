$(document).ready(function(){
    $(".chosen-select").chosen({
        width: "100%",
        allow_single_deselect: true,
        placeholder_text_single: "--",
        search_contains: true,
    });
    // Implemetation i-doit Object selection
    $("#inputImplementationObjectType").chosen().change(function() {
        $.ajax({
            type: "POST",
            data: $(this).serialize(),
            url: $("#inputAjaxUrl").val(),
            success: function(data)
            {
                $.each( data, function( key, value ) {
                    $("#inputImplementationObjects").append(
                        $("<option></option>").val(value.id).html(value.title + ' (ObjID: ' + value.id + ')')
                    );
                });
                $("#inputImplementationObjects").trigger("chosen:updated");
            }
        });
    });
});