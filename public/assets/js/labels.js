$(document).ready(function(){
    set_field_visibility();

    $("#inputDriver").change(function (){
        var output_driver = $("#inputDriver").val();
        console.log('Output format has changed: ' + output_driver);
        set_field_visibility();
    });

    var $c = $('#label');
    var ctx = $c.get(0).getContext('2d');
    var font1 = $("#inputFontsize1").val();
    var attribute1 = $("#inputAttribute1");

    var labelWidth = $("#inputLabelWidth").val();
    var labelHeight = $("#inputLabelHeight").val();
    $("#label").width(labelWidth * 8);
    $("#label").height(labelHeight * 8);
    var labelWidthPx = $("#label").width();
    var labelHeightPx = $("#label").height();
    var px2mm_ratio = labelWidthPx / labelWidth;

    console.log('Die konfigurierte Label-Breite ist ' + labelWidth + ', die gemessene Label-Breite ist ' + labelWidthPx + 'px. D.h. 1mm entspricht ' + px2mm_ratio + 'Pixel.');
    console.log('Die konfigurierte Label-Höhe ist ' + labelHeight + ', die gemessene Label-Höhe ist ' + labelHeightPx + 'px. D.h. 1mm entspricht ' + labelHeightPx/labelHeight + 'Pixel.')

    var logo = new Image();

    if ($("#Logo").val() == '') {
        var logoLink = "/assets/img/empty.png";
    } else {
        var logoLink = $("#Logo").val();
    }

    logo.src= logoLink;

    logo.onload = function () {
        let logoOrigHeightPx = logo.height;
        let logoOrigWidthPx = logo.width;
        let logoHeightMm = $("#inputLogoHeight").val();
        let logoHeightPx = logoHeightMm * px2mm_ratio;
        let logoWidthPx = logoHeightPx / (logoOrigHeightPx / logoOrigWidthPx);

        console.log('Das Logo soll ' + logoHeightMm + 'mm hoch sein. Das entspricht ' + logoHeightPx + 'px');

        ctx.drawImage(logo, 0, 0,  logoWidthPx, logoHeightPx);

        var qr = new Image();
        qr.src=$("#QrDontPanic").val();
        qr.onload = function () {
            let qrOrigHeightPx = qr.height;
            let qrOrigWidthPx = qr.width;
            let qrHeightMm = $("#inputQrSize").val();
            let qrHeightPx = qrHeightMm * px2mm_ratio;
            let qrWidthPx = qrHeightPx * (qrOrigHeightPx / qrOrigWidthPx);

            console.log('Der QR Code soll ' + qrHeightMm + 'mm hoch sein. Das entspricht ' + qrHeightPx + 'px');
            ctx.drawImage(qr, 0, logoHeightPx, qrWidthPx, qrHeightPx);

            let attribute1 = $("#inputAttribute1").val();
            let fontsize1  = $("#inputFontsize1").val()*2;
            console.log('Fontsize 1: ' + fontsize1);
            let posAttribute1 = logoHeightPx + fontsize1*2.2;
            ctx.font = fontsize1 + "pt Roboto";
            ctx.fillText(attribute1, qrWidthPx, posAttribute1);

            let attribute2 = $("#inputAttribute2").val();
            let fontsize2  = $("#inputFontsize2").val()*2;
            console.log('Fontsize 2: ' + fontsize2);
            let posAttribute2 = posAttribute1 + fontsize2*2.2;
            ctx.font = fontsize2*1.3 + "px Roboto";

            let attribute3 = $("#inputAttribute3").val();
            let posAttribute3 = posAttribute2 + fontsize2*2.2;

            let attribute4 = $("#inputAttribute4").val();
            let posAttribute4 = posAttribute3 + fontsize2*2.2;

            ctx.fillText(attribute2, qrWidthPx, posAttribute2);
            ctx.fillText(attribute3, qrWidthPx, posAttribute3);
            ctx.fillText(attribute4, qrWidthPx, posAttribute4);
        };
    };



    //ctx.font = font1*2 + "pt Roboto";
    //ctx.fillText(attribute1, 30, 30);

});

function set_field_visibility() {
    var output_driver = $("#inputDriver").val();

    if (output_driver == 'ZPL') {
        $(".zebra_only").show();
        $(".pdf_only").hide();
    } else if (output_driver == 'PDF') {
        $(".pdf_only").show();
        $(".zebra_only").hide();
    } else {
        $(".zebra_only").hide();
        $(".pdf_only").hide();
    }
}

