$(document).ready(function(){
    $(".chosen-select").chosen({
        width: "100%",
        allow_single_deselect: true,
        placeholder_text_single: "--",
        search_contains: true,
    });

    var tr = '';

    $(".clickable").click( function () {
        $("option:selected").removeAttr("selected");
        $("#inputRfcWorkflowId").val($(this).data("item").id);
        $("#inputRfcWorkflowTitle").val($(this).data("item").title);
        $("#inputRfcWorkflowDescription").val($(this).data("item").description);
        $("#inputRfcWorkflowStartDate").val($(this).data("item").start_date);
        $("#inputRfcWorkflowEndDate").val($(this).data("item").end_date);
        $("#inputRfcWorkflowParent option[value=" + $(this).data("item").parent + "]").attr("selected", true);
        $("#inputRfcWorkflowAssignee option[value=" + $(this).data("item").assignee + "]").attr("selected", true);
        $("#inputRfcWorkflowAssignee").trigger("chosen:updated");

        ul = $("#workflowAttachments");

        $.each( $(this).data("item").attachments, function( key, value ) {
            console.log(value);
            span = jQuery('<span />').addClass('fa').addClass('fa-remove').addClass('clickable').addClass('workflow-attachment');
            li = jQuery('<li class="list-group-item d-flex justify-content-between align-items-center">' + value.original_name + '</li>').data("attachment_name", value.original_name).attr("data-attachment_name", value.original_name).append(span);
            ul.append(li);
        });




        $.each( $(this).data("item").objects, function( key, value ) {
            $.post( $("#readObjectUrl").val(), {objid: value},  function( data ) {
                $("#inputImplementationObjects").append(
                    $("<option></option>").val(value).html(data.title + ' (ObjID: ' + data.id + ')').attr('selected', 'selected')
                );
            }).done(function (){
                $("#inputImplementationObjects").trigger("chosen:updated");
            });

        });




        tr = $(this);
    });

    $(".new").click( function () {
        $("#formSelectionList").trigger("reset");
        $("option:selected").removeAttr("selected");
        $("#inputRfcWorkflowId").val('');
        $("#inputRfcWorkflowTitle").val('');
        $("#inputRfcWorkflowDescription").val('');
        $("#inputRfcWorkflowStartDate").val('');
        $("#inputRfcWorkflowEndDate").val('');
        $('#inputImplementationObjects')
            .find('option')
            .remove()
            .end()
            .trigger("chosen:updated")
        ;
    });



    $("button[name='delete_task']").click(function (){
        var rfc_workflow_delete = $(this).val();

        if (confirm('Do you really want to delete this task?')) {
            window.location.href = rfc_workflow_delete;
        }
    });


    var chart = new ApexCharts(document.querySelector("#chart"), get_chart_options(get_chart_data()));
    chart.render();




    $("button[name='delete_task']").click(function (){
        var rfc_workflow_delete = $(this).val();

        if (confirm('Do you really want to delete this task?')) {
            window.location.href = rfc_workflow_delete; //relative to domain
        }
    });


    // Implemetation i-doit Object selection
    $("#inputImplementationObjectType").chosen().change(function() {
        $.ajax({
            type: "POST",
            data: $(this).serialize(),
            url: $("#inputAjaxUrl").val(),
            success: function(data)
            {
                $.each( data, function( key, value ) {
                    $("#inputImplementationObjects").append(
                        $("<option></option>").val(value.id).html(value.title + ' (ObjID: ' + value.id + ')')
                    );
                });
                $("#inputImplementationObjects").trigger("chosen:updated");
            }
        });
    });

    // Workflow Fileupload handling
    $(document).on("change", ".workflow-attachment", function(event) {
        var inputFile = event.currentTarget;
        $(this).hide();

        new_input = jQuery('<input type="file" class="form-control-file workflow-attachment" name="attachment[]" multiple>');
        $(this).parent().append(new_input);

        $.each(inputFile.files, function (index, file) {
            span = jQuery('<span />').addClass('fa').addClass('fa-remove').addClass('clickable').addClass('workflow-attachment');
            li = jQuery('<li class="list-group-item d-flex justify-content-between align-items-center">'+ file.name +'</li>').data("attachment_name", file.name).attr("data-attachment_name", file.name).append(span);

            $("#workflowAttachments").append(li);
        });
    });

    // Removing uploaded files from workflow
    $(document).on("click", ".fa-remove.workflow-attachment", function (){
        attachment_name = $(this).parent().data("attachment_name");
        //alert('entferne ' + attachment_name);
        new_input = jQuery('<input type="hidden" name="removed-attachments[]" multiple>').val(attachment_name);
        $("#formRfCImplemetation").append(new_input);
    });

});

function get_chart_data() {
    chart_data = [];
    $('.rfc_workflow_row').each(function(){
        var start_date = new Date($(this).data("item").start_date).getTime();
        var end_date = new Date($(this).data("item").end_date).getTime() + (3600000 * 23) + 3599999;
        console.log('start date: ' + start_date);
        console.log('end date: ' + end_date);

        chart_data.push(
            {
                x: $(this).data("item").title,
                y: [
                    start_date,
                    end_date
                ]
            }
        );
    });
    return chart_data;
}

function get_chart_options (chart_data) {
    var options = {
        series: [
            {
                data: chart_data
            }
        ],
        chart: {
            height: 50 * (chart_data.length +1),
            type: 'rangeBar'
        },
        plotOptions: {
            bar: {
                horizontal: true
            }
        },
        xaxis: {
            type: 'datetime'
        },
        colors: ['#212142']
    };

    return options;
}
