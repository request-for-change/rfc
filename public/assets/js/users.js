$(document).ready(function(){
        $(".chosen-select").chosen({width: "100%", allow_single_deselect: true});
        var tr = '';
        $(".clickable").click( function () {

                $("#formUser").trigger("reset");
                $("option:selected").removeAttr("selected");
                $("#inputRoles").trigger("chosen:updated");

                $("#inputId").val($(this).data("user").id);
                $("#inputFirstname").val($(this).data("user").firstname);
                $("#inputLastname").val($(this).data("user").lastname);
                $("#inputEmail").val($(this).data("user").email);
                $("#inputUsername").val($(this).data("user").username);

                $("#goToInventoryLink").attr("href", "inventory/" + $(this).data("user").id);

                //$("#inputDepartment").val($(this).data("user").department);
                console.log($(this).data("user").department);
                $("#inputDepartment option[value="+$(this).data("user").department+"]").attr("selected", true);
                $("#inputLanguage").val($(this).data("user").language);
                $.each($(this).data("user").roles, function( intIndex, objValue ){
                        $("#inputRoles option[value=" + objValue + "]").attr("selected", true);
                })
                if ($(this).data("user").locked) {
                        $("#inputLocked").prop('checked', true);
                } else {
                        $("#inputLocked").prop('checked', false);
                }
                $("#inputRoles").trigger("chosen:updated");

                tr = $(this);
        });

        $(".new").click(function (){
                $("option:selected").removeAttr("selected");
                $("#inputRoles").trigger("chosen:updated");
                $("#inputFirstname").removeAttr("readonly").removeClass('form-control-plaintext').addClass('form-control');
                $("#inputLastname").removeAttr("readonly").removeClass('form-control-plaintext').addClass('form-control');
                $("#inputEmail").removeAttr("readonly").removeClass('form-control-plaintext').addClass('form-control');
                $("#inputUsername").removeAttr("readonly").removeClass('form-control-plaintext').addClass('form-control');


                $('table tr:last').after('<tr class="clickable"><td></td> <td></td> <td></td> <td></td> <td></td> <td></td> <td class="text-center"></td> <td class="text-center"></td> <td class="text-center"></td> </tr>');
                tr = $('table tr:last');
        });

        $('form[name="userform"]').submit(function(e){
                e.preventDefault();

                $.ajax({
                        type: "POST",
                        data: $(this).serialize(),
                        url: $(this).attr('action'),
                        success: function(data)
                        {
                                $(".toast-body").html(data.message);
                                $('.toast').toast('show');
                                $('.modal').modal('toggle');

                                tr.find("td:eq(0)").text(data.user.id);
                                tr.find("td:eq(1)").text(data.user.firstname);
                                tr.find("td:eq(2)").text(data.user.lastname);
                                tr.find("td:eq(3)").text(data.user.email);
                                tr.find("td:eq(4)").text(data.user.username);
                                tr.find("td:eq(5)").text(data.user.department);
                                if ($.inArray('C__ROLES__CM', data.user.roles) > 0) {
                                        tr.find("td:eq(6)").html('<span class="fa fa-check"></span>');
                                } else {
                                        tr.find("td:eq(6)").html('');
                                }
                                if ($.inArray('C__ROLES__CAB', data.user.roles) > 0) {
                                        tr.find("td:eq(7)").html('<span class="fa fa-check"></span>');
                                } else {
                                        tr.find("td:eq(7)").html('');
                                }
                                if ($.inArray('C__ROLES__ADMIN', data.user.roles) > 0) {
                                        tr.find("td:eq(8)").html('<span class="fa fa-check"></span>');
                                } else {
                                        tr.find("td:eq(8)").html('');
                                }

                                tr.data("toggle", "modal");
                                tr.data("target", "#Details");
                                tr.data("user", JSON.parse(data.user.serialized));
                                $(this).reset;
                        }
                });
        });
});
