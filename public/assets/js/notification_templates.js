$(document).ready(function(){
        $("form").submit(function(e){
                e.preventDefault();

                $.ajax({
                        type: "POST",
                        data: $(this).serialize(),
                        url: $(this).attr('action'),
                        success: function(data)
                        {
                                $(".toast-body").html(data.message);
                                $('.toast').toast('show');
                                $('.modal').modal('toggle');

                                if (typeof refresh_items !== "undefined") {
                                        refresh_items(data);
                                }
                        }
                });


        });

        $(".clickable").click( function () {
                $("#formNotificationTemplates").trigger("reset");
                $("option:selected").removeAttr("selected");
                console.log($(this).data("template"));
                $("#inputId").val($(this).data("template").id);
                $("#inputSubject").val($(this).data("template").subject);
                $("#inputContentEn").val($(this).data("template").content_en);
                $("#inputContentDe").val($(this).data("template").content_de);
                $("#inputType").val($(this).data("template").type);

        });
});
