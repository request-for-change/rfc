$(document).ready(function (){
    $("button[name='delete']").click(function (){
        var networkscan_delete = $(this).val();
        if (confirm('Do you really want to delete this scan?')) {
            window.location.href = networkscan_delete; //relative to domain
        }
    });

    $( 'form[name="networkscans"] [type=checkbox]' ).change(function (){
        //alert($( 'form[name="networkscans"] [type=checkbox]:checked' ).length) ;
        if ($( 'form[name="networkscans"] [type=checkbox]:checked' ).length == 2) {
            $("#compare_button").prop('disabled', false);
        } else {
            $("#compare_button").prop('disabled', true);
        }
    });

    $('#highlightChanged').change(function (){
        if($(this).prop("checked") == true){
            $("td.changed").css("background-color", "rgba(80,228,241,0.65)");
        } else if($(this).prop("checked") == false){
            $("td.changed").css("background-color", "#ffffff");
        }
    });

    $('#hideUnchanged').change(function (){
        if($(this).prop("checked") == true){
            $("tr.unchanged").hide();
        }
        else if($(this).prop("checked") == false){
            $("tr.unchanged").show();
        }
    });
})