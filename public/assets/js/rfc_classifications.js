$(document).ready(function(){
    set_standardchange_attributes();
    set_cab_meeting_attributes();


    $(".category").change(function (){
        set_standardchange_attributes();
    });


    $('.authorizationtype').change(function (){
        set_cab_meeting_attributes();
    });

    $(".standardchange").change(function (){
        if ($(".standardchange").val()){
            classificationdata = JSON.parse($('#standardchange_' + $(".standardchange").val()).val());

            //alert($("#rtype option:selected").val());
            // $('.selDiv option[value="SEL1"]')
            $('#rtype option[value="'+classificationdata.rtype+'"]').prop('selected', true);
            $("#costs").val(classificationdata.costs);
            $("#rtime").val(classificationdata.rtime);
            $('#runit option[value="'+classificationdata.runit+'"]').prop('selected', true);
            $("#hardware_software").val(classificationdata.hardware_software);
            $("#riskanalysis").val(classificationdata.riskanalysis);
        }
    });
});

function set_cab_meeting_attributes() {
    var authorizationtype = "";
    $( ".authorizationtype option:selected" ).each(function() {
        authorizationtype = $( this ).val();
    });

    if (authorizationtype === 'C__AUTHTYPE__CAB') {
        $('.cab-meeting').removeAttr('readonly').attr('required', true);
    } else {
        $('.cab-meeting').removeAttr('required').attr('readonly', true);
    }
}

function set_standardchange_attributes() {
    var category = "";
    $( ".category option:selected" ).each(function() {
        category = $( this ).val();
    });

    // If standard change is selected as category
    if (category === 'C__CATEGORY__DEFAULT') {
        $(".authorizationtype").val("C__AUTHTYPE__CM").attr('disabled', true);
        $(".standardchange").val("C__AUTHTYPE__CM").attr('disabled', false);
        $(".standardchange").val("C__AUTHTYPE__CM").attr('required', true);
        set_cab_meeting_attributes();
    } else {
        $(".standardchange").val("C__AUTHTYPE__CM").attr('disabled', true);
        $(".standardchange").val("C__AUTHTYPE__CM").attr('required', false);
        $(".authorizationtype").val("C__AUTHTYPE__CM").attr('disabled', false);
    }
}




