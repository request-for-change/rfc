$(".clickable").click( function () {

        $("#inputId").val($(this).data("item")[0].value);
        $("#inputLabel").val($(this).data("item")[1].value);
        $("#inputType").val($(this).data("item")[2].value);
        $("#inputPlaceholder").val($(this).data("item")[3].value);
        $("#inputDefault").val($(this).data("item")[4].value);
});

$(".new").click( function () {
        $("#formSelectionList").trigger("reset");
        $("#inputId").val(generateUUIDv4());
        $("#inputLabel").val("");
        $("#inputType").val("");
        $("#inputPlaceholder").val("");
        $("#inputDefault").val("");
});

function generateUUIDv4() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
                var r = Math.random() * 16 | 0,
                    v = c == 'x' ? r : (r & 0x3 | 0x8);
                return v.toString(16);
        });
}