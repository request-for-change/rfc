$(".chosen-select").chosen({width: "100%", allow_single_deselect: true});

var tr = '';

$(".clickable").click( function () {
    console.log($(this).data("item"));
    $("option:selected").removeAttr("selected");
    $("#inputId").val($(this).data("item")[0].value);
    $("#inputTitle").val($(this).data("item")[1].value);
    $("#inputDescription").val($(this).data("item")[2].value);
    if ($(this).data("item")[3].value !== "" ) {
        $("#inputWorkflowParent option[value=" + $(this).data("item")[3].value + "]").attr("selected", true);
    }


    tr = $(this);

});

$("#workflowSave").click(function (){
    var workflow_data = [
        {name: 'workflow_id', value: $("#inputId").val()},
        {name: 'title_workflow', value: $("#inputTitle").val()},
        {name: 'desc_workflow', value: $("#inputDescription").val()},
        {name: 'parent_id', value: $("#inputWorkflowParent").val()},
    ];


    console.log(workflow_data);

    tr.find("td:eq(0)").text($("#inputId").val());
    tr.find("td:eq(1)").text($("#inputTitle").val());
    tr.data("item", workflow_data);

    $('.modal').modal('toggle');
});

$("#workflowDelete").click(function (){

    tr.remove();

    $('.modal').modal('toggle');
});

$(".new").click( function () {
    $("#formWorkflowDetails").trigger("reset");
    $("option:selected").removeAttr("selected");
    $("#inputId").val(Math.floor(Math.random() * Math.floor(1000)));
    $("#inputTitle").val('');
    $("#inputDescription").val('');
    tr = jQuery('<tr class="clickable workflow_row"><td class="col-1"></td><td></td></tr>').attr("data-toggle", "modal").attr("data-target", "#workflowDetails").data("toggle", "modal").data("target", "#workflowDetails");

    $(tr).appendTo($("#workflowsList"));
});

$("#standardchangeSave").click(function (){
    var workfolw_data = [];
    $(".workflow_row").each(function (){
        workfolw_data.push($(this).data('item'));
    });

    $("#inputWorkflowdata").val(JSON.stringify(workfolw_data));
    var form = $("#formStandardchange");
    form.submit();
});

