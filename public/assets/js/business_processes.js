$(document).ready(function(){
    $(".chosen-select").chosen({width: "100%", allow_single_deselect: true});

    var tr = '';

    $(".clickable").click( function () {
        $("option:selected").removeAttr("selected");
        $("#inputId").val($(this).data("item").id);
        $("#inputTitle").val($(this).data("item").title);
        $("#inputResponsible").val($(this).data("item").responsible);
        $("#inputResponsible").trigger("chosen:updated");
        tr = $(this);
    });

    $(".new").click( function () {
        $("#formSelectionList").trigger("reset");
        $("option:selected").removeAttr("selected");
        $("#inputId").val('');
        $("#inputTitle").val('');
        $("#inputResponsible").val('');
        $("#inputResponsible").trigger("chosen:updated");
        $('table tr:last').after('<tr class="clickable d-flex"><td  class="col-1"></td><td  class="col-5"></td><td  class="col-6"></td></tr>');
        tr = $('table tr:last');
        $(tr).appendTo($(".table"));
    });

    $("#formSelectionList").submit(function(e){
        e.preventDefault();

        $.ajax({
            type: "POST",
            data: $(this).serialize(),
            url: $(this).attr('action'),
            success: function(data)
            {
                $(".toast-body").html(data.message);
                $('.toast').toast('show');
                $('.modal').modal('toggle');
                tr.find("td:eq(0)").text(data.business_process.id);
                tr.find("td:eq(1)").text(data.business_process.title);
                tr.find("td:eq(2)").text(data.business_process.responsible);
                tr.data("toggle", "modal");
                tr.data("target", "#Details");
                tr.data("item", JSON.parse(data.business_process.serialized));
                $(this).reset;
            }
        });
    });

});
