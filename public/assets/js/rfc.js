$(document).ready(function(){
    $(".chosen-select").chosen({
        width: "100%",
        allow_single_deselect: true,
        placeholder_text_single: "--",
        search_contains: true,
    });
    set_ticket_and_project_attr();
    $(".reason").change(function (){
        set_ticket_and_project_attr();
        console.log('reason has changed');
        $('#add').html('');
        $("#additional_fields").html('');
    });

    // Loading i-doit objects after selecting object type
    $("#inputObjectType").chosen().change(function() {
        $.ajax({
            type: "POST",
            data: $(this).serialize(),
            url: $("#inputAjaxUrl").val(),
            success: function(data)
            {
                $.each( data, function( key, value ) {
                    $("#inputObjects").append(
                        $("<option></option>").val(value.id).html(value.title + ' (ObjID: ' + value.id + ')')
                    );
                });
                $("#inputObjects").trigger("chosen:updated");
            }
        });
    });
    // adding from i-doit Objects to list
    $("#submiObjects").click(function (){
        affected_components_store = $("#affectedComponentsstore").data("affected_components_store");
        if (!Array.isArray(affected_components_store)) {
            affected_components_store = [];
        }
        $('#inputObjects > option:selected').each(function() {
            span = jQuery('<span />').addClass('fa').addClass('fa-remove').addClass('clickable');
            li = jQuery('<li class="list-group-item d-flex justify-content-between align-items-center">'+ $(this).text() +'</li>').data("affected_component_id", $(this).val()).attr("data-affected_component_id", $(this).val()).append(span);
            $("#affectedComponentsstore").append(li);

            affected_components_store.push($(this).val());
        });

        $('#affectedComponentsstore').attr("data-affected_components_store", JSON.stringify(affected_components_store));
        $('#affectedComponentsstore').data("data-affected_components_store", (affected_components_store));
        $('#inputAffectedComponentsstore').val(JSON.stringify(affected_components_store));
    });

    // Removing from i-doit Objects from List
    $(document).on("click", ".fa-remove", function() {
        affected_components_store = ($("#affectedComponentsstore").data("affected_components_store"));
        affected_component_id = $(this).parent().data("affected_component_id");

        for( var i = 0; i < affected_components_store.length; i++){
            console.log('content of component_store ' + i + ' is ' + affected_components_store[i]);
            if ( affected_components_store[i] == affected_component_id) {
                affected_components_store.splice(i, 1);
            } else {
                console.log(affected_components_store[i] + ' !== ' + affected_component_id);
            }
        }

        $(this).parent().remove();
        $('#affectedComponentsstore').attr("data-affected_components_store", JSON.stringify(affected_components_store));
        $('#affectedComponentsstore').data("data-affected_components_store", (affected_components_store));
        $('#inputAffectedComponentsstore').val(JSON.stringify(affected_components_store));
    });


    // Submission Fileupload handling
    $(document).on("change", ".submission-attachment", function(event) {

        attachments = [];
        //$("#Attachments").empty();

        var inputFile = event.currentTarget;
        //$(inputFile).parent().find('.custom-file-label').html(inputFile.files[0].name);
        $(this).hide();

        new_input = jQuery('<input type="file" class="form-control-file submission-attachment" name="attachment[]" multiple>');
        $(this).parent().append(new_input);

        $.each(inputFile.files, function (index, file) {
            console.log(file.name);
            span = jQuery('<span />').addClass('fa').addClass('fa-remove').addClass('clickable');
            li = jQuery('<li class="list-group-item d-flex justify-content-between align-items-center">'+ file.name +'</li>').data("attachment_name", file.name).attr("data-attachment_name", file.name).append(span);

            $("#Attachments").append(li);
            attachments.push(file.name);
            $('#Attachments').attr("data-attachments", JSON.stringify(attachments));
            $('#Attachments').data("data-attachments", (attachments));
        });
    });

    // Authorization Fileupload handling
    $(document).on("change", ".authorization-attachment", function(event) {
        var inputFile = event.currentTarget;
        $(this).hide();

        new_input = jQuery('<input type="file" class="form-control-file authorization-attachment" name="attachment[]" multiple>');
        $(this).parent().append(new_input);

        $.each(inputFile.files, function (index, file) {
            span = jQuery('<span />').addClass('fa').addClass('fa-remove').addClass('clickable');
            li = jQuery('<li class="list-group-item d-flex justify-content-between align-items-center">'+ file.name +'</li>').data("attachment_name", file.name).attr("data-attachment_name", file.name).append(span);

            $("#AuthorizationAttachments").append(li);
        });
    });

    $("button[name='delete']").click(function (){
        var rfc_delete = $(this).val();
        if (confirm('Do you really want to delete this draft?')) {
            window.location.href = rfc_delete; //relative to domain
        }
    });

});

function set_ticket_and_project_attr() {
    var tts_driver = $("#tts_driver").val();
    var placeholder = "";
    var reason = "";

    $( ".reason option:selected" ).each(function() {
        reason = $( this ).val();
    });

    if (reason === '1') {
        // Project
        $(".form-group.project").addClass('required');
        $(".project").attr('required', true).attr('disabled', false);
    } else {
        $(".form-group.project").removeClass('required');
        $(".project").attr('required', false).attr('disabled', true);
    }

    if (reason === '2') {
        // Ticket
        $(".form-group.ticket").addClass('required');
        $(".ticket").attr('required', true).attr('placeholder', 'Ticket # is required when Reason is Ticket');
    } else {
        $(".form-group.ticket").removeClass('required');
        if (tts_driver === 'Zammad') {
            placeholder = "autogenerated when left empty";
        }
        $(".ticket").attr('required', false).attr('placeholder', placeholder);
    }

    if (reason) {
        let additional_fields_values = $("#additional_fields").data("stored_values")
/*        $.each(additional_fields_values,function (id,value) {
            console.log("id: " + id);
            console.log("value: " + value);
        })*/

        $.ajax({
            url: '../reason/fields/' + reason, // your endpoint here
            type: 'GET', // type of request: POST, GET, PUT, DELETE, etc
            dataType: 'json', // type of data you're expecting back from the server
            success: function (data, status, xhr) {
                // handle success
                $.each(data, function(index, field){

                    if (field.type === '1') { // text
                        $("#additional_fields").append(
                            '<div class="form-group">' +
                            '<label for="'+ field.id +'">' + field.label + '</label>' +
                            '<input name="additional_fields['+ field.id +']" class="form-control" type="text" id="' + field.id + '" value="' + field.default + '" placeholder="' + field.placeholder + '" />' +
                            '</form-group>'
                        );
                        if (additional_fields_values !== null && additional_fields_values !== undefined && additional_fields_values.hasOwnProperty(field.id)) {
                            let id = field.id;
                            $("#" + field.id).val(additional_fields_values[id]);
                        }
                    } else if (field.type === '2') { // checkbox
                        $("#additional_fields").append(
                            '<div class="form-group">' +
                            '<div class="form-check">\n' +
                            '  <input name="additional_fields['+ field.id +']" class="form-check-input" type="checkbox" value="1" id="'+ field.id +'">\n' +
                            '  <label class="form-check-label" for="'+ field.id +'">\n' + field.label +'  </label>\n' +
                            '</div>' +
                            '</form-group>'
                        );
                        if (additional_fields_values !== null && additional_fields_values !== undefined && additional_fields_values.hasOwnProperty(field.id)) {
                            let id = field.id;
                            if (additional_fields_values[id]) {
                                $("#" + id).prop('checked', true);
                            }
                        }
                    } else if (field.type === '3') { // Textarea
                        if (additional_fields_values !== null && additional_fields_values !== undefined && additional_fields_values.hasOwnProperty(field.id)) {
                            let id = field.id;
                            var text = additional_fields_values[id];
                        } else {
                            var text = field.default;
                        }

                        $("#additional_fields").append(
                            '<div class="form-group">' +
                            '<label for="'+ field.id +'">' + field.label + '</label>' +
                            '<textarea name="additional_fields['+ field.id +']" class="form-control" rows="7">' + text + '</textarea>' +
                            '</form-group>'
                        );
                    } else if (field.type === '4') { // date
                        $("#additional_fields").append(
                            '<div class="form-group">' +
                            '<label for="'+ field.id +'">' + field.label + '</label>' +
                            '<input name="additional_fields['+ field.id +']" class="form-control" type="date" id="' + field.id + '" value="' + field.default + '" placeholder="' + field.placeholder + '" />' +
                            '</form-group>'
                        );
                        if (additional_fields_values !== null && additional_fields_values !== undefined && additional_fields_values.hasOwnProperty(field.id)) {
                            let id = field.id;
                            $("#" + field.id).val(additional_fields_values[id]);
                        }
                    }




                });
            },
            error: function (xhr, status, error) {
                // handle error
                console.log("An error occurred: " + status, error);
            }
        });
    }


}
