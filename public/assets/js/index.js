$(document).ready(function (){
    $(".chosen-select").chosen({
        width: "100%",
        allow_single_deselect: true,
        placeholder_text_single: "--",
        search_contains: true,
    });

    $(".order-indicator").click(function (){
       //alert($(this).data("orderField"));
        $("#sort_by").val($(this).data("orderField"));
        $("#sort_direction").val($(this).data("orderDirection"));
        $("#search").submit();
    });

    $('#collapseToggler').on('click', function () {
        var text=$('#collapseToggler').html();
        if($("#collapseToggler").hasClass('fa-plus-square-o')){
            $("#collapseToggler").removeClass('fa-plus-square-o');
            $("#collapseToggler").addClass('fa-minus-square-o');
        } else{

            $("#collapseToggler").removeClass('fa-minus-square-o');
            $("#collapseToggler").addClass('fa-plus-square-o');
        }
    });

    $("#clear-button").click(function () {
        // function to clear all form elements on page
        $('#search').find("input[type=date], input[type=text], textarea, select").val("");
        console.log('click…');
    })
});

