$(document).ready(function () {
    $(".chosen-select").chosen({width: "100%", allow_single_deselect: true, placeholder_text_single: " "});

    $("#checkAll").on("change", function (){
        $(".checkbox").not(this).prop('checked', this.checked);
    });

    $("#listSelect").on("change", function(){
        let selected_label = $(this).children("option:selected").val();
        unselect();
        $(".select").children('[value='+selected_label+']').attr('selected', true);
    });



    $("#labelsForm").submit(function(e){
        e.preventDefault();

        $.ajax({
            type: "POST",
            data: $(this).serialize(),
            url: $(this).attr('action'),
            success: function(data)
            {
                console.log(data.error);
                if (data.error === '' && data.redirect !== '') {
                    location.href = data.redirect;
                } else {
                    $(".toast-body").html(data.message);
                    $('.toast').toast('show');
                    $('.modal').modal('toggle');
                }
            }
        });


    });

});

function unselect() {
    $.each($(".select option:selected"), function () {
        $(this).attr('selected', false);
    });
}