$(".clickable").click( function () {
        $("#inputId").val($(this).data("item").id);
        $("#inputTitle").val($(this).data("item").title);
});

$(".new").click( function () {
        $("#formSelectionList").trigger("reset");
});

$(".default-reason").change(function (){
        $.ajax({
                type: "GET",
                url: "reason/default/"+$(this).prop("id"),
        });
});

$(".active-reason").change(function (){
        $.ajax({
                type: "GET",
                url: "reason/active/"+$(this).prop("id")+"/"+$(this).prop("checked"),
        });
});

function refresh_items(data) {
        tbody = '';
        $.each(data.trs, function (key, value){
                console.log(value);
                tbody = tbody + '<tr class="d-flex">';
                tbody = tbody + '<td data-toggle="modal" data-target="#Details" class="clickable col-1">'+ value.id +'</td>';
                tbody = tbody + '<td class="col-11">'+ value.title +'</td>'
                tbody = tbody + '</tr>';

        });

        $("tbody").html(tbody);
}
